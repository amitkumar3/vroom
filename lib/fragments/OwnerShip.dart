//import 'package:flutter/material.dart';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
//import 'package:sample_flutter/constants/EndPoints.dart';

//class OwnerShip extends StatefulWidget {
//  final String title;
//
//  OwnerShip(this.title);
//
//  _OwnerShip createState() => _OwnerShip();
//}
//
//class _OwnerShip extends State<OwnerShip> {
//  @override
//  Widget build(BuildContext context) {
//    return WebviewScaffold(
//      appBar: AppBar(
//        title: Text(super.widget.title),
//        elevation: 8.0,
//      ),
//      hidden: true,
//      url: 'https://vroom.infini.work/ownership',
//      initialChild: Material(
//        color: Colors.black.withAlpha(300),
//        child: Container(
//          child: Center(
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
//                new CircularProgressIndicator(),
//                new SizedBox(
//                  height: 15.0,
//                ),
//                Text(
//                  "Please Wait...",
//                  style: TextStyle(
//                      fontWeight: FontWeight.w700, color: Colors.black),
//                )
//              ],
//            ),
//          ),
//        ),
//      ),
//      withZoom: true,
//      withJavascript: true,
//      scrollBar: false,
//    );
//  }
//}


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OwnerShip extends StatefulWidget
{
  final String title;

  OwnerShip(this.title);

  _OwnerShip createState() => _OwnerShip();
}

class _OwnerShip extends State<OwnerShip>
{
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: new Text(super.widget.title),
      ),
      body:

      Container(
            padding: EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[

                Text('For used personal vehicles only.',
                  textAlign: TextAlign.center,
                ),

                SizedBox(height: 15.0,),

                Text('Transfer of ownership is the most critical part '
                    'of any vehicle buy and sell. Usually in Bangladesh,'
                    ' we do this part after the payment is made or some do not'
                    ' transfer at all which may lead to serious legal issues.'
                    'As such, if any critical situation develops, both the buyer'
                    ' and seller gets into a time consuming hassle. To relieve the'
                    ' vehicle buyers and sellers of used vehicles, we offer this service.',
                  textAlign: TextAlign.center,
                ),

                SizedBox(height: 15.0,),

                Text('OTS (Ownership Transfer Service) customers can '
                    'be assured that Vroom Team will make sure the name '
                    'is properly transferred and all the documents with '
                    'the authorities are properly submitted.', textAlign: TextAlign.center,),

                SizedBox(height: 15.0,),

                Text('Once you finalize selling or buying the vehicle,'
                    ' please book for our OTS services and our Relationship '
                    'Officer will get in touch with you regarding the necessary '
                    'documents and authorizations.', textAlign: TextAlign.center,),

                Form(child:
                  TextFormField(
                    
                  )
                )
              ],
            ),
          ),
    );
  }
}
