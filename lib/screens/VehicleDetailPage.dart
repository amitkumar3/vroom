import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/constants/AppStrings.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class VehicleDetailPage extends StatefulWidget {
  final String title, price, owner, token, year;
  final images;
  String varExteriorColor,
      varBrands,
      varBrandsModel,
      varFuelType,
      varCities,
      varBodyType,
      varTransmissionData,
      varVehicleCondition;

  VehicleDetailPage(
      this.title,
      this.price,
      this.owner,
      this.token,
      this.images,
      this.year,
      this.varExteriorColor,
      this.varBrands,
      this.varBrandsModel,
      this.varFuelType,
      this.varCities,
      this.varBodyType,
      this.varTransmissionData,
      this.varVehicleCondition);

  @override
  _VehicleDetailPage createState() => _VehicleDetailPage();
}

class _VehicleDetailPage extends State<VehicleDetailPage> {
  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  Future getCurrentUser() async {
    String id = await getStringDataLocally(key: 'id');
    return id;
  }

  GlobalKey<FormState> _key = new GlobalKey();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _validate = false;
  String phone;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Vehicle Details"),
      ),
      bottomNavigationBar: Row(children: <Widget>[
        Expanded(
          child: SizedBox(
            height: 45,
            child: RaisedButton(
              elevation: 5.0,
              color: hexToColor("#4da424"),
              child: Text(
                getTokenAmount(widget.price, int.parse(widget.token)),
                style: new TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              splashColor: Colors.lightGreenAccent,
              onPressed: () {},
            ),
          ),
        ),
      ]),
      body: SingleChildScrollView(
        child: new Container(
          color: hexToColor("#ffffff"),
          // color: hexToColor(AppColors.COLOR_GREY_BACKGROUND),
          child: new Stack(
            children: <Widget>[
              new Container(
                height: 200.0,
                color: Colors.grey,
                child: Carousel(
                  images: widget.images,
                  boxFit: BoxFit.cover,
                  autoplay: false,
                  //animationCurve: Curves.fastOutSlowIn,
                  //animationDuration: Duration(milliseconds: 1000),
                  dotSize: 0.0,
                  dotSpacing: 0.0,
                  dotBgColor: Colors.transparent,
                  indicatorBgPadding: 10.0,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 160.0, left: 5.0, right: 5.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        elevation: 5.0,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 12.0, top: 5.0),
                                child: Text(
                                  widget.title,
                                  /*widget.varBrands[0],*/
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[800],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 12.0, top: 10.0),
                                child: Text(
                                  "Selling Price",
                                  style: TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 12.0, top: 4.0),
                                child: Text(
                                  "৳ " + amountFormatted(widget.price),
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[800],
                                  ),
                                ),
                              ),
                            ]),
                      ),
                    ),
                    Container(
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        elevation: 5.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Text(
                                "Basic Facts",
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[800],
                                ),
                              ),
                            ),
                            //buildVehicleDetailTabs("Brand", widget.brand),
                            //buildVehicleDetailTabs("Model", widget.model),
                            buildVehicleDetailTabs(
                                "Year", widget.year.toString()),
                            //buildVehicleDetailTabs("Body Type", widget.model),
                            //buildVehicleDetailTabs("Exterior Color", widget.model),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        elevation: 5.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Text(
                                "Key Factors",
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[800],
                                ),
                              ),
                            ),
                            //buildVehicleDetailTabs("Location", widget.location),
                            buildVehicleDetailTabs(
                                "Number of Owners", widget.owner + " Owner"),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        elevation: 5.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Text(
                                "Rating & Reviews",
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[800],
                                ),
                              ),
                            ),
                            Column(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(12.0),
                                  child: new TextFormField(
                                    decoration: new InputDecoration(
                                      alignLabelWithHint: true,
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: hexToColor(
                                                AppColors.COLOR_VROOM_THEME)),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: hexToColor("#e0e0e0")),
                                      ),
                                      hintText: AppStrings.TEXT_REVIEW_HINT,
                                      hintStyle: TextStyle(color: Colors.grey),
                                    ),
                                    inputFormatters: [
                                      new LengthLimitingTextInputFormatter(500),
                                    ],
                                    expands: false,
                                    maxLines: 4,
                                    keyboardType: TextInputType.multiline,
                                    onSaved: (String val) {
                                      //telephone = val;
                                    },
                                  ),
                                ),
                              ],
                            ),
                            buildVehicleDetailTabs("Rating", "4.7 Stars"),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
