import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter/auth/Auth.dart';
import 'package:sample_flutter/auth/Auth.dart' as prefix0;
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/constants/AppStrings.dart';
import 'package:sample_flutter/constants/EndPoints.dart';
import 'package:sample_flutter/constants/AppAssets.dart';
import 'package:sample_flutter/screens/Dashboard.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'SetPassword.dart';

import 'dart:convert' as JSON;

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  bool selectIndividual = true;
  bool selectOrganization = false;
  String fName, lName, email, telephone, account, organizationName;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = new GoogleSignIn();

// Google GoogleSignIn

  Future<String> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount =
        await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;

    print(user);

    displayProgressDialog(context);

    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => Dashboard()));
  }

//  bool isLoggedIn = true;
//  Map userProfile;
//  final facebookLogin = FacebookLogin();
//
//  FacebookLogin fbLogin = new FacebookLogin();

//  _loginWithFB() async
//  {
//    final result = await facebookLogin.logIn(['email']);
//
//    switch(result.status) {
//
//      case FacebookLoginStatus.loggedIn:
//        final token = result.accessToken.token;
//        final graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=${token}');
//        final profile = JSON.jsonDecode(graphResponse.body);
//        print(profile);
//        setState(() {
//          userProfile = profile;
//          isLoggedIn = true;
//          Navigator.push(context, MaterialPageRoute(builder: (context) => SecondPage()));
//        });
//
//        break;
//
//      case FacebookLoginStatus.cancelledByUser:
//        setState(() {
//          isLoggedIn = false;
//        });
//        break;
//      case FacebookLoginStatus.error:
//        setState(() {
//          isLoggedIn = false;
//        });
//        break;
//    }
//
//  }

//
//  _logout() {
//
//  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 5.0),
        child: SingleChildScrollView(
          padding: EdgeInsets.all(8.0),
          child: Form(
            key: _key,
            autovalidate: _validate,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      SizedBox(
                        height: 60,
                      ),
                      Image(
                        image: AssetImage(AppAssets.ASSET_APP_LOGO),
                        alignment: AlignmentDirectional.center,
                        fit: BoxFit.contain,
                        height: 50,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "New Registration",
                        style: TextStyle(color: Colors.black, fontSize: 18.0),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        "Please enter your Information",
                        style: TextStyle(color: Colors.grey, fontSize: 14.0),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      SizedBox(
                        height: 10.0,
                      ),

                      /**
                       *  START: -------------  FirstName field  ---------------
                       */

                      Text(
                        "First Name*",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        validator: validateName,
                        onSaved: (String val) {
                          fName = val;
                          print('First Name onSaved');
                        },
                      ),

                      /**
                       *   --------------------  END  ---------------------
                       */

                      SizedBox(
                        height: 10.0,
                      ),

                      /**
                       *  START: -------------- LastName field  ---------------
                       */

                      Text(
                        "Last Name*",
                        style:
                            new TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        textCapitalization: TextCapitalization.sentences,
                        validator: validateName,
                        onSaved: (String val) {
                          lName = val;
                          print('Last Name onSaved');
                        },
                      ),

                      /**
                       * ------------------  END  -----------------
                       */

                      SizedBox(
                        height: 10.0,
                      ),

                      /**
                       *  START: ---------------  Email Address ---------------
                       */

                      Text(
                        "Email Address*",
                        style:
                            new TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        validator: validateEmail,
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (String val) {
                          email = val;
                        },
                      ),

                      /**
                       *  -------------------  END  ----------------------
                       */

                      SizedBox(
                        height: 10.0,
                      ),

                      /**
                       *  START: -------------  Phone Number ----------------
                       */

                      Text(
                        "Phone Number*",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(10),
                        ],
                        validator: validateMobile,
                        keyboardType: TextInputType.number,
                        onSaved: (String val) {
                          telephone = val;
                        },
                      ),

                      /**
                       *  ------------------  END  -------------------
                       */

                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text('OR',
                              style: TextStyle(
                                  color: hexToColor("#707070"), fontSize: 14.0))
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),

                      /**
                       *  START: -------------  Social Login FB and Google  --------------
                       */

                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            /**
                             *  START: ---------- Facebook Login ------------
                             */

                            Expanded(
                              child: RaisedButton(
                                textColor: hexToColor("#ffffff"),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0)),
                                color: hexToColor("#3b5998"),
                                splashColor: hexToColor("#ffeaa7"),
                                onPressed:() async
                                {
                                  displayProgressDialog(context);
                                  prefix0.AuthResult result =
                                      await Auth.loginWithFacebook();
                                  if (result.success) {
                                    var usr = Auth.getFacebookUserDetails(
                                        result.token);
                                    //print(usr);
                                    displayProgressDialog(context);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Dashboard()));
                                  } else {
                                    result.error;
                                  }
                                  //initiateSignIn("FB");
                                },


                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Icon(
                                      IconData(0xe903, fontFamily: "Dash"),
                                    ),
                                    SizedBox(
                                      height: 50,
                                      width: 10,
                                    ),
                                    Text('Facebook'),
                                  ],
                                ),
                              ),
                            ),

                            /**
                             *  ---------------- END -------------------
                             */

                            Padding(
                              padding: EdgeInsets.all(8.0),
                            ),

                            /**
                             *  START: ------------  Google Login ---------------
                             */

                            Expanded(
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0)),
                                color: hexToColor(AppColors.COLOR_TEXT_WHITE),
                                splashColor: hexToColor("#ffeaa7"),
                                onPressed: signInWithGoogle,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Image(
                                      image:
                                          AssetImage('assets/google_logo.png'),
                                      width: 50,
                                      height: 30,
                                    ),
                                    /*Icon(
                                      IconData(0xe90a, fontFamily: "Dash"),
                                    ),*/
                                    SizedBox(
                                      height: 50,
                                      width: 0,
                                    ),
                                    Text('Google'),
                                  ],
                                ),
                              ),
                            ),

                            /**
                             *  ------------------  END  ------------------
                             */
                          ]),

                      /**
                       *  ----------------------  END  ---------------------------
                       */

                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "Account Type",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: RaisedButton(
                                child: Text(
                                  'Individual',
                                  style: TextStyle(
                                      color: !selectIndividual
                                          ? hexToColor(
                                              AppColors.COLOR_VROOM_THEME)
                                          : Colors.white),
                                ),
                                color: selectIndividual
                                    ? hexToColor(AppColors.COLOR_VROOM_THEME)
                                    : Colors.white,
                                splashColor: Colors.orangeAccent,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0)),
                                onPressed: () {
                                  setState(() {
                                    selectIndividual = true;
                                    selectOrganization = false;
                                  });
                                },
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                            ),
                            Expanded(
                              child: RaisedButton(
                                child: Text(
                                  'Organization',
                                  style: TextStyle(
                                      color: !selectOrganization
                                          ? hexToColor(
                                              AppColors.COLOR_VROOM_THEME)
                                          : Colors.white),
                                ),
                                color: selectOrganization
                                    ? hexToColor(AppColors.COLOR_VROOM_THEME)
                                    : Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0)),
                                splashColor: Colors.orangeAccent,
                                onPressed: () {
                                  setState(() {
                                    selectOrganization = true;
                                    selectIndividual = false;
                                  });
                                  // Perform some action
                                },
                              ),
                            ),
                          ]),
                      SizedBox(
                        height: 10.0,
                      ),
                      selectOrganization
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Organization Name*",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 14.0),
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                TextFormField(
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: hexToColor(
                                              AppColors.COLOR_VROOM_THEME)),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: hexToColor("#e0e0e0")),
                                    ),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                  ),
                                  validator: validateName,
                                  keyboardType: TextInputType.text,
                                  onSaved: (String val) {
                                    organizationName = val;
                                  },
                                ),
                              ],
                            )
                          : SizedBox(),
                      SizedBox(
                        height: 10.0,
                      ),
                      SizedBox(
                          height: 50.0,
                          width: double.infinity,
                          child: new RaisedButton(
                            child: const Text('Next'),
                            textColor: Colors.white,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5.0)),
                            color: hexToColor(AppColors.COLOR_VROOM_THEME),
                            splashColor: Colors.orangeAccent,
                            onPressed: () {
                              if (selectIndividual) {
                                account = "Individual";
                              } else if (selectOrganization) {
                                account = "Organization";
                              }
                              print(account);
                              _sendToServer();
                            },
                          )),
                      SizedBox(
                        height: 5.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _sendToServer() async {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
      if (selectIndividual) {
        account = "Individual";
      } else if (selectOrganization) {
        account = "Organization";
      }
      if (organizationName == null || organizationName.isEmpty) {
        organizationName = "N/A";
      }
      final body = {"email": email};
      final res = await http.post(EndPoints.checkExistingUser, body: body);
      final response = json.decode(res.body);
      if (response['status'] == "0") {
        //showSnackBar(response['msg'].toString(), _key);
        //closeProgressDialog(context);
        showCupertinoAlert(AppStrings.TEXT_ERROR, response['msg'].toString(),
            AppStrings.TEXT_OKAY, "", context);
      } else if (response['status'] == "1") {
        print("firstname: $fName");
        print("lastname: $lName");
        print("telephone: $telephone");
        print("email: $email");
        print("Account Type: $account");
        print("Organization: $organizationName");
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => SetPassword(
                  fName.trim(),
                  lName.trim(),
                  email.trim(),
                  telephone.trim(),
                  account.trim(),
                  organizationName.trim()),
            ));
      } else {
        // validation error
        setState(() {
          _validate = true;
        });
      }
    }
  }
}
