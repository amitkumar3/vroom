


import 'package:flutter/material.dart';

class MyPurchases extends StatefulWidget
{

  final String title;

  MyPurchases(this.title);

  _MyPurchases createState() => _MyPurchases();
}

class _MyPurchases extends State<MyPurchases>
{
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(super.widget.title),
      ),
    );
  }
}