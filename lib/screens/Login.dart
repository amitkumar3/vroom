import 'dart:async';
import 'dart:convert';
import 'dart:ffi';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter/auth/Auth.dart' as prefix0;
import 'package:sample_flutter/constants/AppAssets.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/constants/EndPoints.dart';
import 'package:sample_flutter/screens/Dashboard.dart';
import 'package:sample_flutter/screens/SignUp.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:sample_flutter/constants/AppStrings.dart';
import 'ForgotPassword.dart';
import 'package:sample_flutter/auth/Auth.dart';

class Login extends StatefulWidget {
  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> {

  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  String user, pass;
  bool isLoggedIn = false;
  bool _passwordVisibility = true;
  var loggedIn = false;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = new GoogleSignIn();

// Google GoogleSignIn

  Future<String> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount =
        await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;

    print(user);

    displayProgressDialog(context);

    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => Dashboard()));
  }

//  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
//  final GoogleSignIn _googleSignIn = new GoogleSignIn();

//    GoogleSignInAccount googleSignInAccount = await _handleGoogleSignIn();
//    final googleAuth = await googleSignInAccount.authentication;
//    final googleAuthCred = GoogleAuthProvider.getCredential(
//        idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);
//    final user = await _firebaseAuth.signInWithCredential(googleAuthCred);
//
////    print("User : " + user.displayName);//     print("phoneNumber : " + user.phoneNumber);
////    print("email : " + user.email);
////    print("providerId : " + user.providerId);
////    print("uid : " + user.uid);
//
//    Navigator.push(context, new MaterialPageRoute(builder: (context) => SecondPage()));
//
//  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //  getIsLoggedIn();
    redirectIfNotLogin();

  }

  void redirectIfNotLogin() async {
    await getIsLoggedIn();
    if (isLoggedIn) {
      Navigator.push(
          context,
          MaterialPageRoute(builder: (BuildContext context) => Dashboard()
              //  Dashboard(),
              ));
    }
  }

  Future getIsLoggedIn() async {
    isLoggedIn = await getBooleanDataLocally("isLoggedIn");
    // null handling of data
    // print("Telephone: " + abc);
  }

  void onLoginStatusChanged(bool isLoggedIn) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _key,
            autovalidate: _validate,
            child: Column(
              children: <Widget>[


                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Image(
                        image: AssetImage(AppAssets.ASSET_APP_LOGO),
                        alignment: AlignmentDirectional.center,
                        fit: BoxFit.contain,
                        height: 50,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        "Login",
                        style: TextStyle(color: Colors.black, fontSize: 18.0),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        "Please enter your credentials",
                        style: TextStyle(color: Colors.grey, fontSize: 14.0),
                      ),
                    ],
                  ),
                ),


                Container(
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      SizedBox(
                        height: 15.0,
                      ),

                      /**
                       *   START : ------------ Email Address/phNo  Field  -----------
                       */

                      Text(
                        "Mobile No/Email Address",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),

                          prefixIcon: Icon(Icons.email, color: Colors.grey, size: 20,)

                        ),
                        keyboardType: TextInputType.emailAddress,
                        validator: validateEmail,
                        onSaved: (String val) {
                          user = val;
                        },
                      ),

                      /**
                       *  --------------------- END: ------------------
                       */

                      SizedBox(
                        height: 15.0,
                      ),

                      /**
                       *  START: ------------  Password Field  -------------
                       */

                      Text(

                        "Password",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),

                      SizedBox(
                        height: 10.0,
                      ),

                      TextFormField(
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.lock, color: Colors.grey, size: 20,),
                          suffixIcon: IconButton(
                            icon: Icon(
                              _passwordVisibility
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                              /*_passwordVisibility
                                  ? IconData(0xe903, fontFamily: "Dash")
                                  : IconData(0xe903, fontFamily: "Dash"),*/
                              color: Colors.grey,
                             size: 20,
                             // color: Color(0xFF595959),
                            ),
                            onPressed: () {
                              setState(() {
                                _passwordVisibility ^= true;
                                print('Show/Hide Password Clicked');
                              });
                            },
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        obscureText: _passwordVisibility ? true : false,
                        validator: validatePassword,
                        onSaved: (String val) {
                          pass = val;
                          print('onSaved getting called');
                        },
                      ),

                      /**
                       *  -----------------  END  -----------------
                       */

                      SizedBox(
                        height: 10.0,
                      ),

                      /**
                       *  START: ------------  Forget Password Field  ------------
                       */

                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ForgotPassword(),
                                  ));
                            },
                            child: Text(
                              'Forgot Password?',
                              style: TextStyle(
                                  color:
                                      hexToColor(AppColors.COLOR_VROOM_THEME),
                                  fontSize: 14.0),
                            ),
                          ),
                        ],
                      ),

                      /**
                       *  ----------------- END --------------
                       */

                      SizedBox(
                        height: 15.0,
                      ),

                      /**
                       *  START: ----------------  Submit button  ----------------
                       */

                      SizedBox(
                        height: 50.0,
                        width: double.infinity,
                        child: RaisedButton(
                          child: const Text('Submit'),
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          color: hexToColor(AppColors.COLOR_VROOM_THEME),
                          splashColor: Colors.orangeAccent,
                          onPressed: callApi,
                        ),
                      ),

                      /**
                       *  ------------------ END -------------------
                       */

                      SizedBox(
                        height: 12.5,
                      ),

                      /**
                       *  START: -------------  Social Login FB and Google  --------------
                       */

                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            /**
                             *  START: ---------- Facebook Login ------------
                             */

                            Expanded(
                              child: RaisedButton(
                                textColor: hexToColor("#ffffff"),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0)),
                                color: hexToColor("#3b5998"),
                                splashColor: hexToColor("#ffeaa7"),
                                onPressed: () async

                                {
                                  prefix0.AuthResult result =
                                      await Auth.loginWithFacebook();
                                  if (result.success) {
                                    var usr = Auth.getFacebookUserDetails(
                                        result.token);
                                    print(usr);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Dashboard()));
                                  } else {
                                    result.error;
                                  }
                                  // initiateSignIn("FB");
                                },

                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Icon(
                                      IconData(0xe903, fontFamily: "Dash"),
                                    ),
                                    SizedBox(
                                      height: 50,
                                      width: 10,
                                    ),
                                    Text('Facebook'),
                                  ],
                                ),
                              ),
                            ),

                            /**
                             *  ---------------- END -------------------
                             */

                            Padding(
                              padding: EdgeInsets.all(8.0),
                            ),

                            /**
                             *  START: ------------  Google Login ---------------
                             */

                            Expanded(
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0)),
                                color: hexToColor("#ffffff"),
                                splashColor: hexToColor("#ffeaa7"),
                                onPressed: signInWithGoogle,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Image(
                                      image:
                                          AssetImage('assets/google_logo.png'),
                                      width: 50,
                                      height: 30,
                                    ),
                                    SizedBox(
                                      height: 50,
                                      width: 0,
                                    ),
                                    Text('Google'),
                                  ],
                                ),
                              ),
                            ),

                            /**
                             *  ------------------  END  ------------------
                             */
                          ]),

                      /**
                       *  ----------------------  END  ---------------------------
                       */

                      SizedBox(
                        height: 12.5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text('OR',
                              style: TextStyle(
                                  color: hexToColor("#707070"), fontSize: 14.0))
                        ],
                      ),
                      SizedBox(
                        height: 12.5,
                      ),

                      /**
                       *  START: ------------ Create account field ------------- #ffeaa7
                       */

                      SizedBox(
                        height: 50.0,
                        width: double.infinity,
                        child: RaisedButton(
                            child: const Text('Create Account'),
                            textColor: hexToColor("#FCAF17"),
                            shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: hexToColor(
                                        AppColors.COLOR_VROOM_THEME)),
                                borderRadius: BorderRadius.circular(5.0)),
                            color: Colors.white70,
                            elevation: 0,
                            //    splashColor: hexToColor("#ffeaa7"),
                            onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => SignUp(),
                                ))
                        ),
                      ),

                      /**
                       *  ---------------- END ------------------
                       */

                      SizedBox(
                        height: 5.0,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

//  void initiateSignIn(String type) {
//    _handleSignIn(type).then((result) {
//      if (result == 1) {
//        setState(() {
//          loggedIn = true;
//          Navigator.push(
//              context,
//              MaterialPageRoute(
//                builder: (BuildContext context) => SecondPage()
//                    //Dashboard(),
//              ));
//        });
//      } else {}
//    });
//  }

//  Future<int> _handleSignIn(String type) async
//  switch (type) {
//      case "FB":
//        FacebookLoginResult facebookLoginResult = await _handleFBSignIn();
//        final accessToken = facebookLoginResult.accessToken.token;
//        if (facebookLoginResult.status == FacebookLoginStatus.loggedIn) {
//          final facebookAuthCred =
//          FacebookAuthProvider.getCredential(accessToken: accessToken);
//          final user =
//          await firebaseAuth.signInWithCredential(facebookAuthCred);
//          final graphResponse = await http.get(
//              'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=' +
//                  accessToken);
//          final profile = json.decode(graphResponse.body);
//          print("Profile : " + profile.toString());
//        //  print("displayName : " + user.displayName);
//          return 1;
//        } else
//          return 0;
//        break;

//      case "G":
//        try {
//          GoogleSignInAccount googleSignInAccount = await _handleGoogleSignIn();
//          final googleAuth = await googleSignInAccount.authentication;
//          final googleAuthCred = GoogleAuthProvider.getCredential(
//              idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);
//          final user = await firebaseAuth.signInWithCredential(googleAuthCred);
//          print("User : " + user);//     print("phoneNumber : " + user.phoneNumber);
//          print("email : " + user.email);
//          print("providerId : " + user.providerId);
//          print("uid : " + user.uid);
//
//          setState(() {
//            loggedIn = true;
//          });
//
//        } catch (error) {
//          return 0;
//        }
//    }
//    return 0;
//  }

//  Future<FacebookLoginResult> _handleFBSignIn() async {
//    FacebookLogin facebookLogin = FacebookLogin();
//    FacebookLoginResult facebookLoginResult =
//  //  await facebookLogin.logInWithReadPermissions(['email']);
//    switch (facebookLoginResult.status) {
//      case FacebookLoginStatus.cancelledByUser:
//        print("Cancelled");
//        break;
//      case FacebookLoginStatus.error:
//        print("error");
//        break;
//      case FacebookLoginStatus.loggedIn:
//        print("Logged In");
//        break;
//    }
//    return facebookLoginResult;
//  }

//  Future<GoogleSignInAccount> _handleGoogleSignIn() async {
//    GoogleSignIn googleSignIn = GoogleSignIn(
//        scopes: ['email', 'https://www.googleapis.com/auth/contacts.readonly']);
//    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
//    return googleSignInAccount;
//  }

  callApi() async {
    if (_key.currentState.validate()) {
      // No any error in validation
      displayProgressDialog(context);
      _key.currentState.save();
      print("Name $user");
      print("Pass $pass");
      final body = {"username": user, "password": pass};
      print(body);
      final res = await http.post(EndPoints.userLogin, body: body);
      print(res);

      final response = json.decode(res.body);
      if (response['status'] == 'error') {
        closeProgressDialog(context);
        showCupertinoAlert(AppStrings.TEXT_ERROR, response['msg'].toString(),
            AppStrings.TEXT_OKAY, "", context);
      } else {
        closeProgressDialog(context);
        print(response);
        writeBooleanDataLocally("isLoggedIn", true);
        writeDataLocally(key: 'id', value: response['user']['id'].toString());
        writeDataLocally(
            key: 'firstname', value: response['user']['firstname']);
        writeDataLocally(key: 'lastname', value: response['user']['lastname']);
        writeDataLocally(
            key: 'telephone', value: response['user']['telephone']);
        writeDataLocally(
            key: 'user_type', value: response['user']['user_type'].toString());
        writeDataLocally(
            key: 'status', value: response['user']['status'].toString());
        writeDataLocally(
            key: 'created_at', value: response['user']['created_at']);
        writeDataLocally(
            key: 'updated_at', value: response['user']['updated_at']);
        writeDataLocally(key: 'token', value: response['user']['token']);
        writeDataLocally(key: 'type', value: response['user']['type']);
        writeDataLocally(
            key: 'organization_name',
            value: response['user']['organization_name']);
        writeDataLocally(key: 'email', value: response['user']['email']);
        writeDataLocally(
            key: 'otpstatus', value: response['user']['otpstatus'].toString());
        writeDataLocally(
            key: 'facebook_id',
            value: response['user']['facebook_id'].toString());
        Navigator.push(
            context,
            MaterialPageRoute(builder: (BuildContext context) => Dashboard()
                //Dashboard(),
                ));
      }
      print(response['msg']);
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }
}

//void initiateFacebookLogin() async {
//    var facebookLogin = FacebookLogin();
//    var facebookLoginResult =
//        await facebookLogin.logInWithReadPermissions(['email']);
//    facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
//    switch (facebookLoginResult.status) {
//      case FacebookLoginStatus.error:
//        print("Error");
//        onLoginStatusChanged(false);
//        break;
//      case FacebookLoginStatus.cancelledByUser:
//        print("CancelledByUser");
//        onLoginStatusChanged(false);
//        break;
//      case FacebookLoginStatus.loggedIn:
//        print("LoggedIn");
//        final result = await facebookLogin.logInWithReadPermissions(['email']);
//        final token = result.accessToken.token;
//        final graphResponse = await http.get(
//            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${token}');
//        final profile = json.decode(graphResponse.body);
//        print(result);
//        print(profile);
//        print(profile['name']);
//        print(profile['first_name']);
//        print(profile['last_name']);
//        print(profile['email']);
//        print(profile['id']);
//        onLoginStatusChanged(true);
//        writeBoolDataLocally(key: "isLoggedIn", value: true);
//        Navigator.push(
//            context,
//            MaterialPageRoute(
//              builder: (BuildContext context) => SecondPage()
//                  //Dashboard(),
//            ));
//        break;
//    }
//  }

//class UserDetails {
//  final String providerDetails;
//  final String userName;
//  final String userEmail;
//  final List<ProviderDetails> providerData;
//
//  UserDetails(this.providerDetails, this.userName, this.userEmail, this.providerData);
//
//}

//class ProviderDetails {
//  ProviderDetails(this.providerDetails);
//  final String providerDetails;
//}
