import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter/constants/AppAssets.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/screens/Dashboard.dart';

//import 'package:vroom_ap/screens/Dashboard.dart';
import 'package:sample_flutter/tools/Constants.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class OTP extends StatefulWidget {
  String mobile;
  String userId, otp;

  OTP(this.mobile, this.userId, this.otp) : super();

  @override
  _OTP createState() => _OTP();
}

class _OTP extends State<OTP> {
  String inputOtp;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  Future getCurrentUser() async {
    String userId = await getStringDataLocally(key: 'id');
  }

  GlobalKey<FormState> _key = new GlobalKey();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _validate = false;
  String phone;

  final _inputOtpController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _key,
            autovalidate: _validate,
            child: Column(
              children: <Widget>[
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Image(
                        image: AssetImage(AppAssets.ASSET_APP_LOGO),
                        alignment: AlignmentDirectional.center,
                        fit: BoxFit.contain,
                        height: 50,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        "Verify Mobile Number",
                        style: TextStyle(color: Colors.black, fontSize: 18.0),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        "Please confirm your Identity",
                        style: TextStyle(color: Colors.grey, fontSize: 14.0),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      SizedBox(
                        height: 50.0,
                      ),
                      Text(
                        "Mobile Number",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        enableInteractiveSelection: false,
                        focusNode: new AlwaysDisabledFocusNode(),
                        decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        initialValue: widget.mobile,
                        validator: (val) {
                          if (val.length == 0) {
                            return "Mobile Number can't be empty";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.phone,
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        "Enter OTP",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        validator: (value) =>
                            value.isEmpty ? "Field can't be empty" : null,
                        //onSaved: (val) => val
                        onSaved: (val) => inputOtp,
                        controller: _inputOtpController,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            'Resend OTP',
                            style:
                                TextStyle(color: Colors.orange, fontSize: 14.0),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      SizedBox(
                        height: 50.0,
                        width: double.infinity,
                        child: RaisedButton(
                            child: const Text('Submit'),
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            color: hexToColor(AppColors.COLOR_VROOM_THEME),
                            splashColor: Colors.orangeAccent,
                            onPressed: () {
                              //_verifyOTP(200, 4654);
                              _verifyOTP(widget.userId, inputOtp);
                            }),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _verifyOTP(String userId, String otp) async {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
      String otp = _inputOtpController.text;
      if (phone != null) {
        showSnackBar("Something went wrong.", scaffoldKey);
      } else {
        final body = {
          /* "inputotp": otp,
          "otp": widget.otp,
          "userid": userId,*/

          "inputotp": otp.toString(),
          "otp": otp.toString(),
          "userid": userId.toString(),
          "responseType": "json"
        };
        final url = 'https://vroom.infini.work/api/checkotp';
        print(body);
        final res = await http.post(url, body: body);
        final response = json.decode(res.body);
        if (response['status'] == 'error') {
          showSnackBar(response['msg'].toString(), scaffoldKey);
        } else if (response['status'] == 1) {
          showSnackBar(response['msg'].toString(), scaffoldKey);
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => Dashboard(),
              ));
        }
        print(response['msg']);
      }
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }
}
