
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sample_flutter/constants/AppAssets.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class ChangePassword extends StatefulWidget
{

  final String title;

  ChangePassword(this.title);

  _ChangePassword createState() => _ChangePassword();
}

class _ChangePassword extends State<ChangePassword>
{

  GlobalKey<FormState> _formKey = GlobalKey();

  bool _validate = false;

  String email, currPassword, newPassword, currNewPassword;

  bool _passwordVisibility = true;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(super.widget.title),
      ),

      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(left: 8.0, right: 8.0),
          child: Column(
            children: <Widget>[

              Container(
                padding: EdgeInsets.only(top: 50, bottom: 50),
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    
                    SvgPicture.asset(AppAssets.CHANGE_PWD_LOGO,
                    height: 75,
                    fit: BoxFit.contain,
                    alignment: Alignment.center,
                    ),
                  ],
                ),
              ),


              Form(
                key: _formKey,
                autovalidate: _validate,
                child: Column(
                  children: <Widget>[

                    /**
                     *  Email field
                     */
                    Container(
                      padding:
                      EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Email *",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0),
                            ),
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(
                                top: 20.0, bottom: 8.0),
                          ),
                          TextFormField(
                            autovalidate: _validate,
                            decoration: InputDecoration(
                              suffixIcon: Icon(Icons.email, color: hexToColor(AppColors.COLOR_VROOM_THEME),
                              ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: hexToColor(AppColors
                                          .COLOR_VROOM_THEME)),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: hexToColor("#e0e0e0")),
                                ),
                                contentPadding:
                                EdgeInsets.symmetric(
                                    vertical: 10.0,
                                    horizontal: 10.0)),
                            keyboardType:
                            TextInputType.emailAddress,
                            validator: validateEmail,
                            onSaved: (String val) {
                              email = val;
                            },
                          ),
                        ],
                      ),
                    ),


                    /**
                     *  current password
                     */
                    Container(
                      padding:
                      EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Current Password *",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0),
                            ),
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(
                                top: 10.0, bottom: 8.0),
                          ),
                          Container(
                            child: TextFormField(
                              autovalidate: _validate,
                              obscureText: _passwordVisibility
                                  ? true
                                  : false,
                              decoration: InputDecoration(

                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      _passwordVisibility
                                          ? Icons.visibility_off
                                          : Icons.visibility,

                                      color: hexToColor(AppColors.COLOR_VROOM_THEME),
                                      size: 20.0,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _passwordVisibility ^= true;
                                        print(
                                            'Show/Hide Password Clicked');
                                      });
                                    },
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: hexToColor(AppColors
                                            .COLOR_VROOM_THEME)),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                        hexToColor("#e0e0e0")),
                                  ),
                                  contentPadding:
                                  EdgeInsets.symmetric(
                                      vertical: 10.0,
                                      horizontal: 10.0)),
                              keyboardType: TextInputType.text,
                              validator: validatePassword,
                              onSaved: (String val) {
                                currPassword = val;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),


                    /**
                     *  New password
                     */
                    Container(
                      padding:
                      EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Text(
                              "New Password *",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0),
                            ),
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(
                                top: 10.0, bottom: 8.0),
                          ),
                          Container(
                            child: TextFormField(
                              autovalidate: _validate,
                              obscureText: _passwordVisibility
                                  ? true
                                  : false,
                              decoration: InputDecoration(

                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      _passwordVisibility
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      /*_passwordVisibility
                                        ? IconData(0xe903, fontFamily: "Dash")
                                        : IconData(0xe903, fontFamily: "Dash"),*/
                                      color: hexToColor(AppColors.COLOR_VROOM_THEME),
                                      size: 20.0,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _passwordVisibility ^= true;
                                        print(
                                            'Show/Hide Password Clicked');
                                      });
                                    },
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: hexToColor(AppColors
                                            .COLOR_VROOM_THEME)),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                        hexToColor("#e0e0e0")),
                                  ),
                                  contentPadding:
                                  EdgeInsets.symmetric(
                                      vertical: 10.0,
                                      horizontal: 10.0)),
                              keyboardType: TextInputType.text,
                              validator: validatePassword,
                              onSaved: (String val) {
                                newPassword = val;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),


                    /**
                     *  Confirm new password
                     */
                    Container(
                      padding: EdgeInsets.only(
                          left: 8.0, right: 8.0, bottom: 15),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Confirm New Password *",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0),
                            ),
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(
                                top: 10.0, bottom: 8.0),
                          ),
                          Container(
                            child: TextFormField(
                              autovalidate: _validate,
                              obscureText: _passwordVisibility
                                  ? true
                                  : false,
                              decoration: InputDecoration(

                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      _passwordVisibility
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: hexToColor(AppColors.COLOR_VROOM_THEME),
                                      size: 20.0,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _passwordVisibility ^= true;
                                        print(
                                            'Show/Hide Password Clicked');
                                      });
                                    },
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: hexToColor(AppColors
                                            .COLOR_VROOM_THEME)),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                        hexToColor("#e0e0e0")),
                                  ),
                                  contentPadding:
                                  EdgeInsets.symmetric(
                                      vertical: 10.0,
                                      horizontal: 10.0)),
                              keyboardType: TextInputType.text,
                              validator: validatePassword,
                              onSaved: (String val) {
                                currNewPassword = val;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),



                    /**
                     *   Submit button
                     */

                    Container(
                      padding:
                      EdgeInsets.only(left: 8.0, right: 8.0, bottom: 20.0),
                      child: SizedBox(
                        height: 50.0,
                        width: double.infinity,
                        child: RaisedButton(
                          child: const Text(
                            'Change Password',
                            style: TextStyle(fontSize: 15),
                          ),
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(5.0),
                          ),
                          color: hexToColor(
                              AppColors.COLOR_VROOM_THEME),
                          splashColor: Colors.orangeAccent,
                          onPressed: resetPassCall,
                        ),
                      ),
                    ),


                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void resetPassCall() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    } else {
      setState(() {
        _validate = true;
      });
    }
  }
}