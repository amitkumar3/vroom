import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter/constants/AppAssets.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/constants/AppStrings.dart';
import 'package:sample_flutter/constants/EndPoints.dart';

//import 'package:vroom_ap/screens/Dashboard.dart';
import 'package:sample_flutter/tools/Constants.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPassword createState() => _ForgotPassword();
}

class _ForgotPassword extends State<ForgotPassword> {
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  String email;
  bool _passwordVisibility = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _key,
            autovalidate: _validate,
            child: Column(children: <Widget>[
              Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Image(
                      image: AssetImage(AppAssets.ASSET_APP_LOGO),
                      alignment: AlignmentDirectional.center,
                      fit: BoxFit.contain,
                      height: 50,
                    ),
                    SizedBox(
                      height: 40.0,
                    ),
                    Text(
                      "Forgot Password?",
                      style: TextStyle(
                          color: hexToColor(AppColors.COLOR_VROOM_THEME),
                          fontSize: 18.0),
                    ),
                    SizedBox(
                      height: 40.0,
                    ),
                    Text(
                      "Please confirm your email address below and we will\nsend you a reset password link.",
                      style: TextStyle(
                          color: hexToColor(AppColors.COLOR_TEXT_BLACK),
                          fontSize: 14.0),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Container(
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        "Email Address",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#ff3d00")),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        validator: validateEmail,
                        onSaved: (String val) {
                          email = val;
                        },
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      SizedBox(
                        height: 50.0,
                        width: double.infinity,
                        child: RaisedButton(
                          child: const Text('Submit'),
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                          color: hexToColor(AppColors.COLOR_VROOM_THEME),
                          splashColor: Colors.orangeAccent,
                          onPressed: () {
                            callApi();
                          },
                        ),
                      ),
                    ],
                  ))
            ]),
          ),
        ),
      ),
    );
  }

  callApi() async {
    if (_key.currentState.validate()) {
      // No any error in validation
      displayProgressDialog(context);
      _key.currentState.save();
      final body = {"username": email};
      final res = await http.post(EndPoints.userForgetPassword, body: body);
      final response = json.decode(res.body);
      if (response['status'] == 'error') {
        closeProgressDialog(context);
      } else if (response['status'] == 'success') {
        closeProgressDialog(context);
        showCupertinoAlert(AppStrings.TEXT_SUCCESS, response['msg'].toString(),
            AppStrings.TEXT_OKAY, "forgotPassword", context);
        /*Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => Dashboard(),
            ));*/
      }
      print(response['msg']);
    } else {
      setState(() {
        _validate = true;
      });
    }
  }
}
