import 'package:carousel_pro/carousel_pro.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/AppAssets.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/constants/UIAssets.dart';
import 'package:sample_flutter/menus/AboutUs.dart';
import 'package:sample_flutter/menus/FleetService.dart';
import 'package:sample_flutter/menus/GetInsurance.dart';
import 'package:sample_flutter/menus/Loan.dart';
import 'package:sample_flutter/menus/OwnerShip.dart';
import 'package:sample_flutter/menus/PrivacyPolicy.dart';
import 'package:sample_flutter/menus/TermsOfUse.dart';
import 'package:sample_flutter/menus/VehicleService.dart';
import 'package:sample_flutter/menus/WashandPolish.dart';
import 'package:sample_flutter/models/BrowseVehicleModels.dart';
import 'package:sample_flutter/models/Deals.dart';
import 'package:sample_flutter/screens/ChangePassword.dart';
import 'package:sample_flutter/screens/Login.dart';
import 'package:sample_flutter/screens/MyBookmarks.dart';
import 'package:sample_flutter/screens/MyPost.dart';
import 'package:sample_flutter/screens/MyPurchases.dart';
import 'package:sample_flutter/screens/SearchPage.dart';
import 'package:sample_flutter/screens/SellForm.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:sample_flutter/widgets/BrowseVehiclesWidget.dart';
import '../tools/app_tools.dart';
import 'MyAccount.dart';

class Dashboard extends StatefulWidget {
  @override
  _Dashboard createState() => _Dashboard();
}

class _Dashboard extends State<Dashboard> {
  BrowseVehicleModels browseVehicleModel;

  var isLoading = true;
  String user, pass;
  List<Deals> dealsList = List();
  List<Deals> tempList = List();
  String DEALS_IMG_PATH;

  ScrollController _scrollController = new ScrollController();

  final dio = new Dio();

  @override
  void initState() {
    super.initState();
    print('Init State');
    // getCarouselListsApi();
    // getCurrentUser();
    fetchDealsPromotions();
  }

  fetchDealsPromotions() async {
    String dealsApi = "http://vroom.infini.work/api/?responseType=json";
    final response = await dio.get(dealsApi);
    browseVehicleModel =
        BrowseVehicleModels.fromJson(response.data['menudata']['maincats']);
    var list1 = response.data['deals'] as List;

    dealsList.addAll(list1.map((data) => new Deals.fromJson(data)).toList());

    DEALS_IMG_PATH = response.data['dealsImgPath'] + "/";

    setState(() {
      isLoading = false;
    });
  }

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isLoading ? 1.0 : 0.0,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget image_carousel = new Container(
      height: 150.0,
      child: Carousel(
        boxFit: BoxFit.fill,
        images: [
          AssetImage(AppAssets.BANNER_1),
          AssetImage(AppAssets.BANNER_2)
        ],
        autoplay: true,
        dotSize: 3.0,
        dotSpacing: 15.0,
        dotBgColor: Colors.transparent,
      ),
    );

    return new Scaffold(
      appBar: new AppBar(
        elevation: 8.0,
        backgroundColor: Colors.white,
        title: new Image(
          image: AssetImage(AppAssets.ASSET_APP_LOGO),
          alignment: AlignmentDirectional.center,
          fit: BoxFit.contain,
          height: 30,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            // color: Colors.grey,
            color: hexToColor(AppColors.COLOR_ICONS),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => SearchPage()));
            },
          ),
          SizedBox(
            height: 50.0,
            width: 75.0,
            child: IconButton(
                padding: EdgeInsets.all(8.0),
                icon: Image(
                  image: AssetImage(AppAssets.SELL_ICON),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => SellForm()));
                }),
          )
        ],
      ),
      drawer: new Drawer(
          child: new ListView(children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("Amitkumar Prajapati"),
              accountEmail: new Text("pamit2897@gmail.com"),
              currentAccountPicture: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          MyAccount("My Account")));
            },
                child: new CircleAvatar(
                backgroundColor: hexToColor(AppColors.COLOR_VROOM_THEME),
                  child: new Text("A"),
            ),
          ),
        ),

            new ListTile(
              leading: Icon(Icons.local_post_office),
              title: new Text("My Post"),
              onTap: () {
            Navigator.of(context).pop();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => MyPost('My Post')));
          },
        ),
        new ListTile(
          leading: Icon(Icons.shopping_cart),
          title: new Text("Purchases"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        MyPurchases('Purchases')));
          },
        ),
        new ListTile(
          leading: Icon(Icons.bookmark),
          title: new Text("Bookmarks"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        MyBookmarks('Bookmarks')));
          },
        ),
            new ListTile(
              leading: Icon(Icons.lock),
              title: Text("Change Password"),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        ChangePassword("Change Password")));
          },
        ),
        Divider(),
        new ListTile(
          leading: Icon(Icons.clear_all),
          title: new Text("Wash & Polish"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => WashandPolish()));


          },
        ),
        new ListTile(
          leading: Icon(Icons.assignment),
          title: new Text("Ownership Transfer Service"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        OwnerShip("Ownership Transfer Service")));
          },
        ),
        new ListTile(
          leading: Icon(Icons.directions_car),
          title: new Text("Vehicle Inspection Service"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        VehicleService("Vehicle Inspection Service")));
          },
        ),
        new ListTile(
          leading: Icon(Icons.attach_money),
          title: new Text("Get Insurance"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        GetInsurance("Get Insurance")));
          },
        ),
        new ListTile(
          leading: Icon(Icons.work),
          title: new Text("Loan"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => Loan("Loan")));
          },
        ),
        new ListTile(
          leading: Icon(Icons.assessment),
          title: new Text("Fleet Management Service"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        FleetService("Fleet Management Service")));
          },
        ),
        Divider(),
        new ListTile(
          leading: Icon(Icons.branding_watermark),
          title: new Text("Terms of use"),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => TermsOfUse()));
          },
        ),
        new ListTile(
          leading: Icon(Icons.pages),
          title: new Text("About Us"),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => AboutUs()));
          },
        ),
        new ListTile(
          leading: Icon(Icons.lock),
          title: new Text("Privacy Policy"),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => PrivacyPolicy()));
          },
        ),
        new ListTile(
          leading: Icon(Icons.arrow_back),
          title: new Text("Log Out"),
          onTap: () {
            clearDataLocally();
            Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context) => Login()));
          },
        )
      ])),
      body: Container(
        color: hexToColor(AppColors.COLOR_GREY_BACKGROUND),
        child: ListView(
          children: <Widget>[
            // image carousel begins here
            image_carousel,

            /**
             *   new : browse vehicle
             */
            isLoading
                ? Container()
                : BrowseVehiclesWidget(this.browseVehicleModel),

            /**
             *  Deals and Promotions
             */

            Padding(
              padding: EdgeInsets.only(
//                      left: 20.0,
//                      bottom: 10.0,
//                      right: 20.0,
//                      top: 20
                  left: UIAssets.outerSpace,
                  right: UIAssets.outerSpace,
                  top: UIAssets.innerSpace,
                  bottom: UIAssets.innerSpace),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Deals And Promotions',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ],
              ),
            ),

            /**
             *  Listing data
             */

            Container(
              // margin: EdgeInsets.symmetric(vertical: 5.0),
//                padding: EdgeInsets.only(
//                  left: UIAssets.innerSpace
//                  //  left: 5.0
//                ),
              height: 130.0,

              child: _buildDealsListing(),
            ),

            new Padding(
              padding: EdgeInsets.only(
//                      left: 20.0,
//                      bottom: 10.0,
//                      right: 20.0,
//                      top: 5.0
                  left: UIAssets.outerSpace,
                  right: UIAssets.outerSpace,
                  top: UIAssets.outerSpace,
                  bottom: UIAssets.innerSpace),

//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      new Text(
//                        'Popular Nearby',
//                        style: new TextStyle(fontSize: 14.0),
//                      ),
//                    ],
//                  )
            ),

            new Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              // padding: EdgeInsets.only(left: 18.0),
              height: 115.0,
            ),

            SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDealsListing() {
    if (isLoading) {
      return Center(child: _buildProgressIndicator());
    }
    if (dealsList.length != 0) {
      return ListView.builder(
          controller: _scrollController,
          scrollDirection: Axis.horizontal,
          itemCount: dealsList.length + 1,
          itemBuilder: (BuildContext context, int index) {
            if (index == dealsList.length) {
              return _buildProgressIndicator();
            } else {
              return buildDealsPromotionTabs(
                  DEALS_IMG_PATH + dealsList[index].image,
                  dealsList[index].description,
                  dealsList[index].title,
                  "",
                  context);
            }
          });
    } else if (dealsList.length == 0 || dealsList.length == null) {
      return new Padding(
        padding: EdgeInsets.all(8.0),
        child: new Center(
          child: new Text(
            "No Deals are available.",
            style: new TextStyle(
                fontSize: 14.0, color: hexToColor(AppColors.COLOR_APP_GREY)),
          ),
        ),
      );
    }
  }
}
