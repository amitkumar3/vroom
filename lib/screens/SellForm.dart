import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//class SellForm extends StatelessWidget
//{
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text("Sellform"),
//      ),
//
//      body: new Column(
//
//        children: <Widget>[
//
//          SizedBox(height: 20,),
//
//          Center(
//            child: new Text("Sell Form Page"),
//          )
//        ],
//      ),
//    );
//  }
//}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/models/SellFormModel.dart';
import 'package:sample_flutter/screens/Dashboard.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:sample_flutter/widgets/sellform/ImageUploadStep.dart';
import 'package:sample_flutter/widgets/sellform/OtherDetails.dart';
import 'package:sample_flutter/widgets/sellform/SelectableListStep.dart';

class SellForm extends StatefulWidget {
  @override
  _SellFormState createState() => _SellFormState();
}

class _SellFormState extends State<SellForm> {
  int currStep = 0;
  int stepCount = 8;

  PageController _controller = new PageController();
  SellFormModel _model = new SellFormModel();
  bool isLoading = true;
  ValueNotifier v;

  @override
  void initState() {
    super.initState();
    setup();
    v = new ValueNotifier(1);
    _controller.addListener(() {
      print("listen");
      v.value = _controller.page + 1;
    });
  }

  void setup() async {
    await _model.setup();
    if (!_model.authenticated) redirectToLogin(context);
    setState(() {
      isLoading = false;
    });
  }

  void next() async {
    if (!_model.authenticated) redirectToLogin(context);
    if (_controller.page == 7) {
      displayProgressDialog(context);
      String message = await _model.submitListing();
      closeProgressDialog(context);
      _showDialog(message);
    }
    currStep = currStep + 1;
    _controller.nextPage(
        duration: Duration(
          milliseconds: 500,
        ),
        curve: Curves.ease);
  }

  void prev() {
    currStep = currStep - 1;
    _controller.previousPage(
        duration: Duration(
          milliseconds: 500,
        ),
        curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading)
      return Scaffold(body: Center(child: CircularProgressIndicator()));
    var steps = [
      _vehicleCondition(),
      _vehicleType(),
      _brands(),
      _models(),
      _fuelTypes(),
      _variants(),
      OtherDetails(
        model: _model,
        totalSteps: stepCount,
        back: prev,
        next: next,
      ),
      ImageUploadStep(
          model: _model, totalSteps: stepCount, back: prev, next: next)
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text("List Your Vehicle"),

//          leading: IconButton(icon:Icon(Icons.arrow_back),
//            onPressed:() => prev
//          )

        // title: Text("List Your Vehicle"),
      ),
      body: Column(
        children: <Widget>[
          ValueListenableBuilder(
            valueListenable: v,
            builder: (context, v, child) {
              return Container(
                child: LinearProgressIndicator(
                  value: (v) / stepCount,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      hexToColor(AppColors.COLOR_VROOM_THEME)),
                  backgroundColor: hexToColor(AppColors.COLOR_LIGHT_GREY),
                  // valueColor: hexToColor(Constants.COLOR_VROOM_THEME),
                ),
              );
            },
          ),
          Expanded(
            child: PageView.builder(
              controller: _controller,
              itemBuilder: (BuildContext context, int index) {
                return steps[index];
              },
              itemCount: stepCount,
              physics: new NeverScrollableScrollPhysics(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _vehicleCondition() {
    return SelectableListStep(
        list: _model.vehicleConditions,
        onSelected: (value) async {
          await _model.setVehicleCondition(value);
          setState(() {
            print("called");
            next();
          });
        },
        selected: _model.vehicleCondition,
        stepNumber: 1,
        totalSteps: stepCount,
        title: "Select Vehicle Condition",
        back: prev);
  }

  Widget _vehicleType() {
    return SelectableListStep(
        list: _model.vehicleTypes,
        onSelected: (value) async {
          await _model.setVehicleType(value);
          setState(() {
            next();
          });
        },
        selected: _model.vehicleType,
        stepNumber: 2,
        totalSteps: stepCount,
        title: "Select Vehicle Type",
        back: prev);
  }

  Widget _brands() {
    return SelectableListStep(
        list: _model.brands,
        onSelected: (value) async {
          await _model.setBrandName(value);
          setState(() {
            print(value);
            print("called");
            next();
          });
        },
        selected: _model.brandName,
        stepNumber: 3,
        totalSteps: stepCount,
        title: "Select Brand",
        back: prev);
  }

  Widget _models() {
    return SelectableListStep(
        list: _model.models,
        onSelected: (value) async {
          await _model.setModelName(value);
          setState(() {
            print(value);
            print("called");
            next();
          });
        },
        selected: _model.modelName,
        stepNumber: 4,
        totalSteps: stepCount,
        title: "Select Model",
        back: prev);
  }

  Widget _fuelTypes() {
    return SelectableListStep(
        list: _model.fuelTypes,
        onSelected: (value) async {
          await _model.setFuelType(value);
          setState(() {
            next();
          });
        },
        selected: _model.fuelType,
        stepNumber: 5,
        totalSteps: stepCount,
        title: "Select Fuel Type",
        back: prev);
  }

  Widget _variants() {
    return SelectableListStep(
        list: _model.variants,
        onSelected: (value) {
          setState(() {
            _model.variant = value;
            next();
          });
        },
        selected: _model.variant,
        stepNumber: 6,
        totalSteps: stepCount,
        title: "Select Variant",
        back: prev);
  }

  void _showDialog(String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      new MaterialPageRoute(builder: (context) {
                    return Dashboard();
                    // return Dashboard();
                  }));
                },
                child: Text("Ok"),
              )
            ],
            content: Text(message),
          );
        });
  }
}
