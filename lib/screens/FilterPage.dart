import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/models/FilterRequest.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:sample_flutter/widgets/FilterTabContent.dart';
import 'package:sample_flutter/widgets/PriceFilterTabContent.dart';
import 'package:vertical_tabs/vertical_tabs.dart';

class FilterPage extends StatefulWidget {
  final String title;
  final FilterRequest filterRequest;
  final callback;
  final tabWidth = 150.0;

  FilterPage(this.title, this.filterRequest, this.callback);

  @override
  _FilterPage createState() => _FilterPage();
}

class _FilterPage extends State<FilterPage> {
  List<Widget> contents = [];

  @override
  Widget build(BuildContext context) {
    List<Tab> tabs = [];

    var filterNames = widget.filterRequest.getFilterNames();
    contents = [];
    print("buliding whole widget again");
    print(widget.filterRequest.getSelectedFilters());

    tabs.add(Tab(child: Text("Price")));
    contents.add(PriceFilterTabContent(
        widget.filterRequest.getMinPrice(),
        widget.filterRequest.getMaxPrice(),
        widget.filterRequest.getSelectedPriceRange(), (min, max) {
      print("callback price range");

      widget.filterRequest.setSelectedPriceRange([min, max]);
    }));
    for (var key in filterNames.keys) {
      tabs.add(Tab(child: Text(filterNames[key])));
      contents.add(new FilterTabContent(
          title: filterNames[key],
          filterData: widget.filterRequest.getFilter(key),
          selected: widget.filterRequest.getSelected(key),
          callback: (v) {
            widget.filterRequest.setSelected(key, v);
          }));
    }
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                child: VerticalTabs(
                    indicatorColor: hexToColor(AppColors.COLOR_VROOM_THEME),
                    selectedTabBackgroundColor: Theme.of(context).primaryColor,
                    tabsWidth: widget.tabWidth,
                    disabledChangePageFromContentView: true,
                    tabs: tabs,
                    contents: contents),
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: widget.tabWidth,
                    child: RaisedButton(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      child: Text("reset"),
                      onPressed: resetFilter,
                    ),
                  ),
                  Expanded(
                    child: RaisedButton(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      color: hexToColor(AppColors.COLOR_VROOM_THEME),
                      textColor: Colors.white,
                      child: Text("Apply Filters"),
                      onPressed: applyFilter,
                    ),
                  )
                ],
              )
            ],
          ),
        )
      );
    }

  void resetFilter() {
    setState(() {
      print("reetting state");
      widget.filterRequest.reset();
      applyFilter();
    });
  }

  void applyFilter() {
    widget.callback();
    Navigator.pop(context);
  }
}
