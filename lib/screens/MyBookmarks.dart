
import 'package:flutter/material.dart';

class MyBookmarks extends StatefulWidget
{
  final String title;

  MyBookmarks(this.title);

  _MyBookmarks createState() => _MyBookmarks();
}

class _MyBookmarks extends State<MyBookmarks>
{
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(super.widget.title),
      ),
    );
  }
}