import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter/constants/AppAssets.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/screens/OTP.dart';
import 'package:sample_flutter/screens/OTP.dart';
import 'package:sample_flutter/screens/Dashboard.dart';
import 'package:sample_flutter/screens/SignUp.dart';
import 'package:sample_flutter/tools/Constants.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class SetPassword extends StatefulWidget {
  final String fName, lName, email, telephone, accountType, organizationName;

  SetPassword(this.fName, this.lName, this.email, this.telephone,
      this.accountType, this.organizationName)
      : super();

  @override
  _SetPassword createState() => _SetPassword();
}

class _SetPassword extends State<SetPassword> {
  // GlobalKey<FormState> _key = new GlobalKey();

  final _formkey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  //bool _validate = false;
  String pass, cpass;
  bool _passwordVisibility = true;
  bool _cPasswordVisibility = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formkey,
            // autovalidate: _validate,
            child: Column(
              children: <Widget>[
                new Container(
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Image(
                        image: AssetImage(AppAssets.ASSET_APP_LOGO),
                        alignment: AlignmentDirectional.center,
                        fit: BoxFit.contain,
                        height: 50,
                      ),
                      new SizedBox(
                        height: 15.0,
                      ),
                      new Text(
                        "Set Password",
                        style:
                            new TextStyle(color: Colors.black, fontSize: 18.0),
                      ),
                      new SizedBox(
                        height: 5.0,
                      ),
                      new Text(
                        "Please set Password for Login",
                        style:
                            new TextStyle(color: Colors.grey, fontSize: 14.0),
                      ),
                    ],
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new SizedBox(
                        height: 15.0,
                      ),
                      new Text(
                        "Password",
                        style:
                            new TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      new SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: new InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              _passwordVisibility
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                              color: Color(0xFF595959),
                            ),
                            onPressed: () {
                              setState(() {
                                _passwordVisibility ^= true;
                                print('Show/Hide Password Clicked');
                              });
                            },
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        obscureText: _passwordVisibility ? true : false,
                        validator: (String value) {
                          if (value.length <= 5) {
                            return "Password must be at least 6 characters long";
                          } else if (value.isEmpty) {
                            return "Password can't be empty";
                          }
                          return null;
                        },
                        onSaved: (val) => pass = val,
                      ),
                      new SizedBox(
                        height: 15.0,
                      ),
                      new Text(
                        "Confirm Password",
                        style:
                            new TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      new SizedBox(
                        height: 10.0,
                      ),
                      new TextFormField(
                        decoration: new InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              _cPasswordVisibility
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                              color: Color(0xFF595959),
                            ),
                            onPressed: () {
                              setState(() {
                                _cPasswordVisibility ^= true;
                              });
                            },
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        obscureText: _cPasswordVisibility ? true : false,
                        validator: (String value) {
                          if (value.length <= 5) {
                            return "Password must be at least 6 characters long";
                          } else if (value.isEmpty) {
                            return "Password can't be empty";
                          }
                          return null;
                        },
                        onSaved: (val) => cpass = val,
                      ),
                      new SizedBox(
                        height: 10.0,
                      ),
                      new SizedBox(
                        height: 50.0,
                        width: double.infinity,
                        child: new RaisedButton(
                            child: const Text('Submit'),
                            textColor: Colors.white,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5.0)),
                            color: hexToColor(AppColors.COLOR_VROOM_THEME),
                            splashColor: Colors.orangeAccent,
                            onPressed: () {
                              _sendToServer(
                                  widget.fName,
                                  widget.lName,
                                  widget.email,
                                  widget.telephone,
                                  widget.accountType,
                                  widget.organizationName);
                            }),
                      ),
                      new SizedBox(
                        height: 15.0,
                      ),
                      new SizedBox(
                        height: 50.0,
                        width: double.infinity,
                        child: new RaisedButton(
                          child: const Text('Back'),
                          textColor: hexToColor("#000000"),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                          color: Colors.white,
                          splashColor: Colors.orangeAccent,
                          onPressed: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (BuildContext context) => SignUp(),
                              )),
                        ),
                      ),
                      new SizedBox(
                        height: 5.0,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _sendToServer(String fName, String lName, String email, String telephone,
      String accountType, String organizationName) async {
    final form = _formkey.currentState;
    if (form.validate()) {
      // No any error in validation
      _formkey.currentState.save();
      // if (pass != cpass) {
      //  showSnackBar("Passwords don't match", scaffoldKey);
      //} else {
      final body = {
        "email": email,
        "firstname": fName,
        "lastname": lName,
        "telephone": telephone,
        "password": pass,
        "cpassword": cpass,
        "responseType": "json",
        "type": accountType,
        "organization_name": organizationName
      };
      final url = 'https://vroom.infini.work/api/save-register';
      print(body);
      final resp = await http.post(url, body: body);
      final response = json.decode(resp.body);
      if (response != null) {
        print("Response" + response.toString());
        if (response['result']['status'] == 'error') {
          print('Error');
        } else if (response['result']['status'] == "Match") {
          // print(response['result']['msg']);
          showSnackBar(response['result']['msg'], scaffoldKey);
        } else if (response['result']['status'] == 'success') {
          //showSnackBar(message, scaffoldKey)
          showSnackBar(
            response['result']['msg'],
            scaffoldKey,
          );
          // print(response['result']['msg']);

          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => OTP(
                    widget.telephone.toString(),
                    response['result']['userid'],
                    response['result']['otp']),
              ));
        }
      }
    }
  }
/*else {
      //print("error");

     // _validate = true;


      // validation error.

      setState(() {
        _validate = true;
      });
    }*/
}
//}
