import 'package:carousel_pro/carousel_pro.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/constants/AppStrings.dart';
import 'package:sample_flutter/models/FilterRequest.dart';
import 'package:sample_flutter/models/ProductDetailModel.dart';
import 'package:sample_flutter/models/Vehicle.dart';
import 'package:sample_flutter/screens/Login.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:sample_flutter/widgets/AppliedFilters.dart';
import 'package:sample_flutter/widgets/SortWidget.dart';

import 'FilterPage.dart';
import 'VehicleDetailPage.dart';

class VehicleListing extends StatefulWidget {
  final String title, endPoint, sortExt;

  VehicleListing(this.title, this.endPoint, this.sortExt);

  @override
  _VehicleListing createState() => _VehicleListing();
}

class _VehicleListing extends State<VehicleListing> {
  String PROD_IMG_PATH;

  ScrollController _scrollController = new ScrollController();

  final dio = new Dio();
  String sortby;

  var nextPage = "";
  var responseObject;
  bool sort, isUserLoggedIn = false;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
    filterRequest = new FilterRequest();
    _fetchData(widget.sortExt);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _fetchData(widget.sortExt);
      }
    });
  }

  Future getCurrentUser() async {
    String abc = await getStringDataLocally(key: 'telephone');
    isUserLoggedIn = await getBooleanDataLocally("isLoggedIn");
    //print("Telephone: " + abc);
  }

  List<Vehicle> vehicleList = List();
  List<Vehicle> tempList;

  //List<ExteriorColor> exteriorColorList = List();
  //Map<Categories> categoriesList = List();

  Map<int, Category> categoryList = {};
  Map<int, VehicleCondition> vehicleConditionList = {};
  Map<int, ExteriorColor> exteriorColorList = {};
  Map<int, Brands> brandsList = {};
  Map<int, BrandModels> brandsModelList = {};
  Map<int, FuelType> fuelTypeList = {};
  Map<int, Cities> citiesList = {};
  Map<int, BodyType> bodyTypeList = {};
  Map<int, TransmissionData> transmissionDataList = {};
  FilterRequest filterRequest;
  var isLoading = false;
  var intialLoading = true;

  _fetchData(String sortUrl) async {
    if (nextPage == null) return 0;
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      if (nextPage == "") {
        nextPage = widget.endPoint;
      } else if (sort == true) {
        nextPage = widget.endPoint + sortUrl;
      }

      if (filterRequest.getFilterParamsURL() != "") {
        nextPage += "&" + filterRequest.getFilterParamsURL();
      }
      print("final url $nextPage");
      final response = await dio.get(nextPage);
      responseObject = response;

      nextPage = response.data['products']['next_page_url'];
      tempList = new List();

      setState(() {
        isLoading = false;
        vehicleList.addAll(tempList);
      });

      PROD_IMG_PATH = response.data['prodImgPath'] + "/";
      filterRequest.setPriceRange(double.parse(response.data['minp']['min']),
          double.parse(response.data['maxp']['max']));
      for (var i in response.data['exteriorColor'] as List) {
        exteriorColorList[i['id']] = ExteriorColor.fromJson(i);
      }
      filterRequest.addFilter("color_id", "Color", exteriorColorList);

      for (var i in response.data['categories'] as List) {
        categoryList[i['id']] = Category.fromJson(i);
      }
      filterRequest.addFilter("cat", "Category", categoryList);

      for (var i in response.data['mcategories'] as List) {
        vehicleConditionList[i['id']] = VehicleCondition.fromJson(i);
      }
      filterRequest.addFilter(
          "mcat", "Vehicle Condition", vehicleConditionList);

      for (var i in response.data['brands'] as List) {
        brandsList[i['id']] = Brands.fromJson(i);
      }
      filterRequest.addFilter("makes", "Brand", brandsList);

      for (var i in response.data['brandModels'] as List) {
        brandsModelList[i['id']] = BrandModels.fromJson(i);
      }
      filterRequest.addFilter("models", "Model", brandsModelList);

      for (var i in response.data['fuelType'] as List) {
        fuelTypeList[i['id']] = FuelType.fromJson(i);
      }
      filterRequest.addFilter("fuel_type", "Fuel Type", fuelTypeList);

      for (var i in response.data['cities'] as List) {
        citiesList[i['id']] = Cities.fromJson(i);
      }
      filterRequest.addFilter("city", "Location", citiesList);

      for (var i in response.data['bodyType'] as List) {
        bodyTypeList[i['id']] = BodyType.fromJson(i);
      }
      filterRequest.addFilter("body_type", "Vehicle Type", bodyTypeList);

      for (var i in response.data['transmissionData'] as List) {
        transmissionDataList[i['id']] = TransmissionData.fromJson(i);
      }
      filterRequest.addFilter(
          "transmission", "Transmission", transmissionDataList);

      var list1 = response.data['products']['data'] as List;

      vehicleList.addAll(list1
          .map((data) => new Vehicle.fromJson(
              data,
              exteriorColorList,
              brandsList,
              brandsModelList,
              fuelTypeList,
              citiesList,
              bodyTypeList,
              transmissionDataList,
              vehicleConditionList))
          .toList());

      PROD_IMG_PATH = response.data['prodImgPath'] + "/";

      setState(() {
        isLoading = false;
        intialLoading = false;
      });
    } else {
      throw Exception('Failed to load photos');
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  GlobalKey<FormState> _key = new GlobalKey();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _validate = false;
  String phone;

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isLoading ? 1.0 : 0.0,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget _buildVehicleListing() {
    if (intialLoading) {
      return Center(child: _buildProgressIndicator());
    }
    if (vehicleList.length != 0) {
      return ListView.builder(
          controller: _scrollController,
          itemCount: vehicleList.length + 1,
          itemBuilder: (BuildContext context, int index) {
            if (index == vehicleList.length) {
              return _buildProgressIndicator();
            } else {
              var images = vehicleList[index]
                  .images
                  .map((image) => NetworkImage(
                        PROD_IMG_PATH + vehicleList[index].images[0].imageName,
                      ))
                  .toList();

              return Padding(
                padding: EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: () =>
                      showVehicleDetail(vehicleList[index], images, context),
                  child: Card(
                    clipBehavior: Clip.antiAlias,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          height: 200.0,
                          child: Carousel(
                            images: images,
                            boxFit: BoxFit.cover,
                            autoplay: false,
                            showIndicator: images.length == 1 ? false : true,
                            dotSize: 3.0,
                            dotSpacing: 15.0,
                            dotBgColor: Colors.transparent,
                            overlayShadowSize: 5.0,
                            overlayShadow: true,
                            overlayShadowColors:
                                hexToColor(AppColors.COLOR_TEXT_BLACK),
                            dotColor: hexToColor(AppColors.COLOR_VROOM_THEME),
                            indicatorBgPadding: 10.0,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 8.0, left: 8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                vehicleList[index].product_year.toString() +
                                    " " +
                                    vehicleList[index].product_name,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.grey[800],
                                ),
                              ),
                              newUsedContainer(
                                vehicleList[index].vehicleCondition.toString(),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            "৳" +
                                " " +
                                amountFormatted(
                                    vehicleList[index].product_price),
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey[800],
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 8.0, right: 8.0, top: 10.0, bottom: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              buildListImage(
                                  0xe906, vehicleList[index].product_variant),
                              buildListImage(
                                  0xe907, vehicleList[index].product_location),
                              buildListImage(
                                  0xe909,
                                  vehicleList[index]
                                          .product_kilometers_driven
                                          .toString() +
                                      " kms"),
                              buildListImage(
                                  0xe908,
                                  vehicleList[index].product_no_of_owners +
                                      " Owner"),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
          });
    } else if (vehicleList.length == 0 || vehicleList.length == null) {
      return new Padding(
        padding: EdgeInsets.all(8.0),
        child: new Center(
          child: new Text(
            "No Results Found.",
            style: new TextStyle(
                fontSize: 13.0, color: hexToColor(AppColors.COLOR_APP_GREY)),
          ),
        ),
      );
    }
  }

  void _showSortDialog() {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
          title: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Select Sort",
                  textAlign: TextAlign.center,
                ),
              ),
              Divider()
            ],
          ),
          titlePadding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
          contentPadding: EdgeInsets.all(0.0),
          content: SortWidget({
            "&sortOrder=1": "Popularity",
            "&sortOrder=2": "Price(High To Low)",
            "&sortOrder=3": "Price(Low To High)",
            "&sortOrder=4": "Newest Listing",
          }, (value) {
            Navigator.pop(context);
            sort = true;
            intialLoading = true;
            sortby = value;
            vehicleList.clear();
            _fetchData(value);
          }, sortby)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        bottomNavigationBar: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: 40),
          child: new SizedBox.expand(
            child: Row(
              children: <Widget>[
                buildBottomButtons(Icons.sort, "Sort", _showSortDialog),
                buildBottomButtons(Icons.filter_list, "Filter", showFilter),
              ],
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            AppliedFilters(filterRequest, applyFilter),
            Expanded(child: _buildVehicleListing()),
          ],
        ));
  }

  Expanded buildBottomButtons(IconData icon, String text, Function onPressed) {
    return Expanded(
      child: RaisedButton.icon(
        icon: Icon(
          icon,
          color: hexToColor(AppColors.COLOR_DARK_BLACK),
        ),
        color: Colors.white,
        label: Text(
          text,
          style: new TextStyle(color: hexToColor(AppColors.COLOR_TEXT_BLACK)),
        ),
        splashColor: hexToColor(AppColors.COLOR_VROOM_THEME),
        onPressed: onPressed,
      ),
    );
  }

  void showFilter() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => FilterPage(
            AppStrings.TEXT_FILTER_TITLE, filterRequest, applyFilter),
      ),
    );
  }

  void applyFilter() {
    sort = true;
    intialLoading = true;
    nextPage = "";
    vehicleList.clear();
    _fetchData(sortby);
  }

  void showVehicleDetail(Vehicle vehicle, images, BuildContext context) {
    String title =
        vehicle.product_name + ", " + vehicle.product_year.toString();
    String price = vehicleDetailValue(vehicle.product_price);
    String owner = vehicleDetailValue(vehicle.product_no_of_owners);
    String token = vehicleDetailValue(vehicle.token_discount.toString());
    String year = vehicleDetailValue(vehicle.product_year.toString());
    String ext_color = vehicleDetailValue(vehicle.exteriorColor);
    String brand = vehicleDetailValue(vehicle.brands);
    String model_name = vehicleDetailValue(vehicle.brandModels);
    String fuel_type = vehicleDetailValue(vehicle.fuelType);
    String location = vehicleDetailValue(vehicle.cities);
    String body_type = vehicleDetailValue(vehicle.bodyType);
    String transmission_type = vehicleDetailValue(vehicle.transmissionData);
    String vehicle_Condition = vehicleDetailValue(vehicle.vehicleCondition);
    if (isUserLoggedIn == null || isUserLoggedIn == false) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => Login(),
          ));
    } else {
      Navigator.push(context,
          MaterialPageRoute(builder: (BuildContext context) {
        return VehicleDetailPage(
            title,
            price,
            owner,
            token,
            images,
            year,
            ext_color,
            brand,
            model_name,
            fuel_type,
            location,
            body_type,
            transmission_type,
            vehicle_Condition);
      }));
    }
  }
}
