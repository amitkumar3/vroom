import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sample_flutter/constants/AppAssets.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/screens/ChangePassword.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class MyAccount extends StatefulWidget {
  final String title;

  MyAccount(this.title);

  _MyAccount createState() => _MyAccount();
}

class _MyAccount extends State<MyAccount> {

  GlobalKey<FormState> _key = GlobalKey();

  bool _validate = false;

  String fName, lName, email, mobile;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(super.widget.title),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(8.0),
          child: Container(
            child: Column(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      child: Form(
                        key: _key,
                        autovalidate: _validate,
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[

                              Container(
                                margin: EdgeInsets.only(bottom: 20.0),
                                child: Text(
                                  "Update Profile",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16),
                                ),
                                padding: EdgeInsets.only(
                                    left: 8.0, right: 8.0, top: 8.0),
                                alignment: Alignment.centerLeft,
                              ),

                              Container(
                                padding: EdgeInsets.only(top: 30, bottom: 30),
                                alignment: Alignment.center,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[

                                    SvgPicture.asset(AppAssets.EDIT_PROFILE_LOGO,
                                      height: 60,
                                      fit: BoxFit.contain,
                                      alignment: Alignment.center,
                                    ),
                                  ],
                                ),
                              ),


                              /**
                               *  First Name field
                               */

                              Container(
                                padding: EdgeInsets.only(
                                  left: 8.0,
                                  right: 8.0,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        "First Name *",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14.0),
                                      ),
                                      alignment: Alignment.centerLeft,
                                      padding:
                                          EdgeInsets.only(top: 20, bottom: 8.0),
                                    ),
                                    TextFormField(
                                      decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: hexToColor(AppColors
                                                    .COLOR_VROOM_THEME)
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: hexToColor("#e0e0e0")),
                                          ),
                                          contentPadding: EdgeInsets.symmetric(
                                              vertical: 10.0,
                                              horizontal: 10.0)),
                                      keyboardType: TextInputType.text,
                                      validator: validateName,
                                      onSaved: (String val) {
                                        fName = val;
                                      },
                                    ),
                                  ],
                                ),
                              ),

                              /**
                               *  Last Name field
                               */
                              Container(
                                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        "Last Name *",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14.0),
                                      ),
                                      alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.only(
                                          top: 10.0, bottom: 8.0),
                                    ),
                                    TextFormField(
                                      decoration: InputDecoration(

                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: hexToColor(AppColors
                                                    .COLOR_VROOM_THEME)),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: hexToColor("#e0e0e0")),
                                          ),
                                          contentPadding: EdgeInsets.symmetric(
                                              vertical: 10.0,
                                              horizontal: 10.0)),
                                      keyboardType: TextInputType.text,
                                      validator: validateName,
                                      onSaved: (String val) {
                                        lName = val;
                                      },
                                    ),
                                  ],
                                ),
                              ),

                              /**
                               *  mobile field
                               */
                              Container(
                                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        "Mobile *",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14.0),
                                      ),
                                      alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.only(
                                          top: 10.0, bottom: 8.0),
                                    ),
                                    TextFormField(
                                      decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: hexToColor(AppColors
                                                    .COLOR_VROOM_THEME)),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: hexToColor("#e0e0e0")),
                                          ),
                                          contentPadding: EdgeInsets.symmetric(
                                              vertical: 10.0,
                                              horizontal: 10.0)),
                                      keyboardType: TextInputType.phone,
                                      validator: validateMobile,
                                      onSaved: (String val) {
                                        mobile = val;
                                      },
                                    ),
                                  ],
                                ),
                              ),

                              /**
                               *  Email field
                               */
                              Container(
                                padding: EdgeInsets.only(
                                    left: 8.0, right: 8.0, bottom: 15.0),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        "Email *",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14.0),
                                      ),
                                      alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.only(
                                          top: 10.0, bottom: 8.0),
                                    ),
                                    TextFormField(
                                      decoration: InputDecoration(

                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: hexToColor(AppColors
                                                    .COLOR_VROOM_THEME)),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: hexToColor("#e0e0e0")),
                                          ),
                                          contentPadding: EdgeInsets.symmetric(
                                              vertical: 10.0,
                                              horizontal: 10.0)),
                                      keyboardType: TextInputType.emailAddress,
                                      validator: validateEmail,
                                      onSaved: (String val) {
                                        email = val;
                                      },
                                    ),
                                  ],
                                ),
                              ),

                              /**
                               *   Submit button
                               */

                              Container(
                                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 15.0),
                                child: SizedBox(
                                  height: 50.0,
                                  width: double.infinity,
                                  child: RaisedButton(
                                    child: const Text(
                                      'Update',
                                      style: TextStyle(fontSize: 15),
                                    ),
                                    textColor: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    color:
                                        hexToColor(AppColors.COLOR_VROOM_THEME),
                                    splashColor: Colors.orangeAccent,
                                    onPressed: updateProfileCall,
                                  ),
                                ),
                              ),


                              /**
                               *   Change password
                               */
                              Container(
                                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                                child: SizedBox(
                                  height: 50.0,
                                  width: double.infinity,
                                  child: RaisedButton(
                                    child: const Text(
                                      'Change Password',
                                      style: TextStyle(fontSize: 15),
                                    ),
                                    textColor: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    color:
                                    hexToColor(AppColors.COLOR_VROOM_THEME),
                                    splashColor: Colors.orangeAccent,
                                    onPressed: () {
                                      Navigator.push(context, MaterialPageRoute(
                                        builder: (BuildContext context) => ChangePassword('Change Password'),
                                      ));
                                    },
                                  ),
                                ),
                              ),

                            ],
                          ),
                        )
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        )
    );
  }


  void updateProfileCall() {
    if (_key.currentState.validate()) {
      _key.currentState.save();
    } else {
      setState(() {
        _validate = true;
      });
    }
  }
}
