import 'package:carousel_pro/carousel_pro.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/constants/AppStrings.dart';
import 'package:sample_flutter/constants/EndPoints.dart';
import 'package:sample_flutter/models/ProductDetailModel.dart';
import 'package:sample_flutter/models/Vehicle.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'VehicleDetailPage.dart';

class SearchPage extends StatefulWidget {
  //String otp, userId;

  @override
  _SearchPage createState() => _SearchPage();
}

class _SearchPage extends State<SearchPage> {
  String PROD_IMG_PATH;

  ScrollController _scrollController = new ScrollController();

  TextEditingController editingController =
      TextEditingController(); // text editing controller

  final dio = new Dio();

  var nextPage = "";

  List<Vehicle> vehicleList = List();
  List<Vehicle> tempList;

  // List<ExteriorColor> exteriorColorList = List();
  // Map<Categories> categoriesList = List();
  Map<int, ExteriorColor> exteriorColorList = {};
  Map<int, Brands> brandsList = {};
  Map<int, BrandModels> brandsModelList = {};
  Map<int, FuelType> fuelTypeList = {};
  Map<int, Cities> citiesList = {};
  Map<int, BodyType> bodyTypeList = {};
  Map<int, TransmissionData> transmissionDataList = {};
  Map<int, VehicleCondition> vehicleCondition = {};
  var isLoading = false;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  Future getCurrentUser() async {
    String id = await getStringDataLocally(key: 'id');
    return id;
  }

  GlobalKey<FormState> _key = new GlobalKey();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _validate = false;

  // bool _search = false;
  bool _search = false;
  String phone;

  void filterSearchResults(String searchText) {
    _fetchData(searchText);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _fetchDataWhenScroll(searchText);
      }
    });
  }

  _fetchDataWhenScroll(String searchText) async {
    if (nextPage == null) return;
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      nextPage = EndPoints.getSearchCarListing + searchText;

      final response = await dio.get(nextPage);

      nextPage = response.data['products']['next_page_url'];
      tempList = new List();
      _mapMethod(response);

      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load photos');
    }
  }

  _fetchData(String searchText) async {
    //if (nextPage == null) return;
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      nextPage = EndPoints.getSearchCarListing + searchText;

      final response = await dio.get(nextPage);

      nextPage = response.data['products']['next_page_url'];
      tempList = new List();

      _mapMethod(response);

      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load photos');
    }
  }

  _mapMethod(var response) async {
    setState(() {
      isLoading = false;
      vehicleList.clear();
      vehicleList.addAll(tempList);
    });

    PROD_IMG_PATH = response.data['prodImgPath'] + "/";

    for (var i in response.data['exteriorColor'] as List) {
      exteriorColorList[i['id']] = ExteriorColor.fromJson(i);
    }
    for (var i in response.data['brands'] as List) {
      brandsList[i['id']] = Brands.fromJson(i);
    }

    for (var i in response.data['brandModels']


    as List) {
      brandsModelList[i['id']] = BrandModels.fromJson(i);
    }

    for (var i in response.data['fuelType'] as List) {
      fuelTypeList[i['id']] = FuelType.fromJson(i);
    }

    for (var i in response.data['cities'] as List) {
      citiesList[i['id']] = Cities.fromJson(i);
    }

    for (var i in response.data['bodyType'] as List) {
      bodyTypeList[i['id']] = BodyType.fromJson(i);
    }

    for (var i in response.data['transmissionData'] as List) {
      transmissionDataList[i['id']] = TransmissionData.fromJson(i);
    }

    for (var i in response.data['mcategories'] as List) {
      vehicleCondition[i['id']] = VehicleCondition.fromJson(i);
    }

    //print(exteriorColorList[1].color_name);
    //print(brandsList[5].brand_name);
    //print(brandsModelList[13].model_name);
    //print(fuelTypeList[2].fuel_type);
    //print(citiesList[2].location_name);
    //print(bodyTypeList[1].body_type_name);
    //print(transmissionDataList[12].transmission_type);

    var list1 = response.data['products']['data'] as List;

    vehicleList.addAll(list1
        .map((data) => new Vehicle.fromJson(
            data,
            exteriorColorList,
            brandsList,
            brandsModelList,
            fuelTypeList,
            citiesList,
            bodyTypeList,
            transmissionDataList,
            vehicleCondition))
        .toList());

    //print("ASDASAD " + vehicleList[1].exteriorColor.color_name);
    //print("ASDASAD " + vehicleList[1].brands.brand_name);
    //print("ASDASAD " + vehicleList[1].brandModels.model_name);
    //print("ASDASAD " + vehicleList[1].fuelType.fuel_type);
    //print("ASDASAD " + vehicleList[1].cities.location_name);
    //print("ASDASAD " + vehicleList[1].bodyType.body_type_name);
    //print("ASDASAD " + vehicleList[1].transmissionData.transmission_type);

    PROD_IMG_PATH = response.data['prodImgPath'] + "/";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Listing"),
      ),
      body: Container(
        color: hexToColor(AppColors.COLOR_GREY_BACKGROUND),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // START -------- search text box ------------
            new Container(
              padding:
                  new EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 10),
              color: hexToColor(AppColors.COLOR_TEXT_WHITE),
              height: 50.0,

              child: TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                    BorderSide(color: hexToColor("#e0e0e0")),
                  ),
                  hintText: "Find Vehicles, Services and more",
                  hintStyle: TextStyle(
                    fontSize: 14.0,
                    color: hexToColor(AppColors.COLOR_APP_GREY)
                  ),
                  contentPadding: EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  
                  prefixIcon: Icon(Icons.search,
                  color: hexToColor(AppColors.COLOR_APP_GREY),)
                ),
                

                validator: validateSearch,
                onChanged: (value) {
                  _search = true;
                  filterSearchResults(value);
                },
              ),

//              child: TextField(
//                onSubmitted: (value) {
//                  _search = true;
//                  filterSearchResults(value);
//                } ,
//                textInputAction: TextInputAction.go,
//                autofocus: false,
//                controller: editingController,
//                decoration: new InputDecoration(
//                  focusedBorder: OutlineInputBorder(
//                    borderSide:
//                        BorderSide(color: hexToColor(AppColors.COLOR_APP_GREY)),
//                  ),
//                  enabledBorder: OutlineInputBorder(
//                    borderSide:
//                        BorderSide(color: hexToColor(AppColors.COLOR_APP_GREY)),
//                  ),
//                  hintText: "Find Vehicles, Services and more",
//                  contentPadding: EdgeInsets.all(10.0),
//                  hintStyle: TextStyle(
//                      fontSize: 14.0,
//                      color: hexToColor(AppColors.COLOR_APP_GREY)),
//                  prefixIcon: Icon(
//                    Icons.search,
//                    color: hexToColor(AppColors.COLOR_APP_GREY),
//                  ),
//                ),
//              ),

            ),

            // ------------------ END -------------

            _buildEmptyState(),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  /*
   *  Progress Bar dialog
   */
  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isLoading ? 1.0 : 0.0,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget _buildEmptyState() {
    if (vehicleList.length > 0) {
      return Expanded(
        child: Container(
          color: hexToColor(AppColors.COLOR_GREY_BACKGROUND),
          child: ListView.builder(
              shrinkWrap: true,
              controller: _scrollController,
              itemCount: vehicleList.length + 1,
              itemBuilder: (BuildContext context, int index) {
                if (index == vehicleList.length) {
                  return _buildProgressIndicator();
                } else {
                  var images = vehicleList[index]
                      .images
                      .map((image) => NetworkImage(
                            PROD_IMG_PATH +
                                vehicleList[index].images[0].imageName,
                          ))
                      .toList();
                  return Padding(
                    padding: EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        String title = vehicleList[index].product_name +
                            ", " +
                            vehicleList[index].product_year.toString();
                        String price = vehicleDetailValue(
                            vehicleList[index].product_price);
                        String owner = vehicleDetailValue(
                            vehicleList[index].product_no_of_owners);
                        String token = vehicleDetailValue(
                            vehicleList[index].token_discount.toString());
                        String year = vehicleDetailValue(
                            vehicleList[index].product_year.toString());
                        String ext_color = vehicleDetailValue(
                            vehicleList[index].exteriorColor.name);
                        String brand =
                            vehicleDetailValue(vehicleList[index].brands.name);
                        String model_name = vehicleDetailValue(
                            vehicleList[index].brandModels.name);
                        String fuel_type = vehicleDetailValue(
                            vehicleList[index].fuelType.name);
                        String location =
                            vehicleDetailValue(vehicleList[index].cities.name);
                        String body_type = vehicleDetailValue(
                            vehicleList[index].bodyType.name);
                        String transmission_type = vehicleDetailValue(
                            vehicleList[index].transmissionData.name);
                        String vehicle_condition = vehicleDetailValue(
                            vehicleList[index].vehicleCondition.name);

                        Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return VehicleDetailPage(
                              title,
                              price,
                              owner,
                              token,
                              images,
                              year,
                              ext_color,
                              brand,
                              model_name,
                              fuel_type,
                              location,
                              body_type,
                              transmission_type,
                              vehicle_condition);
                        }));
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              height: 200.0,
                              child: Carousel(
                                images: images,
                                boxFit: BoxFit.cover,
                                autoplay: false,
                                showIndicator:
                                    images.length == 1 ? false : true,
                                dotSize: 3.0,
                                dotSpacing: 15.0,
                                dotBgColor: Colors.transparent,
                                overlayShadowSize: 5.0,
                                overlayShadow: true,
                                overlayShadowColors:
                                    hexToColor(AppColors.COLOR_TEXT_BLACK),
                                dotColor:
                                    hexToColor(AppColors.COLOR_VROOM_THEME),
                                indicatorBgPadding: 10.0,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                vehicleList[index].product_year.toString() +
                                    " " +
                                    vehicleList[index].product_name,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.grey[800],
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                "৳" +
                                    " " +
                                    amountFormatted(
                                        vehicleList[index].product_price),
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[800],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0,
                                  right: 8.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  buildListImage(0xe906,
                                      vehicleList[index].product_variant),
                                  buildListImage(0xe907,
                                      vehicleList[index].product_location),
                                  buildListImage(
                                      0xe909,
                                      vehicleList[index]
                                              .product_kilometers_driven
                                              .toString() +
                                          " kms"),
                                  buildListImage(
                                      0xe908,
                                      vehicleList[index].product_no_of_owners +
                                          " Owner"),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                }),
              ),
            );
          }
        else if (vehicleList.isEmpty) {
           return Padding(
              padding: EdgeInsets.all(20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _buildProgressIndicator(),
                    _search
                    ? buildSuggestionsText(AppStrings.TEXT_NO_RESULTS_FOUND, 24.0,
                      AppColors.COLOR_TEXT_BLACK)

                  : SizedBox(
                      height: 0.0,
                    ),

              // Search suggestions
//              Container(
//                child: Padding(
//                  padding: EdgeInsets.only(left: 8.0, top: 15),
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      buildSuggestionsText(AppStrings.TEXT_SUGGESTIONS, 18.0,
//                          AppColors.COLOR_TEXT_BLACK)
//                      ,
//                      buildSuggestionsSubText(
//                          AppStrings.TEXT_SUGGESTION_POINT_1,
//                          14.0,
//                          AppColors.COLOR_TEXT_BLACK),
//                      buildSuggestionsSubText(
//                          AppStrings.TEXT_SUGGESTION_POINT_2,
//                          14.0,
//                          AppColors.COLOR_TEXT_BLACK),
//                      buildSuggestionsSubText(
//                          AppStrings.TEXT_SUGGESTION_POINT_3,
//                          14.0,
//                          AppColors.COLOR_TEXT_BLACK),
//                      buildSuggestionsSubText(
//                          AppStrings.TEXT_SUGGESTION_POINT_4,
//                          14.0,
//                          AppColors.COLOR_TEXT_BLACK),
//                    ],
//                  ),
//                ),
//              ),


                    /**
                     *  Suggestions
                      */

             Container(
               child: Padding(
                 padding: EdgeInsets.only(left: 8.0, top: 15),
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                     buildSuggestionsText(AppStrings.TEXT_SUGGESTIONS, 18.0,
                         AppColors.COLOR_TEXT_BLACK),
                     buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_1, 14.0,
                         AppColors.COLOR_TEXT_BLACK),
                     buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_2, 14.0,
                         AppColors.COLOR_TEXT_BLACK),
                     buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_3, 14.0,
                         AppColors.COLOR_TEXT_BLACK),
                     buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_4, 14.0,
                         AppColors.COLOR_TEXT_BLACK),
                   ],
                 ),
               ),
             )

                  ],
          )
        );
      }

//    Container(
//      child: Padding(
//        padding: EdgeInsets.only(left: 8.0, top: 15),
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.start,
//          children: <Widget>[
//            buildSuggestionsText(AppStrings.TEXT_SUGGESTIONS, 18.0,
//                AppColors.COLOR_TEXT_BLACK),
//            buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_1, 14.0,
//                AppColors.COLOR_TEXT_BLACK),
//            buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_2, 14.0,
//                AppColors.COLOR_TEXT_BLACK),
//            buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_3, 14.0,
//                AppColors.COLOR_TEXT_BLACK),
//            buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_4, 14.0,
//                AppColors.COLOR_TEXT_BLACK),
//          ],
//        ),
//      ),
//    );



//              buildSuggestionsText(
//                  AppStrings.TEXT_SUGGESTIONS, 18.0, AppColors.COLOR_TEXT_BLACK),

//              Visibility(
//                visible: false,
//                child: Container(
//                  child: Padding(
//                    padding: EdgeInsets.only(left: 8.0, top: 15),
//                    child: Column(
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        buildSuggestionsText(AppStrings.TEXT_SUGGESTIONS,
//                            18.0, AppColors.COLOR_TEXT_BLACK),
//                        buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_1,
//                            14.0, AppColors.COLOR_TEXT_BLACK),
//                        buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_2,
//                            14.0, AppColors.COLOR_TEXT_BLACK),
//                        buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_3,
//                            14.0, AppColors.COLOR_TEXT_BLACK),
//                        buildSuggestionsSubText(AppStrings.TEXT_SUGGESTION_POINT_4,
//                            14.0, AppColors.COLOR_TEXT_BLACK),
//                    ],
//                  ),
//                ),
//            ),
//              ),

//          ],
//        )
//      );
//    }
  }
}
