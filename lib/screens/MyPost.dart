
import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class MyPost extends StatefulWidget
{

  final String title;

  MyPost(this.title);

  _MyPost createState() => _MyPost();
}

class _MyPost extends State<MyPost>
{

  bool _isHidden = true;

  void _toggleVisibility(){
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(super.widget.title),
      ),

      body: Container(
        child: Column(
          children: <Widget>[

            Text(
              "LOGIN",
              style: TextStyle(
                  fontSize: 32.0,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).primaryColor
              ),
            ),
            SizedBox(height: 40.0,),
            buildTextField("Email"),
            SizedBox(height: 20.0,),
            buildTextField("Password"),
            SizedBox(height: 20.0,),
          ],
        ),
      ),

    );
  }

  Widget buildTextField(String hintText){
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: TextFormField(

        decoration: InputDecoration(

          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
          ),

//          prefixIcon: hintText == "Email" ? Icon(Icons.email) : Icon(Icons.lock),
//        suffixIcon: hintText == "Password" ? IconButton(
//          onPressed: _toggleVisibility,
//          icon: _isHidden ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
//        ) : null,
//      ),

          enabledBorder: OutlineInputBorder(
            borderSide:
            BorderSide(color: hexToColor("#e0e0e0")
            ),
          ),
          contentPadding: EdgeInsets.symmetric(
              vertical: 10.0, horizontal: 10.0),


          prefixIcon: hintText == "Email" ? Icon(Icons.email) : Icon(Icons.lock),
          suffixIcon: hintText == "Password" ? IconButton(
            onPressed: _toggleVisibility,
            icon: _isHidden ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
          ) : null,
        ),

        obscureText: hintText == "Password" ? _isHidden : false,

        ),

//      decoration: InputDecoration(
//        hintText: hintText,
//        hintStyle: TextStyle(
//          color: Colors.grey,
//          fontSize: 16.0,
//        ),
//        border: OutlineInputBorder(
//          borderRadius: BorderRadius.circular(20.0),
//        ),
//        prefixIcon: hintText == "Email" ? Icon(Icons.email) : Icon(Icons.lock),
//        suffixIcon: hintText == "Password" ? IconButton(
//          onPressed: _toggleVisibility,
//          icon: _isHidden ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
//        ) : null,
//      ),



//        obscureText: hintText == "Password" ? _isHidden : false,


      );

  }


}
