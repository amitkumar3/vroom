import 'package:firebase_auth/firebase_auth.dart' as prefix0;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as JSON;

enum AuthProvider { facebook, google, vroom }

class AuthResult {
  AuthProvider provider;
  bool success;
  String error;
  String token;
}

class Auth {


  // Facebook Login

  static Future<AuthResult> loginWithFacebook() async {
    AuthResult authResult = new AuthResult();
    authResult.provider = AuthProvider.facebook;

    final facebookLogin = FacebookLogin();
    final loginResult = await facebookLogin.logIn(['email']);
    switch (loginResult.status) {
      case FacebookLoginStatus.loggedIn:
        authResult.success = true;
        authResult.token = loginResult.accessToken.token;
        break;

      case FacebookLoginStatus.cancelledByUser:
        authResult.error = "Cancelled by user.";
        break;

      case FacebookLoginStatus.error:
        authResult.error = loginResult.errorMessage;
        break;
    }
    return authResult;
  }

  static Future<User> getFacebookUserDetails(String token) async {
    var fields = 'fields=id,name,first_name,last_name,middle_name,email,gender,about';
    var graphUrl = 'https://graph.facebook.com/v2.12/me?$fields&access_token=$token';
    final graphResponse = await http.get(graphUrl);
    final responseJson = JSON.jsonDecode(graphResponse.body);
    if (responseJson["error"] != null) {
      // TODO: - Handle error
      return null;
    } else{
      User user = new User.withFacebook(responseJson);
      return user;
    }
  }

  // Google Login
//  static Future<AuthResult> loginWithGoogle() async {
//    AuthResult authResult = new AuthResult();
//    authResult.provider = AuthProvider.google;
//
//    final googleLogin = GoogleSignIn();
//    final loginResult = await googleLogin.signIn();
//    switch (loginResult.status) {
//      case FacebookLoginStatus.loggedIn:
//        authResult.success = true;
//        authResult.token = loginResult.accessToken.token;
//        break;
//
//      case FacebookLoginStatus.cancelledByUser:
//        authResult.error = "Cancelled by user.";
//        break;
//
//      case FacebookLoginStatus.error:
//        authResult.error = loginResult.errorMessage;
//        break;
//    }
//
//
//    return authResult;
//  }


//  static Future<AuthResult> loginWithGoogle() async {
//    AuthResult authResult = new AuthResult();
//
//
//    authResult.provider = AuthProvider.google;
//
//    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
//    final GoogleSignIn _googleSignIn = new GoogleSignIn();
//
//    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
//    final googleAuth = await googleSignInAccount.authentication;
//    final googleAuthCred = GoogleAuthProvider.getCredential(
//        idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);
//    final user = await _firebaseAuth.signInWithCredential(googleAuthCred);
//
//    print(user.user.displayName);
//    print(user.user.email);
//
////    final googleLogin = GoogleSignIn();
////    final loginResult1 = await googleLogin.signIn();
//  }

}

class User {
  String id;
  String firstName;
  String lastName;
  String profilePicture;
  String name;
 // String familyName;
  String gender;
  String email;
  String middleName;

  AuthProvider provider;

  User() {
    provider = AuthProvider.vroom;
  }

  User.withGoogle(prefix0.AuthResult result) {
    provider = AuthProvider.google;
    email = result.user.email;
  }

  User.withFacebook(Map<String, dynamic> json) {
    provider = AuthProvider.facebook;
    print("fb profile: $json");
    id = json["id"];
    name = json["name"];
    profilePicture = json["profile_pic"];
    email = json["email"];
    gender = json["gender"];
    firstName = json["fisrt_name"];
    lastName = json["last_name"];
    middleName = json["middle_name"];
  }
}