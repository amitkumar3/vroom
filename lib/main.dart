import 'package:flutter/material.dart';
import 'package:sample_flutter/screens/Dashboard.dart';
import 'package:sample_flutter/screens/Login.dart';
import 'package:sample_flutter/screens/OTP.dart';
import 'package:sample_flutter/screens/SetPassword.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // LocalStorage.setValues();
    //  LocalStorage.getValues();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Vroom',
      theme: ThemeData(primaryColor: Colors.white, fontFamily: 'Lato'),
      //home: SecondPage(),
      //  home: new SetPassword("Amitkumar", "Prajapati", "pamit@gmail.com", "8108377993", "Individual", "N/A"),
     // home: Dashboard(),
      home: Dashboard(),
      // home: new OTP("8108377993", "201", "4654"),
    );
  }


}
