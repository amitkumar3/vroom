import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class SortWidget extends StatelessWidget {
  final Map dropdowns;
  final onChanged;
  final selected;

  SortWidget(this.dropdowns, this.onChanged, this.selected);

  @override
  Widget build(BuildContext context) {
    List<Widget> list = [];

    for (var i in dropdowns.keys) {
      Widget trailing;
      if (selected == i) {
        print("i is selected");
        trailing = Icon(Icons.check);
      }
      list.add(Column(
        children: <Widget>[
          ListTile(
            onTap: () => onChanged(i),
            title: Text(
              dropdowns[i],
              style: TextStyle(
                  color: hexToColor(AppColors.COLOR_TEXT_BLACK),
                  fontSize: 18.0),
            ),
            trailing: trailing,
          ),
        ],
      ));
    }
    return ListView(
      shrinkWrap: true, //just set this property
      //   padding: EdgeInsets.all(8.0),
      children: list,
    );
  }
}
