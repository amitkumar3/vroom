//
//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:sample_flutter/models/BrowseVehicleModels.dart';
//import 'package:sample_flutter/tools/fontawesome.dart';
//
//class BrowseVehiclesWidget extends StatefulWidget
//{
//   BrowseVehicleModels model;
//   BrowseVehiclesWidget(this.model);
//
//  _BrowseVehicleWidgetState createState() => _BrowseVehicleWidgetState();
//}
//
//
//class _BrowseVehicleWidgetState extends State<BrowseVehiclesWidget>
//{
//  @override
//    Widget build(BuildContext context) {
//      List<Widget> cols = [];
//      List sliders = widget.model.getSliders();
//      for (var i = 0; i < sliders.length; i++) {
//        List<CategoryModel> items = widget.model.getSliderItems(i);
//        if (items.length > 0) {
//          cols.add(CategorySlider(sliders[i], items));
//          cols.add(SizedBox(height: 10.0));
//        }
//      }
//      return Column(
//        // mainAxisSize: MainAxisSize.min,
//          children: cols);
//    }
//}
//
//class CategorySlider extends StatelessWidget {
//
//  final childs;
//  final title;
//  CategorySlider(this.title, this.childs);
//
//  Widget build(BuildContext context) {
//    return Container(
//      height: 70.0,
//      child: Column(
//        mainAxisAlignment: MainAxisAlignment.start,
//        children: <Widget>[
//          Container(
//            width: double.infinity,
//            child: Text(title),
//          ),
//          SizedBox(height: 10.0),
//          Expanded(
//            child: ListView.builder(
//              scrollDirection: Axis.horizontal,
//              itemCount: childs.length,
//              itemBuilder: (BuildContext context, int index) {
//
//                return
//              //    InkWell(
//             //     onTap: () {
//
////                    Navigator.push(
////                        context,
////                        MaterialPageRoute(
////                          builder: (BuildContext context) => VehicleListing(
////                              childs[index].name,
////                              Constants.getCarListing +
////                                  '&catfilterval={"cat":[' +
////                                  childs[index].id.toString() +
////                                  ']}',
////                              ""),
////                        ));
//             //     },
//
//               //   child:
//                Card(
//                      borderOnForeground: true,
//                      // color:Colors.grey,
//                      child: Padding(
//                        padding: const EdgeInsets.all(8.0),
//                        child: Row(
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          children: <Widget>[
//                            Container(
//                              width: 32.0,
//                              height: 32.0,
//                              decoration: BoxDecoration(
//                                  shape: BoxShape.circle, color: Colors.grey),
//                              child: Icon(
//                                fontAwsomeIcon(childs[index].fontAwsomeClass),
//                                size: 12.0,
//                                color: Colors.white,
//                              ),
//                            ),
//                            SizedBox(width: 2.0),
//                            Text(childs[index].name)
//                          ],
//                        ),
//                      )
//                  );
//              },
//            ),
//          ),
//        ],
//      ),
//    );
//  }
//}
//
//
//

import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/EndPoints.dart';
import 'package:sample_flutter/constants/UIAssets.dart';
import 'package:sample_flutter/models/BrowseVehicleModels.dart';
import 'package:sample_flutter/screens/VehicleListing.dart';

class BrowseVehiclesWidget extends StatefulWidget {
  final BrowseVehicleModels model;

  BrowseVehiclesWidget(this.model);

  @override
  _BrowseVehicleWidgetState createState() => _BrowseVehicleWidgetState();
}

class _BrowseVehicleWidgetState extends State<BrowseVehiclesWidget>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    List<Widget> cols = [];
    List sliders = widget.model.getSliders();
    for (var i = 0; i < sliders.length; i++) {
      List<CategoryModel> items = widget.model.getSliderItems(i);
      print(sliders[i]);

      if (items.length > 0) {
        cols.add(CategorySlider(sliders[i], items));
        cols.add(SizedBox(height: 10.0));
      }
    }
    return Column(
        // mainAxisSize: MainAxisSize.min,
        children: cols);
  }
}

class CategorySlider extends StatelessWidget {
  final childs;
  final title;

  CategorySlider(this.title, this.childs);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(
//                  left: 20,
//                  top: 10

                  left: UIAssets.outerSpace,
                  top: UIAssets.innerSpace),
              child: Text(title),
            ),
          ),
          SizedBox(height: 10.0),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(
                  // left: 10,
                  // right: 10
                  left: UIAssets.innerSpace,
                  right: UIAssets.innerSpace),
              scrollDirection: Axis.horizontal,
              itemCount: childs.length,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => VehicleListing(
                              childs[index].name,
                              EndPoints.getCarListing +
                                  '&catfilterval={"cat":[' +
                                  childs[index].name.toString() +
                                  ']}',
                              ""),
                        ));
                  },
                  child: Card(
                    margin: EdgeInsets.only(
                        // left: 5,
                        // right: 5
                        left: UIAssets.breathingSpace,
                        right: UIAssets.breathingSpace),
                    borderOnForeground: true,
                    // color:Colors.grey,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(5.0),
                          width: 32.0,
                          height: 32.0,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.grey),
                          child: Icon(
                            // fontAwesomeIcon(childs[index].fontAwesomeClass),
                            Icons.directions_car,
                            size: 19.0,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(width: 2.0),
                        Container(
                            margin: EdgeInsets.only(right: 10.0),
                            child: Text(childs[index].name)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
