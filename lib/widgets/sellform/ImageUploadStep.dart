import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/models/SellFormModel.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:sample_flutter/widgets/sellform/StepTitle.dart';

class ImageUploadStep extends StatefulWidget {
  final SellFormModel model;
  final int totalSteps;

  final Function back, next;

  ImageUploadStep({this.next, this.back, this.model, this.totalSteps});

  @override
  _ImageUploadStepState createState() => _ImageUploadStepState();
}

class _ImageUploadStepState extends State<ImageUploadStep> {
  List<File> images = [];

  @override
  void initState() {
    super.initState();
    images = widget.model.imageList ?? [];
  }

  Future getImageFromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context);
    setState(() {
      if (image != null) images.add(image);
    });
  }

  Future getImageFromGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.pop(context);
    setState(() {
      if (image != null) images.add(image);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        children: <Widget>[
          SizedBox(height: 40.0),
          new StepTitle(
            stepNumber: 8,
            totalSteps: widget.totalSteps,
            title: "Upload Images",
          ),
          SizedBox(height: 20.0),
          InkWell(
            onTap: _showDialog,
            child: Container(
              width: double.infinity,
              child: RaisedButton.icon(
                // height: 20.0,
                textColor: Colors.white,

                color: hexToColor(AppColors.COLOR_APP_GREY),
                onPressed: _showDialog,
                label: Text("Image"),
                icon: Icon(Icons.camera_alt),
              ),
            ),
          ),
          SizedBox(height: 20.0),
          Expanded(
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 10.0,
              ),
              itemCount: images.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: Image.file(
                    images[index],
                    fit: BoxFit.cover,
                  ),
                );
              },
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                onPressed: widget.back,
                child: Row(
                  children: <Widget>[
                    Icon(Icons.arrow_back, size: 14.0),
                    SizedBox(width: 4.0),
                    Text(
                      "Back",
                      style: TextStyle(fontSize: 14.0),
                    ),
                  ],
                ),
              ),
              RaisedButton(
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                textColor: Colors.white,
                color: hexToColor(AppColors.COLOR_VROOM_THEME),
                child: Text("SUBMIT"),
                onPressed: () {
                  widget.model.saveImages(images);
                  widget.next();
                },
              )
            ],
          )
        ],
      ),
    );
  }

  void _showDialog() {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
          contentPadding: EdgeInsets.all(0.0),
          content: ListView(
            shrinkWrap: true,
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.camera),
                title: Text("Pick from Camera"),
                onTap: getImageFromCamera,
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.image),
                title: Text(
                  "Pick from Gallery",
                ),
                onTap: getImageFromGallery,
              ),
            ],
          )),
    );
  }
}
