import 'package:flutter/material.dart';

class StepTitle extends StatelessWidget {
  const StepTitle({
    Key key,
    @required this.stepNumber,
    @required this.totalSteps,
    @required this.title,
  }) : super(key: key);

  final stepNumber;
  final totalSteps;
  final title;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text("STEP $stepNumber of $totalSteps",
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(letterSpacing: 1.0, fontWeight: FontWeight.w600)),
            Text(title, style: Theme.of(context).textTheme.headline),
        ],
      )
    );
  }
}
