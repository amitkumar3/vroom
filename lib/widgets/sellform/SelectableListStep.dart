import 'package:flutter/material.dart';
import 'package:sample_flutter/widgets/SelectableList.dart';

import 'StepTitle.dart';

class SelectableListStep extends StatelessWidget {
  final onSelected;
  final selected;
  final title;
  final stepNumber;
  final totalSteps;
  final list;
  final Function back;

  SelectableListStep(
      {this.selected,
      this.onSelected,
      this.title,
      this.stepNumber,
      this.totalSteps,
      this.list,
      this.back});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 40.0),
          new StepTitle(
              stepNumber: stepNumber, totalSteps: totalSteps, title: title),
          SizedBox(height: 20.0),
          Expanded(
              child: (list.length != 0)
                  ? SelectableList(list, selected, onSelected)
                  : Center(
                      child: Text("we do not support the following type"),
                    )),
          stepNumber == 1
              ? Container()
              : FlatButton(
                  onPressed: back,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.arrow_back,
                        size: 14.0,
                      ),
                      SizedBox(width: 4.0),
                      Text(
                        "Back",
                        style: TextStyle(fontSize: 14.0),
                      ),
                    ],
                  ),
                )
        ],
      ),
    );
  }
}
