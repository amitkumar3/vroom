import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/models/SellFormModel.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:sample_flutter/widgets/sellform/StepTitle.dart';

class OtherDetails extends StatefulWidget {
  final SellFormModel model;
  final int totalSteps;
  final Function back, next;

  OtherDetails({this.model, this.totalSteps, this.back, this.next});

  @override
  _OtherDetailsState createState() => _OtherDetailsState();
}

class _OtherDetailsState extends State<OtherDetails> {
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          children: <Widget>[
            SizedBox(height: 20.0),
            new StepTitle(
                stepNumber: 7,
                totalSteps: widget.totalSteps,
                title: "Other Information"),
            SizedBox(height: 20.0),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    TextBox(
                      label: "Price",
                      value: widget.model.price ?? "",
                      hintText: "1000000",
                      validate: widget.model.validatePrice,
                      save: widget.model.savePrice,
                    ),
                    TextBox(
                      label: "Kilo meter driven",
                      value: widget.model.kmDriven ?? "",
                      hintText: "1000000",
                      validate: widget.model.validateKmDriven,
                      save: widget.model.saveKmDriven,
                    ),
                    TextBox(
                      label: "Enter Model number",
                      value: widget.model.model ?? "",
                      hintText: "MH-43-AA-3324",
                      validate: widget.model.validateModelNo,
                      save: widget.model.saveModelNo,
                    ),
                    DropDown(
                      label: "Select Registration Year",
                      value: widget.model.registrationYear,
                      items: widget.model.registrationYears,
                      // hintText: "regi",
                      validate: widget.model.validateSelect,
                      save: widget.model.saveRegistrationYear,
                    ),
                    DropDown(
                      label: "Location",
                      items: widget.model.locations,
                      value: widget.model.location,
                      // hintText: "regi",
                      validate: widget.model.validateSelect,
                      save: widget.model.saveLocation,
                    ),
                    DropDown(
                      label: "Owner",
                      items: widget.model.owners,
                      value: widget.model.owner,
                      // hintText: "regi",
                      validate: widget.model.validateSelect,
                      save: widget.model.saveOwner,
                    ),
                    DropDown(
                      label: "Exterior Color",
                      value: widget.model.color,
                      items: widget.model.colors,
                      // hintText: "regi",
                      validate: widget.model.validateSelect,
                      save: widget.model.saveColor,
                    ),
                    DropDown(
                      label: "Body Type",
                      value: widget.model.bodyType,
                      items: widget.model.bodyTypes,
                      // hintText: "regi",
                      validate: widget.model.validateSelect,
                      save: widget.model.saveBodyType,
                    ),
                    DropDown(
                      label: "Transmission Type",
                      value: widget.model.transmissionType,
                      items: widget.model.transmissionTypes,
                      // hintText: "regi",
                      validate: widget.model.validateSelect,
                      save: widget.model.saveTransmissionType,
                    ),
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  onPressed: widget.back,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.arrow_back,
                        size: 14.0,
                      ),
                      SizedBox(width: 4.0),
                      Text(
                        "Back",
                        style: TextStyle(fontSize: 14.0),
                      ),
                    ],
                  ),
                ),
                RaisedButton(
                  child: Text("Next"),
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  color: hexToColor(AppColors.COLOR_VROOM_THEME),
                  splashColor: Colors.orangeAccent,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      widget.next();
                    }
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class TextBox extends StatelessWidget {
  final String label;
  final String hintText;
  final String value;
  final Function validate, save;

  TextBox({this.label, this.validate, this.save, this.value, this.hintText});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          label,
          style: TextStyle(color: Colors.black, fontSize: 14.0),
        ),
        SizedBox(height: 10.0),
        TextFormField(
          initialValue: value,
          decoration: InputDecoration(
            hintText: hintText,
            focusedBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: hexToColor(AppColors.COLOR_VROOM_THEME)),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: hexToColor("#ff3d00")),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: hexToColor("#e0e0e0")),
            ),
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          ),
          validator: validate,
          onSaved: save,
        ),
        SizedBox(height: 20.0)
      ],
    );
  }
}

class DropDown extends StatefulWidget {
  final String label;
  final value;
  final String hintText;
  final Map items;
  final Function validate, save;

  DropDown(
      {this.items,
      this.label,
      this.value,
      this.validate,
      this.save,
      this.hintText});

  @override
  _DropDownState createState() => _DropDownState();
}

class _DropDownState extends State<DropDown> {
  dynamic selected;

  @override
  void initState() {
    super.initState();
    selected = widget.value ?? "";
  }

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem> list = [];
    list.add(DropdownMenuItem(
      value: "",
      child: Text("Please Select"),
    ));
    for (var i in widget.items.keys) {
      list.add(
        DropdownMenuItem(
          value: i,
          child: Text(widget.items[i].name),
        ),
      );
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          widget.label,
          style: TextStyle(color: Colors.black, fontSize: 14.0),
        ),
        SizedBox(height: 10.0),
        DropdownButtonFormField(
          onChanged: (v) {
            FocusScope.of(context).requestFocus(FocusNode());
            setState(() {
              selected = v;
            });
          },
          value: selected,
          validator: widget.validate,
          onSaved: widget.save,
          items: list,
          decoration: InputDecoration(
            hintText: widget.hintText,
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: hexToColor("#ff3d00")),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: hexToColor(AppColors.COLOR_VROOM_THEME)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: hexToColor("#e0e0e0")),
            ),
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          ),
        ),
        SizedBox(height: 20.0)
      ],
    );
  }
}
