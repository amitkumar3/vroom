import 'package:flutter/material.dart';

class SelectableList extends StatelessWidget {
  final Map list;
  final onSelected;
  final selected;

  SelectableList(this.list, this.selected, this.onSelected);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: list.keys.length,
      itemBuilder: (BuildContext context, int index) {
        var tick = list.keys.elementAt(index) == selected
            ? Icon(Icons.check_circle_outline, color: Colors.green)
            : Icon(Icons.check_circle_outline, color: Colors.grey);
        return Card(
            child: InkWell(
          onTap: () {
            onSelected(list.keys.elementAt(index));
          },
          child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Container(
                  width: 100.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(list[list.keys.elementAt(index)].name),
                      tick
                    ],
                  ))),
        ));
      },
    );
  }
}
