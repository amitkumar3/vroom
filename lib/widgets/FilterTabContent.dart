import 'package:flutter/material.dart';

class FilterTabContent extends StatefulWidget {
  final Map filterData;
  final String title;
  final Function callback;
  final List<int> selected;

  FilterTabContent({this.title, this.filterData, this.selected, this.callback});

  @override
  _FilterTabContentState createState() => _FilterTabContentState();
}

class _FilterTabContentState extends State<FilterTabContent> {
  String tabKey;
  List<int> selected;

  @override
  void initState() {
    tabKey = widget.title;
    selected = [];
    print("setting selected to widget.selected");
    selected = widget.selected;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    selected = widget.selected;
    // print(selected);
    return ListView.builder(
      itemCount: widget.filterData.keys.length,
      itemBuilder: (BuildContext context, int index) {
        var filterItem =
            widget.filterData[widget.filterData.keys.elementAt(index)];
        // print(filterItem);

        return ListTile(
            onTap: () => performSelect(filterItem.id),
            leading: Checkbox(
              onChanged: (v) => performSelect(filterItem.id),
              value: selected.contains(filterItem.id),
            ),
            title: Text(filterItem.name));
      },
    );
  }

  performSelect(int id) {
    setState(() {
      if (selected.contains(id))
        selected.remove(id);
      else
        selected.add(id);
      widget.callback(selected);
    });
  }
}