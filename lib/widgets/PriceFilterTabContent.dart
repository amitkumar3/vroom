import 'package:flutter/material.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class PriceFilterTabContent extends StatefulWidget {
  final double min, max;
  final List selectedPriceRange;
  final Function callback;

  PriceFilterTabContent(
      this.min, this.max, this.selectedPriceRange, this.callback);

  @override
  _PriceFilterTabContentState createState() => _PriceFilterTabContentState();
}

class _PriceFilterTabContentState extends State<PriceFilterTabContent> {
  double min, max;

  @override
  void initState() {
    super.initState();
    print("intialized with ");
    print(widget.selectedPriceRange);
    min = (widget.selectedPriceRange[0] / widget.max) * 100;
    max = (widget.selectedPriceRange[1] / widget.max) * 100;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 60.0),
        Container(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            width: double.infinity,
            child: Text("Select Price Range",
                style: Theme.of(context).textTheme.body2)),
        SizedBox(height: 16.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(formatAmount((min / 100) * widget.max).toString(),
                  style: Theme.of(context).textTheme.caption),
              Text(
                formatAmount((max / 100) * widget.max).toString(),
                style: Theme.of(context).textTheme.caption,
              ),
            ],
          ),
        ),
        SliderTheme(
          data: SliderTheme.of(context).copyWith(
            // overlayColor: overlayColor,
            // activeTickMarkColor: activeTickMarkColor,
            activeTrackColor: hexToColor(AppColors.COLOR_VROOM_THEME),
            inactiveTrackColor: hexToColor(AppColors.COLOR_LIGHT_GREY),
            //trackHeight: 8.0,
            thumbColor: Colors.red,
          ),
          child: frs.RangeSlider(
            min: 0.0,
            max: 100.0,
            lowerValue: min,
            upperValue: max,
            divisions: 100,
            onChanged: (double newLowerValue, double newUpperValue) {
              setState(() {
                min = newLowerValue;
                max = newUpperValue;
              });
            },
            onChangeEnd: (min, max) {
              widget.callback(
                  (min / 100) * widget.max, (max / 100) * widget.max);
            },
          ),
        )
      ],
    );
  }
}
