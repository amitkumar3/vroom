import 'package:flutter/material.dart';
import 'package:sample_flutter/models/FilterRequest.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class AppliedFilters extends StatelessWidget {
  final FilterRequest filterRequest;
  final Function callback;

  AppliedFilters(this.filterRequest, this.callback);

  @override
  Widget build(BuildContext context) {
    List<Widget> chips = [];
    var appliedfilters = filterRequest.getSelectedFilters();
    for (var filterKey in appliedfilters.keys) {
      for (var itemKey in appliedfilters[filterKey])
        chips.add(Chip(
            deleteIcon: Icon(Icons.clear),
            onDeleted: () {
              filterRequest.removeFilterItem(filterKey, itemKey);
              callback();
            },
            label: Text(
                filterRequest.getFilterItem(filterKey, itemKey) ?? "Empty")));
    }
    if (filterRequest.getPriceFilter()) {
      List range = filterRequest.getSelectedPriceRange();
      chips.add(Chip(
        label: Text(formatAmount(range[0]) + " - " + formatAmount(range[1])),
        deleteIcon: Icon(Icons.clear),
        onDeleted: () {
          filterRequest.resetPriceFilter();
          callback();
        },
      ));
    }

    return Wrap(
      spacing: 5.0,
      children: chips,
    );
  }
}
