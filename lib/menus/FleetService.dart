import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:sample_flutter/constants/EndPoints.dart';

class FleetService extends StatefulWidget {
  final String title;

  FleetService(this.title);

  _FleetService createState() => _FleetService();
}

class _FleetService extends State<FleetService> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: Text(super.widget.title),
        elevation: 8.0,
      ),
      hidden: true,
      url: 'https://vroom.infini.work/fleetmgmt',
      initialChild: Material(
        color: Colors.black.withAlpha(300),
        child: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                new SizedBox(
                  height: 15.0,
                ),
                Text(
                  "Please Wait...",
                  style: TextStyle(
                      fontWeight: FontWeight.w700, color: Colors.black),
                )
              ],
            ),
          ),
        ),
      ),
      withZoom: true,
      withJavascript: true,
      scrollBar: false,
    );
  }
}
