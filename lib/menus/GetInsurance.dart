import 'package:file_picker/file_picker.dart' as prefix0;
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/screens/FilePicker.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:toast/toast.dart';

class GetInsurance extends StatefulWidget {
  final String title;

  GetInsurance(this.title);

  _GetInsurance createState() => _GetInsurance();
}

class _GetInsurance extends State<GetInsurance> {
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;

  int _radioValue1 = -1;
  int _radioValue2 = -1;

  String name, email, phNo, address, regNo, kms;

  var _vehicleType = ['Motorbike', 'Car', 'SUV/Micro', 'Commerical vehicle'];
  var _year = ['2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019'];
  var _policyType = ['Comprehensive', '3rd Party'];

  var _currentItemSelected = 'Motorbike';
  var _ItemSelected = "2019";
  var _selectedItem = "Comprehensive";

  String _fileName = 'Choose a file';
  String _path = '...';
  String extension;
  bool _hasValidMime = false;
  FileType _pickingType = prefix0.FileType.ANY;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(super.widget.title),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(8.0),
        child: Form(
          key: _key,
          autovalidate: _validate,
          child: Column(
            children: <Widget>[

              Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Center(
                      child: Container(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'Buy comprehensive or third-party vehicle '
                          'insurance through Vroom hassle-free.'
                          ' Just complete the form below and our '
                          'team of insurance specialists will get in touch with you'
                          ' regarding documentation and pricing. All documents will'
                          ' be delivered to you signed and sealed in the fastest possible time. '
                          'Vroom brings in the service in collaboration with Green Delta Insurance Company Limited.',
                          //  textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[

                    SizedBox(
                      height: 10.0,
                    ),

                    Text(
                      'Apply for Vehicle Insurance in 3 easy steps:',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18.0),
                      textAlign: TextAlign.center,
                    ),

                    SizedBox(height: 25.0),

                    Text(
                      'Step 1: Personal Information',
                      style: TextStyle(
                          fontWeight: FontWeight.w700, fontSize: 15.0),
                    ),

                    SizedBox(
                      height: 20.0,
                    ),

                    /**
                     *  Name field
                     */
                    Text      (
                      "Name *",
                      style: TextStyle(color: Colors.black, fontSize: 14.0),
                    ),

                    SizedBox(
                      height: 10.0,
                    ),

                    TextFormField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          hintText: 'Enter Full Name'),
                      keyboardType: TextInputType.emailAddress,
                      validator: validateName,
                      onSaved: (String val) {
                        name = val;
                      },
                    ),

                    SizedBox(
                      height: 15.0,
                    ),

                    /**
                     *  Email field
                     */
                    Text(
                      "Email *",
                      style: TextStyle(color: Colors.black, fontSize: 14.0),
                    ),

                    SizedBox(
                      height: 10.0,
                    ),

                    TextFormField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          hintText: 'Enter Email'),
                      keyboardType: TextInputType.emailAddress,
                      validator: validateEmail,
                      onSaved: (String val) {
                        email = val;
                      },
                    ),

                    SizedBox(
                      height: 15.0,
                    ),

                    /**
                     *  Mobile No Field
                     */
                    Text(
                      "Contact Number *",
                      style: TextStyle(color: Colors.black, fontSize: 14.0),
                    ),

                    SizedBox(
                      height: 10.0,
                    ),

                    TextFormField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          hintText: 'Enter Contact Number'),
                      keyboardType: TextInputType.phone,
                      validator: validateMobile,
                      onSaved: (String val) {
                        phNo = val;
                      },
                    ),

                    SizedBox(
                      height: 15.0,
                    ),

                    /**
                     *  Address Field
                     */
                    Text(
                      "Address *",
                      style: TextStyle(color: Colors.black, fontSize: 14.0),
                    ),

                    SizedBox(
                      height: 10.0,
                    ),

                    TextFormField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0
                          ),
                          hintText: 'Enter Address'
                      ),
                      keyboardType: TextInputType.text,
                      validator: validateAddress,
                      onSaved: (String val) {
                        phNo = val;
                      },
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    /**
                     *  Divider
                     */
                    Container(

                        child: Divider(
                      color: Colors.grey[350],
                      //   height: 50,
                    )
                    ),

                    SizedBox(
                      height: 15.0,
                    ),

                    Text(
                      'Step 2: Vehicle Condition',
                      style: TextStyle(
                          fontWeight: FontWeight.w700, fontSize: 15.0),
                    ),

                    SizedBox(height: 15.0),

                    Text('Condition'),

                    Stack(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: 0,
                                  groupValue: _radioValue1,
                                  onChanged: _handleRadioValueChange1,
                                  activeColor:
                                      hexToColor(AppColors.COLOR_VROOM_THEME),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                ),
                                Text(
                                  'New Vehicles',
                                  //   style: new TextStyle(fontSize: 16.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: 1,
                                  groupValue: _radioValue1,
                                  onChanged: _handleRadioValueChange1,
                                  activeColor:
                                      hexToColor(AppColors.COLOR_VROOM_THEME),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                ),
                                Text('Reconditioned Vehicles'),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: 2,
                                  groupValue: _radioValue1,
                                  onChanged: _handleRadioValueChange1,
                                  activeColor:
                                      hexToColor(AppColors.COLOR_VROOM_THEME),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                ),
                                Text('Used Vehicles')
                              ],
                            ),
                          ],
                        )
                      ],
                    ),

                    SizedBox(
                      height: 10.0,
                    ),

                    /**
                     *  Registration no field
                     */
                    Text('Enter Vehicle Registration Number *'),

                    SizedBox(
                      height: 10.0,
                    ),

                    TextFormField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          hintText: 'Registration Number'),
                      keyboardType: TextInputType.phone,
                      validator: validateRegNumber,
                      onSaved: (String val) {
                        phNo = val;
                      },
                    ),

                    SizedBox(
                      height: 15.0,
                    ),

                    Text('Enter KMS driven by your Vehicle *'),

                    SizedBox(
                      height: 10.0,
                    ),

                    TextFormField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          hintText: 'KMS Driven'),
                      keyboardType: TextInputType.phone,
                      validator: validateKmDriven,
                      onSaved: (String val) {
                        phNo = val;
                      },
                    ),

                    SizedBox(
                      height: 15.0,
                    ),

                    Text('Vehicle Type *'),

                    SizedBox(
                      height: 10.0,
                    ),

                    Container(
                      width: MediaQuery.of(context).size.width * 10,
                      height: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? MediaQuery.of(context).size.height * 0.05
                          : MediaQuery.of(context).size.height * 0.12,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: hexToColor("#e0e0e0"),
                            style: BorderStyle.solid,
                            width: 1.0),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            items:
                                _vehicleType.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                value: dropDownStringItem,
                                child: Text(dropDownStringItem),
                              );
                            }).toList(),
                            onChanged: (String newValueSelected) {
                              // Your code to execute, when a menu item is selected form drop down
                              setState(() {
                                this._currentItemSelected = newValueSelected;
                              });
                            },
                            value: _currentItemSelected,
                            hint: Text('Select Vehicle Type'),
                            isExpanded: false,
                          ),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 15.0,
                    ),

                    Text('Select Year *'),

                    SizedBox(
                      height: 10.0,
                    ),

                    Container(
                      width: MediaQuery.of(context).size.width * 10,
                      height: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? MediaQuery.of(context).size.height * 0.05
                          : MediaQuery.of(context).size.height * 0.12,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: hexToColor("#e0e0e0"),
                            style: BorderStyle.solid,
                            width: 1.0),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            items: _year.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                value: dropDownStringItem,
                                child: Text(dropDownStringItem),
                              );
                            }).toList(),
                            onChanged: (String newValueSelected) {
                              // Your code to execute, when a menu item is selected form drop down
                              setState(() {
                                this._ItemSelected = newValueSelected;
                              });
                            },
                            value: _ItemSelected,
                          ),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 20.0,
                    ),

                    Container(
                        //margin: const EdgeInsets.only(left: 8.0, right: 8.0),
                        child: Divider(
                      color: Colors.grey[350],
                      //   height: 50,
                    )
                    ),

                    SizedBox(
                      height: 15.0,
                    ),

                    Text(
                      'Step 3: Insurance Information',
                      style: TextStyle(
                          fontWeight: FontWeight.w700, fontSize: 15.0),
                    ),

                    SizedBox(
                      height: 10.0,
                    ),

                    Text('Insurance for:'),

                    SizedBox(
                      height: 8.0,
                    ),

                    Stack(children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Radio(
                                value: 0,
                                groupValue: _radioValue2,
                                onChanged: _handleRadioValueChange2,
                                activeColor:
                                    hexToColor(AppColors.COLOR_VROOM_THEME),
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                              ),
                              Text(
                                'Renew Existing Policy',
                                //   style: new TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Radio(
                                value: 1,
                                groupValue: _radioValue2,
                                onChanged: _handleRadioValueChange2,
                                activeColor:
                                    hexToColor(AppColors.COLOR_VROOM_THEME),
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                              ),
                              Text('New Vehicle Insurance Policy'),
                            ],
                          ),

                          SizedBox(
                            height: 8.0,
                          ),

                        ],
                      ),
                    ]
                    ),

                    Text('Policy Type *'),

                    SizedBox(
                      height: 10.0,
                    ),

                    Container(
                      width: MediaQuery.of(context).size.width * 10,
                      height: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? MediaQuery.of(context).size.height * 0.05
                          : MediaQuery.of(context).size.height * 0.12,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: hexToColor("#e0e0e0"),
                            style: BorderStyle.solid,
                            width: 1.0),
                      ),

                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            items: _policyType.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                value: dropDownStringItem,
                                child: Text(dropDownStringItem),
                              );
                            }).toList(),
                            hint: Text("Select Policy"),
                            onChanged: (String newValueSelected) {
                              // Your code to execute, when a menu item is selected form drop down
                              setState(() {
                                this._selectedItem = newValueSelected;
                              });
                            },
                            value: _selectedItem,
                          ),
                        ),
                      ),
                    ),


                    SizedBox(
                      height: 20.0,
                    ),

                    Container(
                        //margin: const EdgeInsets.only(left: 8.0, right: 8.0),
                        child: Divider(
                      color: Colors.grey[350],
                      //   height: 50,
                      )
                    ),

                    SizedBox(
                      height: 15.0,
                    ),

                    Text(
                      'Step 4: Tax Token',
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),

                    SizedBox(
                      height: 10.0,
                    ),

                    Text('Upload Tax Token:'),
                    SizedBox(
                      height: 8.0,
                    ),

                    /**
                     *  Choose file container
                     */
                    Container(
                      width: MediaQuery.of(context).size.width * 10,
                      height: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? MediaQuery.of(context).size.height * 0.05
                          : MediaQuery.of(context).size.height * 0.12,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: hexToColor("#e0e0e0"),
                            style: BorderStyle.solid,
                            width: 1.0),
                      ),

                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text(_fileName,
                              style: TextStyle(color: Colors.grey[600]),),

                            ),
                          ),
                          InkWell(
                            onTap: () {
                              _openFileExplorer();
                              //  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context ) => FilePicker()));
                              //openFile();

                            },
                            child: Container(
                              height: 50,
                              width: 65,
                              alignment: Alignment.center,
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Choose",
                                style: TextStyle(
                                    color: hexToColor(
                                      AppColors.COLOR_VROOM_THEME,
                                    ),
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 20.0,
                    ),

                    SizedBox(
                      height: 45.0,
                      width: double.infinity,
                      child: RaisedButton(
                        child: const Text('Submit'),
                        textColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        color: hexToColor(AppColors.COLOR_VROOM_THEME),
                        splashColor: Colors.orangeAccent,
                        onPressed: () {
                            funCall();
                         // Toast.show("Click", context);

                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

//    return WebviewScaffold(
//      appBar: AppBar(
//        title: Text(super.widget.title),
//        elevation: 8.0,
//      ),
//
//
//      hidden: true,
//      url: 'https://vroom.infini.work/getinsurance',
//      initialChild: Material(
//        color: Colors.black.withAlpha(300),
//        child: Container(
//          child: Center(
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
//                new CircularProgressIndicator(),
//                new SizedBox(
//                  height: 15.0,
//                ),
//                Text(
//                  "Please Wait...",
//                  style: TextStyle(
//                      fontWeight: FontWeight.w700, color: Colors.black),
//                )
//              ],
//            ),
//          ),
//        ),
//      ),
//      withZoom: true,
//      withJavascript: true,
//      scrollBar: false,
//    );

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          Toast.show("New Vehicle", context);
          break;
        case 1:
          Toast.show("Reconditoned Vehicles", context);
          break;
        case 2:
          Toast.show("Used Vehicle", context);
          break;
      }
    });
  }

  void _handleRadioValueChange2(int value) {
    setState(() {
      _radioValue2 = value;

      switch (_radioValue2) {
        case 0:
          Toast.show("Existing policy", context);
          break;
        case 1:
          Toast.show("Insurance policy", context);
          break;
      }
    });
  }

  void _openFileExplorer() async {
    if(_pickingType == FileType.ANY || _hasValidMime) {
      try {
        _path = await prefix0.FilePicker
            .getFilePath(type: _pickingType, fileExtension: extension);
      } on PlatformException catch (e) {
        print("Unsupported operation" + e.toString());
      }

     // if (!mounted) return;

      setState(() {
        _fileName = _path != null ? _path.split('/').last : '...';
      });
    }
  }

  void funCall() async {
    if(_key.currentState.validate()) {
      Toast.show("Submit btn clicked", context);
    }
    else {
      setState(() {
        _validate = true;
        }
      );
    }
  }
}