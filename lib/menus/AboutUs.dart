import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("About us"),
      ),

      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[

              Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                alignment: Alignment.centerLeft,
                child: Text("Vroom Services Limited is the first online vehicle maintenance service"
                    " in Bangladesh. Launched with a view to cater the growing but unstructured "
                    "automotive industry, Vroom aspires to work for the betterment of the auto "
                    "eco-system in the long run."),
              ),


              Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                alignment: Alignment.centerLeft,
                child: Text("Besides home services, Vroom offers Fleet Management Solution and creating"
                    " a platform to connect not all rather the quality workshops of the country."
                    " Dhaka being the capital city and primary market was the obvious choice for "
                    "Vroom to commence its operation here. But gradually Vroom plans to spread out to "
                    "all districts of Bangladesh."),
              ),

              Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                alignment: Alignment.centerLeft,
                child: Text("Vroom also offers vehicle buy-sell and automotive marketplace. "
                    "The experience of buying and selling an automobile is completely broken"
                    " and full of pain points and friction. With its disruptive marketplace "
                    "approach, Vroom is leveraging technology and data in a way that removes"
                    " impediments why a transaction between two unknown people may not take "
                    "place. With Vroom’s innovative approach buyers get huge selection, low "
                    "prices, and develop trust while sellers get access to millions of online"
                    " shoppers, higher trade velocity, and a full stack of sellers' tools and "
                    "e-commerce services to self-manage their online store at Vroom."),
              ),

              Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                alignment: Alignment.centerLeft,
                child: Text("The Vroom team has the right kind of mix of experience. Head Quartered "
                    "at Gulshan, Vroom has its own workshop team to serve the customers equipped with"
                    " necessary skill and mind set."),
              ),

              Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                alignment: Alignment.centerLeft,
                child: Text("As a start-up Vroom is constantly in need of funds to expand horizontally "
                    "and vertically. Any query on potential partnership is always welcome to take the "
                    "company to the next level."),
              )

            ],
          ),
        ),
      ),
    );
  }

}