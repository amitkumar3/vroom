//import 'package:flutter/material.dart';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
//import 'package:sample_flutter/constants/EndPoints.dart';
//import 'package:sample_flutter/tools/app_tools.dart';
//
//class WashandPolish extends StatefulWidget {
//  final String title;
//
//  WashandPolish(this.title);
//
//  _WashandPolish createState() => _WashandPolish();
//}
//
//class _WashandPolish extends State<WashandPolish> {
//  @override
//  Widget build(BuildContext context) {
//    return WebviewScaffold(
//      appBar: AppBar(
//        title: Text(super.widget.title),
//        elevation: 8.0,
//      ),
//      hidden: true,
//      url: 'https://vroom.infini.work/washnpolish',
//      initialChild: Material(
//        color: Colors.black.withAlpha(300),
//        child: Container(
//          child: Center(
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
//                new CircularProgressIndicator(),
//                new SizedBox(
//                  height: 15.0,
//                ),
//                Text(
//                  "Please Wait...",
//                  style: TextStyle(
//                      fontWeight: FontWeight.w700, color: Colors.black),
//                )
//              ],
//            ),
//          ),
//        ),
//      ),
//      withZoom: true,
//      withJavascript: true,
//      scrollBar: false,
//    );
//  }
//}

//import 'package:flutter/material.dart';
//import 'package:zoom_widget/zoom_widget.dart';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
//import 'package:sample_flutter/constants/EndPoints.dart';
//import 'package:sample_flutter/tools/app_tools.dart';
//
//class WashandPolish extends StatefulWidget {
//  final String title;
//
//  WashandPolish(this.title);
//
//  _WashandPolish createState() => _WashandPolish();
//}
//
//class _WashandPolish extends State<WashandPolish> {
//  @override
//  Widget build(BuildContext context) {
//
//    return Scaffold(
//      appBar: AppBar(title: Text('Wash & Polish'),
//      ),
//
//      body: ListView(
//        children: <Widget>[
//
//        ],
//      ),

import 'package:flutter/material.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class WashandPolish extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Wash & Polish"),
        elevation: 5,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
              child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Center(
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Our dedicated home service team always '
                      'ready to provide you vehicle care service'
                      ' at your door step. We want to make your life easier.',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0),
                    ),
                  ),
                ),


                Center(
                  child: Container(
                    padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0, top: 8.0),
                    child: Text(
                      'Time is precious. Time is scarce. '
                      'Time is not for wastage. '
                      'That is why we have introduced our home service for your vehicle'
                      'maintenance so that time can be saved. No need to send your vehicle '
                      'to the workshop for regular wash/polish/oil and filter change. '
                      'Rather the Vroom team will come to your place and do the necessary '
                      'job during the vehicle’s idle time. You can save on time and fuel.'
                      'At present – the following services are available.',
                    ),
                  ),
                ),

//                SizedBox(
//                  height: 20.0,
//                ),

                Padding(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0 ),
                  child: Text(
                    "For Vroom Members",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),

                SizedBox(
                  height: 15,
                ),
                // Vroom members table

                /**
                     *  Vroom Members First row
                     */
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        // color: Colors.orange,
                        border: Border(
                          top: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          left: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          right: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                        ),
                        color: hexToColor('#E8E8E8')),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.only(left: 10),
                              //color: Colors.grey[200],
                              child: Text(
                                  'All Bike-Wash, Vacuum Cleaning & Polish')
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 50,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(
                              alignment: Alignment.center,
                              //  color: Colors.grey[300],
                              child: Text('BDT 500.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                     *  Vroom Members Second row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        left: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        right: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.only(left: 8.0),
                              //  color: Colors.grey[200],
                              child: Text(
                                  'Any Type of Vehicle-Oil Change & Filter Change')
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 50,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(

                              //  color: Colors.grey[300],
                              alignment: Alignment.center,
                              child: Text('BDT 600.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                     *  Vroom Members Third row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        // color: Colors.orange,
                        border: Border(
                          top: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          left: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          right: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                        ),
                        color: hexToColor('#E8E8E8')),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.all(8.0),
                              //color: Colors.grey[200],
                              child: Text(
                                  'Any Type of Vehicle-Wash, Vacuum Cleaning, Oil Change, '
                                      'Filter Change')
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 85,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(
                              alignment: Alignment.center,
                              //color: Colors.grey[300],
                              child: Text('BDT 800.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                        Vroom Members Fourth row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      // color: Colors.orange,
                      border: Border(
                        top: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        left: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        right: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              // color: Colors.grey[200],
                              padding: EdgeInsets.all(8),
                              child: Text(
                                  'Sedan/Hatchback-Interior Polish, Exterior Polish,'
                                      ' Oil Change & Filter Change')
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 85,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(
                              alignment: Alignment.center,
                              // color: Colors.grey[300],
                              child: Text('BDT 2500.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                        Vroom Members Fifth row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      // color: Colors.orange,
                      border: Border(
                        top: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        left: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        right: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                      ),
                      color: hexToColor('#E8E8E8'),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              //  color: Colors.grey[200],
                              padding: EdgeInsets.all(8),
                              child: Text(
                                  'Sedan/Hatchback-Wash, Vacuum Cleaning, Interior Polish, '
                                      'Exterior Polish, Oil Change & Filter Change')
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 118,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(

                              //color: Colors.grey[300],
                              alignment: Alignment.center,
                              child: Text('BDT 3000.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                        Vroom Members Sixth row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      // color: Colors.orange,
                      border: Border(
                        top: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        left: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        right: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.all(8.0),
                              // color: Colors.grey[200],
                              child: Text(
                                  'SUV/Crossover/Micro-Interior Polish, Exterior Polish,'
                                      ' Oil Change & Filter Change')
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 85,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(

                              //  color: Colors.grey[300],
                              alignment: Alignment.center,
                              child: Text('BDT 4000.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                        Vroom Members Seventh row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        // color: Colors.orange,
                        border: Border(
                          top: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          left: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          right: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          bottom: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                        ),
                        color: hexToColor('#E8E8E8')),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                                alignment: Alignment.center,
                                width: 500,
                                // color: Colors.grey[200],
                                child: Text(
                                    'SUV/Crossover/Micro-Wash, Vacuum Cleaning, '
                                    'Interior Polish, Exterior Polish, '
                                    'Oil Change & Filter Change')),
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 120,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(
                              alignment: Alignment.center,
                              //  color: Colors.grey[300],
                              child: Text('BDT 4500.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: 20.0,
                ),

                Text(
                  "For Non Vroom Members",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),

                SizedBox(
                  height: 15,
                ),

                /**
                     *  Non Vroom Members First row
                     */
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        // color: Colors.orange,
                        border: Border(
                          top: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          left: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          right: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                        ),
                        color: hexToColor('#E8E8E8')),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.only(left: 10),
                              //color: Colors.grey[200],
                              child: Text(
                                  'All Bike-Wash, Vacuum Cleaning & Polish')),
                        ),
                        Container(
                          width: 1,
                          height: 50,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(
                              alignment: Alignment.center,
                              //  color: Colors.grey[300],
                              child: Text('BDT 600.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                     *  Non Vroom Members Second row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        left: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        right: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.only(left: 8.0),
                              //  color: Colors.grey[200],
                              child: Text(
                                  'Any Type of Vehicle-Oil Change & Filter Change')),
                        ),
                        Container(
                          width: 1,
                          height: 50,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(

                              //  color: Colors.grey[300],
                              alignment: Alignment.center,
                              child: Text('BDT 700.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                     *  Non Vroom Members Third row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        // color: Colors.orange,
                        border: Border(
                          top: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          left: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          right: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                        ),
                        color: hexToColor('#E8E8E8')),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.all(8.0),
                              //color: Colors.grey[200],
                              child: Text(
                                  'Any Type of Vehicle-Wash, Vacuum Cleaning,'
                                  ' Oil Change, Filter Change')),
                        ),
                        Container(
                          width: 1,
                          height: 85,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(
                              alignment: Alignment.center,
                              //color: Colors.grey[300],
                              child: Text('BDT 900.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                        Non Vroom Members Fourth row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      // color: Colors.orange,
                      border: Border(
                        top: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        left: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        right: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              // color: Colors.grey[200],
                              padding: EdgeInsets.all(8),
                              child: Text(
                                  'Sedan/Hatchback-Interior Polish, Exterior Polish,'
                                  ' Oil Change & Filter Change with'
                                  ' Free Membership Card')),
                        ),
                        Container(
                          width: 1,
                          height: 100,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(
                              alignment: Alignment.center,
                              // color: Colors.grey[300],
                              child: Text('BDT 2800.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                        Non Vroom Members Fifth row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      // color: Colors.orange,
                      border: Border(
                        top: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        left: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        right: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                      ),
                      color: hexToColor('#E8E8E8'),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              //  color: Colors.grey[200],
                              padding: EdgeInsets.all(8),
                              child: Text(
                                  'Sedan/Hatchback-Wash, Vacuum Cleaning, '
                                  'Interior Polish, Exterior Polish, '
                                  'Oil Change & Filter Change with Free Membership Card')),
                        ),
                        Container(
                          width: 1,
                          height: 135,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(

                              //color: Colors.grey[300],
                              alignment: Alignment.center,
                              child: Text('BDT 3500.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                        Non Vroom Members Sixth row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      // color: Colors.orange,
                      border: Border(
                        top: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        left: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                        right: BorderSide(
                            width: 1.0, color: hexToColor('#C0C0C0')),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.all(8.0),
                              // color: Colors.grey[200],
                              child: Text(
                                  'SUV/Crossover/Micro-Interior Polish, '
                                  'Exterior Polish, Oil Change & Filter Change '
                                  'with Free Membership Card')),
                        ),
                        Container(
                          width: 1,
                          height: 100,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(

                              //  color: Colors.grey[300],
                              alignment: Alignment.center,
                              child: Text('BDT 4300.00')),
                        ),
                      ],
                    ),
                  ),
                ),

                /**
                        Non Vroom Members Seventh row
                     */

                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        // color: Colors.orange,
                        border: Border(
                          top: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          left: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          right: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                          bottom: BorderSide(
                              width: 1.0, color: hexToColor('#C0C0C0')),
                        ),
                        color: hexToColor('#E8E8E8')),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                                // color: Colors.grey[200],
                                child: Text(
                                    'SUV/Crossover/Micro-Wash, Vacuum Cleaning,'
                                    ' Interior Polish, Exterior Polish, Oil Change '
                                    '& Filter Change with Free Membership Card')),
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 135,
                          color: hexToColor('#C0C0C0'),
                        ),
                        Expanded(
                          child: Container(
                              alignment: Alignment.center,
                              //  color: Colors.grey[300],
                              child: Text('BDT 5000.00')),
                        ),
                      ],
                    ),
                  ),
                ),


                Padding(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4),
                  child: Center(
                    child: Container(
                      child: Text(
                        'Besides home services, Vroom offers Fleet Management Solution'
                        'and creating a platform to connect not all rather the quality '
                        'workshops of the country. Dhaka being the capital city and '
                        'primary market was the obvious choice for Vroom to commence '
                        'its operation here. But gradually Vroom plans to spread out'
                        'to all districts of Bangladesh.',
                        // textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),


                Padding(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, bottom: 4),
                  child: Center(
                    child: Container(
                      child: Text(
                        'Vehicle Wash, Polish and Oil/Filter Change comes'
                        ' as a all-in-one solution to make sure you get the optimum out of the deal.'
                        ' Our home services team equipped with foam wash, '
                        'steam and polishing machines will make your vehicle tiptop in no time.',
                        //  textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),


                Padding(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, bottom: 4),
                  child: Center(
                    child: Container(
                      child: Text(
                        'You can take the services like Wash, Polish '
                        'or Oil/Filter change individually or in bundle. '
                        'Just dial our call center at +8809678187666 or '
                        'use our App to book your requisite service.',
                        //   textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )),
        ),
      ),
    );
  }
}

//        Center(
//          child: DataTable(
//            columns: <DataColumn> [
//
//              DataColumn(
//                label: Text('Services'),
//              ),
//
//              DataColumn(
//                label: Text('Price')
//              )
//            ],
//
//            rows: <DataRow> [
//
//              // First row
//              DataRow(
//                cells: <DataCell>[
//
//                  DataCell(
//                      Container(child: Text('Person'),color: Colors.amberAccent,)
//
//                    //Text('All Bike-Wash, Vacuum Cleaning & Polish')
//                  ),
//
//                  DataCell(
//                    Text('BDT 500.00')
//                  )
//                ]
//              )
//
//              // Second row
//
//            ],
//          ),
//        )
