//import 'package:flutter/material.dart';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
//import 'package:sample_flutter/constants/EndPoints.dart';

//class OwnerShip extends StatefulWidget {
//  final String title;
//
//  OwnerShip(this.title);
//
//  _OwnerShip createState() => _OwnerShip();
//}
//
//class _OwnerShip extends State<OwnerShip> {
//  @override
//  Widget build(BuildContext context) {
//    return WebviewScaffold(
//      appBar: AppBar(
//        title: Text(super.widget.title),
//        elevation: 8.0,
//      ),
//      hidden: true,
//      url: 'https://vroom.infini.work/ownership',
//      initialChild: Material(
//        color: Colors.black.withAlpha(300),
//        child: Container(
//          child: Center(
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
//                new CircularProgressIndicator(),
//                new SizedBox(
//                  height: 15.0,
//                ),
//                Text(
//                  "Please Wait...",
//                  style: TextStyle(
//                      fontWeight: FontWeight.w700, color: Colors.black),
//                )
//              ],
//            ),
//          ),
//        ),
//      ),
//      withZoom: true,
//      withJavascript: true,
//      scrollBar: false,
//    );
//  }
//}

import 'package:flutter/material.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:toast/toast.dart';

class OwnerShip extends StatefulWidget
{
  final String title;

  OwnerShip(this.title);

  _OwnerShip createState() => _OwnerShip();
}

class _OwnerShip extends State<OwnerShip>
{
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  String name, email, phNo;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(super.widget.title),
      ),
      body: SingleChildScrollView(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _key,
            autovalidate: _validate,
            child: Column(
              children: <Widget>[
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[

                        Center(
                          child: Container(
                            padding: EdgeInsets.all(8.0),
                            child: Text('For used personal vehicles only.',
                            ),
                          ),
                        ),

                        Center(
                          child: Container(
                            padding: EdgeInsets.only(left: 8.0, right: 8.0, ),
                            child: Text('Transfer of ownership is the most critical part '
                                'of any vehicle buy and sell. Usually in Bangladesh, '
                                'we do this part after the payment is made or some do not '
                                'transfer at all which may lead to serious legal issues. '
                                'As such, if any critical situation develops, both '
                                'the buyer and seller gets into a time consuming hassle. '
                                'To relieve the vehicle buyers and sellers of used vehicles, '
                                'we offer this service.',
                            ),
                          ),
                        ),



                        Center(
                          child: Container(
                            padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                            child: Text('OTS (Ownership Transfer Service) customers can'
                                ' be assured that Vroom Team will make '
                                'sure the name is properly transferred and all'
                                ' the documents with the authorities are properly '
                                'submitted.',
                            ),
                          ),
                        ),


                        Center(
                          child: Container(
                            padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                            child: Text('Once you finalize selling or buying the vehicle, '
                                'please book for our OTS services and our Relationship '
                                'Officer will get in touch with you regarding the necessary '
                                'documents and authorizations.',
                      //    textAlign: TextAlign.center,
                            ),
                          ),
                        )
                    ],
                  ),
                ),

                Container(
                  padding:
                //  const EdgeInsets.only(left: 20.0, right: 20.0),
                  const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      SizedBox(
                        height: 15.0,
                      ),


                      /**
                       *  Name field
                       */
                      Text(
                        "Name *",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        validator: validateName,
                        onSaved: (String val) {
                          name = val;
                        },
                      ),

                      SizedBox(height: 15.0,),

                      /**
                       *  Email field
                       */
                      Text(
                        "Email *",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),

                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        validator: validateEmail,
                        onSaved: (String val) {
                            email = val;
                        },
                      ),

                      SizedBox(height: 15.0,),

                      /**
                       *  Mobile No Field
                       */
                      Text(
                        "Mobile *",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),

                      SizedBox(
                        height: 10.0,
                      ),

                      TextFormField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: hexToColor(AppColors.COLOR_VROOM_THEME)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: hexToColor("#e0e0e0")),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        keyboardType: TextInputType.phone,
                        validator: validateMobile,
                        onSaved: (String val) {
                            phNo = val;
                        },
                      ),

                      SizedBox(height: 20.0,),

                      /**
                       *   Submit button
                       */

                      SizedBox(
                        height: 50.0,
                        width: double.infinity,
                        child: RaisedButton(
                          child: const Text('Submit'),
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          color: hexToColor(AppColors.COLOR_VROOM_THEME),
                          splashColor: Colors.orangeAccent,
                          onPressed: funCall
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
    );

//    return
//      Scaffold(
//      appBar: AppBar(
//        title: new Text(super.widget.title),
//      ),
//

//      body: SingleChildScrollView(
//        child: Padding(
//          padding: const EdgeInsets.all(10.0),
//
//          child: Container(
//
//            child: Column(
//              children: <Widget>[
//
//                  Text('For used personal vehicles only.',
//                    textAlign: TextAlign.center,
//                  ),
//
//                SizedBox(height: 15.0),
//
//                Text('Transfer of ownership is the most critical part '
//                      'of any vehicle buy and sell. Usually in Bangladesh,'
//                      ' we do this part after the payment is made or some do not'
//                      ' transfer at all which may lead to serious legal issues.'
//                      'As such, if any critical situation develops, both the buyer'
//                      ' and seller gets into a time consuming hassle. To relieve the'
//                      ' vehicle buyers and sellers of used vehicles, we offer this service.',
//                    textAlign: TextAlign.center,
//                  ),
//
//                SizedBox(height: 15.0),
//
//                 Text('OTS (Ownership Transfer Service) customers can '
//                      'be assured that Vroom Team will make sure the name '
//                      'is properly transferred and all the documents with '
//                      'the authorities are properly submitted.',
//                    textAlign: TextAlign.center,
//                  ),
//
//                SizedBox(height: 15.0),
//
//                Text('Once you finalize selling or buying the vehicle,'
//                    ' please book for our OTS services and our Relationship '
//                    'Officer will get in touch with you regarding the necessary '
//                    'documents and authorizations.',
//                  textAlign: TextAlign.center,
//                ),
//
//                SizedBox(height: 50.0),
//
//                Container(
//                  child: Form(
//                    child: Column(
//                      children: <Widget>[
//
//
//                      ],
//                    ),
//                  ),
//                )
//
////                Form(
////                  key: _key,
////                  autovalidate: _validate,
////                  child: Column(
////                    children: <Widget>[
////
////                      Text("Name *",textAlign: TextAlign.right,),
////
////                      TextFormField()
////                    ],
////                  ),
////                )
//
//              ],
//            ),
//          ),
//        )
//      ),

//      Container(
//            padding: EdgeInsets.all(15.0),
//            child: Column(
//              children: <Widget>[
//
//
//
//
//
//
//
//
//
//
//
//
//                Form(key: _key,
//                    autovalidate: _validate,
//                    child: Column(
//                      children: <Widget>[
//
//                        Text(
//                          "Name *",
//                       //   style: TextStyle(color: Colors.black, fontSize: 14.0),
//                          textAlign: TextAlign.right,
//                        ),
//
//                        SizedBox(
//                          height: 10.0,
//                        ),
//
//                        TextFormField(
//                          decoration: InputDecoration(
//                            focusedBorder: OutlineInputBorder(
//                              borderSide: BorderSide(
//                                  color: hexToColor(AppColors.COLOR_VROOM_THEME)),
//                            ),
//                            enabledBorder: OutlineInputBorder(
//                              borderSide:
//                              BorderSide(color: hexToColor("#e0e0e0")),
//                            ),
//                            contentPadding: EdgeInsets.symmetric(
//                                vertical: 10.0, horizontal: 10.0),
//                          ),
//                          keyboardType: TextInputType.emailAddress,
//                          validator: validateName,
//                          onSaved: (String val) {
//                            name = val;
//                        },
//                      ),
//                    ],
//                  )
//                ),
//              ],
//            ),
//          ),




    //);
  }

  funCall() async {
    if(_key.currentState.validate()) 
    {
        Toast.show("Weclome to OCT screen", context);
    }
    else 
    {
        setState(() {
          _validate = true;
        });
    }
  }
}
