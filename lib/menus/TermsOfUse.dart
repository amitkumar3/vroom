
import 'package:flutter/material.dart';

class TermsOfUse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Terms of Use"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                child: Text("The domain name www.vroom.com.bd is owned/ licensed and operated by Vroom"
                    " Services Limited, a company incorporated under the Companies Act in Bangladesh,"
                    " hereinafter, individually and/ or collectively referred to as “Vroom/ Company”."
                    " Vroom provides access to a variety of services, including but not limited to "
                    "access to the website, mobile site and mobile application (hereinafter individually"
                    " and collectively referred to as “Website”). These Terms of Use constitute a binding"
                    " legal agreement between users and the Website."),
              ),

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
              child: Text("For the purpose of these Terms of Use, wherever the context so requires"
                  " \"You\", \"Your\" or 'User' shall mean any natural or legal person who is "
                  "a user/ visitor of the Website availing the services, raising queries and"
                  " seeking information and/ or who has agreed to become user on the Website by"
                  " providing registration data while registering on the Website as registered User"
                  " using the computer systems. Vroom allows the User to surf the Website or making "
                  "purchases without registering on the Website. The term \"We\", \"Us\", \"Our\" "
                  "shall mean Vroom."),
            ),

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
              child: Text("Your use of the Website, services and tools are governed by the following Terms"
                  " of Use as applicable to the Website including the applicable policies which are "
                  "incorporated herein by way of reference. If You transact on the Website, You shall be"
                  " subject to the policies that are applicable to the Website for such transactions. "
                  "By mere use of the Website, You shall be contracting with Vroom and these Terms of Use "
                  "including the policies constitute Your binding obligations, with Vroom."),
            ),


            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
              child: Text("When You use any of the services provided by Us through the Website, including but not"
                  " limited to, buying and selling any vehicles, mobile phones, merchandise, leaving product "
                  "reviews, seller reviews, etc. (hereinafter referred to as “Services”), You will be subject "
                  "to the rules, guidelines, policies, terms, and conditions applicable to such Service(s),"
                  " and they shall be deemed to be incorporated into these Terms of Use and shall be considered "
                  "as part and parcel of these Terms of Use."),
            ),

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
              child: Text("We reserve the right, at Our sole discretion, to change, modify, "
                  "add or remove portions of these Terms of Use, at any time without any prior"
                  " written notice to You. It is Your responsibility to review these Terms of Use "
                  "periodically for updates / changes. Your continued use of the Website following"
                  " the posting of changes will mean that You accept and agree to the revisions."
                  " As long as You comply with these Terms of Use, We grant You a personal, non-exclusive,"
                  " non-transferable, limited privilege to enter and use the Website."),
            ),

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 8.0),
              child: Text("Users making a purchase on the Website shall be referred to as the “Buyer” "
                  "and Users selling their products by listing them on the Website shall be"
                  " referred to as the “Seller”."),
            ),


            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 8.0),
              child: Text("BY ACCESSING, BROWSING OR OTHERWISE USING THE SITE INDICATES YOUR ACCEPTANCE TO ALL THE "
                  "TERMS AND CONDITIONS UNDER THESE TERMS OF USE, SO PLEASE READ THE TERMS OF USE"
                  " CAREFULLY BEFORE PROCEEDING.",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),

            Container(
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 8.0),
              child: Text("By impliedly or expressly accepting Vroom’s and this Website’s Terms of Use,"
                  " You also accept and agree to be bound by Vroom’s and this Website’s Policies "
                  "(including but not limited to Privacy Policy available on our website as amended"
                  " from time to time. We shall not be obliged to notify You, irrespective of the fact"
                  " that You are registered or not, of any modifications made to the Terms of Use."),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0) ,
                child: Text("YOUR ACCOUNT AND REGISTRATION OBLIGATIONS",
                  style: TextStyle(fontWeight: FontWeight.w700),
               )
            ),

            Container(
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              alignment: Alignment.center,
              child: Text("If You use the Website, You shall be responsible for maintaining the confidentiality"
                  " of Your information including your display name and password (if applicable) and Your"
                  " other personal information. You shall be responsible for all activities undertaken by "
                  "You, including but not limited to use of the Website for availing the Services through "
                  "this Website that occur under Your Display Name and Password. You agree that if You provide "
                  "any information that is untrue, inaccurate, not current or incomplete or We have reasonable"
                  " grounds to suspect that such information is untrue, inaccurate, not current or incomplete, "
                  "or not in accordance with these Terms of Use, We shall have the right to indefinitely"
                  " suspend or terminate or block access of Your membership on the Website and refuse to "
                  "provide You with access to the Website."),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                child: Text("COMMUNICATIONS",
                  style: TextStyle(fontWeight: FontWeight.w700),
               )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 8.0),
              child: Text("When You use the Website, or send emails or other data, information or communication"
                  " to Us and provide Your information required for availing the Services,"
                  " You agree and understand that You are communicating with Us through electronic records,"
                  " by telephonic calls, SMS and You consent to receive communications via electronic records, "
                  "telephone calls and SMSs from Us periodically and as and when required. We may communicate with"
                  " You by email or by such other mode of communication, electronic or otherwise."),
            ),

            Container(
              alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                child: Text("PLATFORM FOR TRANSACTION AND COMMUNICATION",
                  style: TextStyle(fontWeight: FontWeight.w700),
               )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 8.0),
              child: Text("The Website is a platform that Users utilize to inter alia get required information, "
                  "meet and interact with one another for their transactions and share their reviews about"
                  " automobiles and Sellers. Vroom and this Website shall in no matter be held responsible "
                  "for any opinion expressed, review made by any of the Users on this Website and/ or is not"
                  " and cannot be a party to or control in any manner any transaction between the Website's "
                  "Users."),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("HENCE FORWARD:",
                  style: TextStyle(fontWeight: FontWeight.w700),
               )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "1. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "All commercial/contractual terms are offered by and agreed to between Buyers"
                          " and Sellers alone. The commercial/contractual terms may include without limitation,"
                          " price, shipping costs, payment methods, payment terms, date, period and mode of delivery,"
                          " warranties related to products and services and after sales services related to products"
                          " and services. Vroom does not have any control or does not determine or advise or in any "
                          "way involve itself in the offering or acceptance of such commercial/contractual terms "
                          "between the Buyers and Sellers.", style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "2. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Vroom does not make any representation or Warranty as to specifics (such as quality,"
                          " value, saleability, etc.) of the products or services proposed to be sold or offered to"
                          " be sold or purchased on the Website. Vroom does not implicitly or explicitly support or"
                          " endorse the sale or purchase of any products or services on the Website. Vroom accepts "
                          "no liability for any errors or omissions, whether on behalf of itself or third parties.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "3. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Vroom is not responsible for any non-performance or breach of any contract entered "
                          "into between Buyers and Sellers. Vroom cannot and does not guarantee that the concerned"
                          " Buyers and/or Sellers will perform any transaction concluded on the Website. Vroom shall"
                          " not and is not required to mediate or resolve any dispute or disagreement between Buyers"
                          " and Sellers.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "4. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Vroom does not make any representation or warranty as to the item-specifics "
                          "(such as legal title, creditworthiness, identity, etc.) of any of its Users. "
                          "You are advised to independently verify the bona fides of any particular User "
                          "that You choose to deal with on the Website and use Your best judgment in that"
                          " behalf.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "5. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Vroom does not at any point of time during any transaction between Buyer and Seller"
                          " on the Website come into or take possession of any of the products or services offered "
                          "by Seller nor does it at any point gain title to or have any rights or claims over the"
                          " products or services offered by Seller to Buyer.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "6. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "At no time shall Vroom hold any right, title or interest over the products nor shall"
                          " Vroom have any obligations or liabilities in respect of such contract entered into between"
                          " Buyers and Sellers. Vroom is not responsible for unsatisfactory or delayed performance of "
                          "services or damages or delays as a result of products which are out of stock, unavailable "
                          "or back ordered.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "7. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "The Website is only a platform that can be utilized by Users to reach a larger base"
                          " to buy and sell products or services. Vroom is only providing a platform for communication"
                          " and it is agreed that the contract for sale of any of the products or services shall be a"
                          " strictly bipartite contract between the Seller and the Buyer.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "8. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Vehicle inspections are performed by independent, third-party mechanics"
                          " and/ or auto repair shops. Vroom does not own, operate, or control any repair shop"
                          " or mechanic designated as a Vroom certified inspection location, station or shop or"
                          " employ any third-party mechanics and, as such, Vroom does not guarantee the inspection"
                          " results.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),


            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
              child: Text("INSPECTION REPORTS ARE NOT A WARRANTY OR GUARANTEE ON THE VEHICLE. NEITHER VROOM, ITS"
                  " AFFILIATES, NOR ANY OF THEIR RESPECTIVE EMPLOYEES, AGENTS, THIRD PARTY CONTENT PROVIDERS, "
                  "OR LICENSORS MAKE ANY REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, TO ANY ACTUAL OR "
                  "PROSPECTIVE BUYER OR OWNER OF ANY VEHICLE AS TO THE EXISTENCE, OWNERSHIP OR CONDITION OF THE"
                  " VEHICLE, OR AS TO THE ACCURACY OR COMPLETENESS OF ANY INFORMATION IN AN INSPECTION REPORT."),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 6.0),
              child: Text("Used car buyers and Users of this Website, agree and acknowledge that they will not rely solely"
                  " on an Inspection report in their decision to buy a vehicle."),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "9. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "The inspection report is meant to cover certain items, but not all mechanical components "
                          "of a vehicle. The inspection reports available for the vehicles may not be updated with the "
                          "latest information about the vehicles and items that may appear acceptable now, may need work or "
                          "attention shortly after the inspection is completed. Vroom is not liable or responsible for any"
                          " damage to your vehicle caused during the inspection or while your vehicle is in the possession "
                          "of the mechanic or auto repair shop.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "10. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Vroom provides guidelines on how to perform the 5-segment, 121 points plus mechanical"
                          " inspection, but other than this, Vroom or the Website has no control or authority over such"
                          " mechanics. Vroom implements procedures to seek out quality mechanics, but does not guarantee"
                          " the work of any mechanic, as each mechanic operates separately and independently without any "
                          "control or direct supervision from Vroom.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "11. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "At no time shall Vroom hold any right, title or interest over the products nor shall Vroom "
                          "have any obligations or liabilities in respect of such contract.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "12. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "You shall independently agree upon the manner and terms and conditions of delivery,"
                          " payment, insurance etc. with the seller(s) that You transact with.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "13. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "You/ Users of the Website release and indemnify Vroom and/or any of its officers, "
                          "assigns and representatives from any cost, damage, liability or other consequence of any "
                          "of the actions of the Users of the Website and specifically waive any claims that You/ User"
                          " may have in this behalf under any applicable law(s). Notwithstanding the indemnification"
                          " mentioned hereinabove, despite its reasonable efforts in that behalf, Vroom cannot take"
                          " responsibility or control the information provided by other Users, which is made available"
                          " on the Website. You may find other User's information to be offensive, harmful, inconsistent, "
                          "inaccurate, or deceptive. Please use caution and practice safe trading when using the Website.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "14. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Please note that there could be risks in dealing with underage persons or people acting under"
                          " false pretense and Users are expected to conduct due diligence in this regard.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),



            Container(
              alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top:  8.0),
                child: Text("FEE POLICY",
                  style: TextStyle(fontWeight: FontWeight.w700),
               )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom:8.0),
              child: Text("Membership on the Website is free for buyers. Vroom does not charge any fee for browsing and/ "
                  "or buying/ transacting on the Website. Vroom reserves the right to change its Fee Policy from "
                  "time to time at its sole discretion. In particular, Vroom may at its sole discretion introduce "
                  "new services and modify some or all of the existing services offered on the Website. In such an "
                  "event Vroom reserves the right to introduce fees for the new services offered or amend/introduce "
                  "fees for existing services, as the case may be. Changes to the Fee Policy shall be posted on the "
                  "Website and such changes shall automatically become effective immediately after they are posted on"
                  " the Website. Unless otherwise stated, all fees shall be quoted in Bangladesh Taka. You/ Users shall"
                  " be solely responsible for compliance of all applicable laws including those in India for making "
                  "payments to Vroom, wherever applicable."),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("USE OF THE WEBSITE",
                  style: TextStyle(fontWeight: FontWeight.w700),
               )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0,  bottom: 6.0),
              child: Text("You/ Users Agree, Undertake And Confirm That Your Use Of The Website Shall Be Strictly Governed"
                  " By The Following Binding Principles:",
                style: TextStyle(fontWeight: FontWeight.w700),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "1. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "You shall not host, display, upload, modify, publish, transmit, update "
                          "or share any information which:",
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, top: 4.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17 )),
                      TextSpan(text: "Belongs to another person and to which You do not have any right to;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17 )),
                      TextSpan(text: "Is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, pedophilic,"
                          " libelous, invasive of another's privacy, hateful, or racially, ethnically objectionable, "
                          "disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any "
                          "manner whatever; or unlawfully threatening or unlawfully harassing including but not limited to"
                          " \"indecent representation of women\" within the meaning of the Indecent Representation of Women "
                          "(Prohibition) Act, 1986;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17 )),
                      TextSpan(text: "Is misleading in any way;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17 )),
                      TextSpan(text: "Is patently offensive to the online community, such as sexually explicit content, or content "
                          "that promotes obscenity, pedophilia, racism, bigotry, hatred or physical harm of any kind against"
                          " any group or individual;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17 )),
                      TextSpan(text: "Harasses or advocates harassment of another person;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17 )),
                      TextSpan(text: "Involves the transmission of \"junk mail\", \"chain letters\", or unsolicited mass mailing or \"spamming\";",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17 )),
                      TextSpan(text: "Promotes illegal activities or conduct that is abusive, threatening, obscene, defamatory or libelous;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17 )),
                      TextSpan(text: "Infringes upon or violates any third party's rights [including, but not limited to, intellectual"
                          " property rights, rights of privacy (including without limitation unauthorized disclosure of a "
                          "person's name, email address, physical address or phone number) or rights of publicity];",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17),

                      ),
                      TextSpan(text: "Promotes an illegal or unauthorized copy of another person's copyrighted work,"
                          " such as providing pirated computer programs or links to them, providing information to "
                          "circumvent manufacture-installed copy-protect devices, or providing pirated music or links"
                          " to pirated music files;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17),

                      ),
                      TextSpan(text: "Contains restricted or password-only access pages, or hidden pages or "
                          "images (those not linked to or from another accessible page);",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Provides material that exploits people in a sexual, violent or otherwise "
                          "inappropriate manner or solicits personal information from anyone;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Provides instructional information about illegal activities such as"
                          " making or buying illegal weapons, violating someone's privacy, or providing "
                          "or creating computer viruses;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Contains video, photographs, or images of another person (with a minor or an adult).",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Tries to gain unauthorized access or exceeds the scope of authorized access"
                          " to the Website or to profiles, blogs, communities, account information, bulletins,"
                          " friend request, or other areas of the Website or solicits passwords or personal "
                          "identifying information for commercial or unlawful purposes from other Users;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Engages in commercial activities and/or sales without Our prior"
                          " written consent such as contests, sweepstakes, barter, advertising and "
                          "pyramid schemes, or the buying or selling of \"virtual\" products related "
                          "to the Website. Throughout these Terms of Use, Vroom's prior written consent"
                          " means a communication coming from Vroom's Legal Department, specifically "
                          "in response to Your request, and specifically addressing the activity or "
                          "conduct for which You seek authorization;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Solicits gambling or engages in any gambling activity which We, in Our sole "
                          "discretion, believe is or could be construed as being illegal;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Interferes with another User's use and enjoyment of the Website or any other "
                          "individual's User and enjoyment of similar services;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Refers to any website or URL that, in Our sole discretion, contains material that is "
                          "inappropriate for the Website or any other website, contains content that would be prohibited "
                          "or violates the letter or spirit of these Terms of Use.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Harm minors in any way;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Infringes any patent, trademark, copyright or other proprietary rights or third party's"
                          " trade secrets or rights of publicity or privacy or shall not be fraudulent or involve the "
                          "sale of counterfeit or stolen products;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Violates any law for the time being in force;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Deceives or misleads the addressee/ users about the origin of such messages or communicates"
                          " any information which is grossly offensive or menacing in nature;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Impersonate another person;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Contains software viruses or any other computer code, files or programs designed to "
                          "interrupt, destroy or limit the functionality of any computer resource; or contains any"
                          " trojan horses, worms, time bombs, cancel bots, easter eggs or other computer programing "
                          "routines that may damage, detrimentally interfere with, diminish value of, surreptitiously"
                          " intercept or expropriate any system, data or personal information;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Threatens the unity, integrity, defense, security or sovereignty of Bangladesh, friendly"
                          " relations with foreign states, or public order or causes incitement to the commission of "
                          "any cognizable offence or prevents investigation of any offence or is insulting any other"
                          " nation.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "2. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "The User of this Website is required to ensure and undertakes that the"
                          " information provided on this Website or any actions of the User(s) on this Website shall:",
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0 ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not be false, inaccurate or misleading;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not, directly or indirectly, offer, attempt to offer, trade or attempt to trade in any "
                          "item, the dealing of which is prohibited or restricted in any manner under the provisions "
                          "of any applicable law, rule, regulation or guideline for the time being in force;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not create liability for Us or cause Us to lose (in whole or in part) the services of "
                          "Our internet service provider (\"ISPs\") or other suppliers;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not use any \"deep-link\", \"page-scrape\", \"robot\", \"spider\" or other automatic device,"
                          " program, algorithm or methodology, or any similar or equivalent manual process, to access, "
                          "acquire, copy or monitor any portion of the Website or any content, or in any way reproduce or"
                          " circumvent the navigational structure or presentation of the Website or any content, to obtain "
                          "or attempt to obtain any materials, documents or information through any means not purposely made "
                          "available through the Website. We reserve Our right to bar any such activity;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not attempt to gain unauthorized access to any portion or feature of the Website, or any "
                          "other systems or networks connected to the Website or to any server, computer, network, or to"
                          " any of the services offered on or through the Website, by hacking, password \"mining\" or "
                          "any other illegitimate means;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not probe, scan or test the vulnerability of the Website or any network connected to the"
                          " Website nor breach the security or authentication measures on the Website or any network "
                          "connected to the Website. You may not reverse look-up, trace or seek to trace any information "
                          "on any other User of or visitor to Website, or any other customer, including any account on "
                          "the Website not owned by You, to its source, or exploit the Website or any service or information"
                          " made available or offered by or through the Website, in any way where the purpose is to reveal"
                          " any information, including but not limited to personal identification or information, other "
                          "than Your own information, as provided for by the Website;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not make any negative, denigrating or defamatory statement(s) or comment(s) about Us or the"
                          " brand name or domain name used by Us or any other person/ entity or otherwise engage in any "
                          "conduct or action that might tarnish the image or reputation, of Vroom or Sellers on platform "
                          "or otherwise tarnish or dilute any Vroom's trade or service marks, trade name and/or goodwill "
                          "associated with such trade or service marks, trade name as may be owned or used by us. You agree"
                          " that You will not take any action that imposes an unreasonable or disproportionately large load"
                          " on the infrastructure of the Website or Vroom's systems or networks, or any systems or networks"
                          " connected to Vroom;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not make any negative, denigrating or defamatory statement(s) or comment(s) about Us or the"
                          " brand name or domain name used by Us or any other person/ entity or otherwise engage in any "
                          "conduct or action that might tarnish the image or reputation, of Vroom or Sellers on platform "
                          "or otherwise tarnish or dilute any Vroom's trade or service marks, trade name and/or goodwill "
                          "associated with such trade or service marks, trade name as may be owned or used by us. You agree"
                          " that You will not take any action that imposes an unreasonable or disproportionately large load"
                          " on the infrastructure of the Website or Vroom's systems or networks, or any systems or networks"
                          " connected to Vroom;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not to use any device, software or routine to interfere or attempt to interfere"
                          " with the proper working of the Website or any transaction being conducted on the Website, "
                          "or with any other person's use of the Website;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "You may not forge headers or otherwise manipulate identifiers in order to disguise the"
                          " origin of any message or transmittal You send to Us on or through the Website or any service "
                          "offered on or through the Website. You may not pretend that You are, or that You represent, "
                          "someone else, or impersonate any other individual or entity;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Not use the Website or any content for any purpose that is unlawful or prohibited by "
                          "these Terms of Use, or to solicit the performance of any illegal activity or other activity"
                          " which infringes the rights of Vroom and / or others;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "At all times ensure full compliance with the applicable provisions of the Information "
                          "Technology Act and rules thereunder as applicable and as amended from time to time and also "
                          "all applicable domestic laws, rules and regulations regarding Your use of Our Services and"
                          " Your listing, purchase, solicitation of offers to purchase, and sale of products or services."
                          " You shall not engage in any transaction in an item or service, which is prohibited by the "
                          "provisions of any applicable law including exchange control laws or regulations for the time"
                          " being in force.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Act in a bonafide manner to enable Us to use the information You supply Us with, "
                          "so that we are not violating any rights You might have in Your Information. You agree "
                          "to grant Us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable"
                          " (through multiple tiers) right to exercise the copyright, publicity, database rights or"
                          " any other rights You have in Your Information, in any media now known or not currently"
                          " known, with respect to Your Information. We will only use Your information in accordance"
                          " with the Terms of Use and Privacy Policy applicable to use of the Website;",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 6.0),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "3. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "From time to time, You shall be responsible for providing information relating to the"
                          " products or services proposed to be sold by You. In this connection, You undertake that "
                          "all such information shall be accurate in all respects. You shall not exaggerate or over "
                          "emphasize the attributes of such products or services so as to mislead other Users in "
                          "any manner.",
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 6),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "4. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "You shall not engage in advertising to, or solicitation of, other Users of the Website"
                          " to buy or sell any products or services, including, but not limited to, products or"
                          " services related to that being displayed on the Website or related to Us. You may "
                          "not transmit any chain letters or unsolicited commercial or junk email to other Users"
                          " via the Website. It shall be a violation of these Terms of Use to use any information"
                          " obtained from the Website in order to harass, abuse, or harm another person, or in order "
                          "to contact, advertise to, solicit, or sell to another person other than Us without Our "
                          "prior explicit consent. In order to protect Our Users from such advertising or solicitation,"
                          " We reserve the right to restrict the number of messages or emails which a user may send "
                          "to other Users in any 24-hour period which We deems appropriate in its sole discretion. "
                          "You understand that We have the right at all times to disclose any information "
                          "(including the identity of the persons providing information or materials on the Website)"
                          " as necessary to satisfy any law, regulation or valid governmental request. This may"
                          " include, without limitation, disclosure of the information in connection with investigation "
                          "of alleged illegal activity or solicitation of illegal activity or in response to a lawful "
                          "court order or subpoena. In addition, We can (and You hereby expressly authorize Us to)"
                          " disclose any information about You to law enforcement or other government officials, "
                          "as we, in Our sole discretion, believe necessary or appropriate in connection with the "
                          "investigation and/or resolution of possible crimes, especially those that may involve "
                          "personal injury.",
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 6),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "5. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "We reserve the right, but have no obligation, to monitor the materials posted on"
                          " the Website. Vroom shall have the right to remove or edit any content that in its "
                          "sole discretion violates, or is alleged to violate, any applicable law or either the "
                          "spirit or letter of these Terms of Use. Notwithstanding this right, YOU REMAIN SOLELY "
                          "RESPONSIBLE FOR THE CONTENT OF THE MATERIALS YOU POST ON THE WEBSITE AND IN YOUR PRIVATE MESSAGES."
                          " Please be advised that such Content posted does not necessarily reflect Vroom’s views."
                          " In no event shall Vroom assume or have any responsibility or liability for any Content"
                          " posted or for any claims, damages or losses resulting from use of Content and/or appearance"
                          " of Content on the Website. You hereby represent and warrant that You have all necessary rights"
                          " in and to all Content which You provide and all information it contains and that such Content"
                          " shall not infringe any proprietary or other rights of third parties or contain any libelous,"
                          " tortious, or otherwise unlawful information.",
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 6),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "6. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Your correspondence or business dealings with, or participation in promotions of,"
                          " advertisers found on or through the Website, including payment and delivery of related"
                          " products or services, and any other terms, conditions, warranties or representations "
                          "associated with such dealings, are solely between You and such advertiser. We shall not"
                          " be responsible or liable for any loss or damage of any sort incurred as the result of"
                          " any such dealings or as the result of the presence of such advertisers on the Website.",
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 6),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "7. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "It is possible that other Users (including unauthorized users or \"hackers\") may post or "
                          "transmit offensive or obscene materials on the Website and that You may be involuntarily exposed "
                          "to such offensive and obscene materials. It also is possible for others to obtain personal "
                          "information about You due to Your use of the Website, and that the recipient may use such"
                          " information to harass or injure You. We do not approve of such unauthorized uses, but by "
                          "using the Website You acknowledge and agree that We are not responsible for the use of "
                          "any personal information that You publicly disclose or share with others on the Website."
                          " Please carefully select the type of information that You publicly disclose or share with "
                          "others on the Website.",
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700))
                    ]
                ),
              ),
            ),


            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 6),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "8. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Vroom shall have all the rights to take necessary action and claim damages that "
                          "may occur due to Your involvement/participation in any way on your own or through"
                          " group/s of people, intentionally or unintentionally in DoS/DDoS (Distributed Denial"
                          " of Services).",
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700))
                    ]
                ),
              ),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 8.0),
                child: Text("CONTENTS POSTED ON WEBSITE:",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("A.) THIRD PARTY CONTENT:",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
              child: Text("All text, graphics, videos, user interfaces, visual interfaces, "
                  "photographs, trademarks, logos, sounds, music and artwork (collectively, "
                  "\"Content\"), other than the ones created, originated and used by the "
                  "Company is a third-party user generated content and Vroom has no control "
                  "over such third-party user generated content as Vroom is merely an intermediary"
                  " for the use of the materials and information available on this Website and "
                  "for the purposes of these Terms of Use."),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("B.) USER GENERATED CONTENT:",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
              child: Text("You shall be solely responsible for any notes, messages, emails,"
                  " billboard postings, photos, drawings, profiles, opinions, ideas, images, "
                  "videos, audio files or other materials or information posted or transmitted"
                  " to the Website (collectively, \"User Content\"). Such User Content will "
                  "become Our property and You grant Us the worldwide, perpetual and transferable"
                  " rights in such User Content. We shall be entitled to, consistent with Our "
                  "Privacy Policy as adopted in accordance with applicable law, to use the User"
                  " Content or any of its elements for any type of use forever, including but"
                  " not limited to promotional and advertising purposes and in any media whether"
                  " now, known or hereafter devised, including the creation of derivative works "
                  "that may include the User Content You provide. You agree that any User Content"
                  " You post may be used by Us, consistent with Our Privacy Policy and rules of "
                  "conduct on the Website as mentioned herein, and You are not entitled to any "
                  "payment or other compensation for such use."),
            ),

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
              child: Text("Except as expressly provided in these Terms of Use, no part of the "
                  "Website and no content including but not limited to the third-party Content "
                  "and/or User Content may be copied, reproduced, republished, uploaded, posted, "
                  "publicly displayed, encoded, translated, transmitted or distributed in any way "
                  "(including \"mirroring\") to any other computer, server, Website or other medium"
                  " for publication or distribution or for any commercial enterprise, without Vroom's"
                  " express prior written consent. You may use information on the products and Services"
                  " purposely made available on the Website for downloading, provided that You (1) "
                  "do not remove any proprietary notice language in all copies of such documents, "
                  "(2) use such information only for Your personal, non-commercial informational "
                  "purpose and do not copy or post such information on any networked computer or "
                  "broadcast it in any media, (3) make no modifications to any such information,"
                  " and (4) do not make any additional representations or warranties relating "
                  "to such documents."),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("PROFANITY POLICY",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
              child: Text("Vroom prohibits the use of language that is racist, hateful, sexual"
                  " or obscene in nature in a public area. This policy extends to text within"
                  " listings, on Seller pages and all other areas of the site that another"
                  " User may view. If the profane words are part of a title for the item being"
                  " sold, we allow Sellers to 'blur' out the bulk of the offending word with "
                  "asterisks (i.e., s*** or f***)."),
            ),

            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
              child: Text("Please report any violations of this policy to the correct area for review:"),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
              child: Text("• Report offensive Display Names"),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
              child: Text("• Report offensive language in a listing or therewise"),
            ),


            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: Text("If a feedback comment; or any communication made between Users on the Website; "
                  "or email communication between Users in relation to transactions conducted on "
                  "Website contain profanity, please review Our feedback removal policy and submit "
                  "a request for action/removal. Disciplinary action may result in the indefinite "
                  "suspension of a User's account, temporary suspension, or a formal warning. Vroom "
                  "will consider the circumstances of an alleged policy violation and the user's "
                  "trading records before taking action."),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("VIOLATIONS OF THIS POLICY MAY RESULT IN A RANGE OF ACTIONS, INCLUDING:",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),



            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "1. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)),
                      TextSpan(text: "Limits placed on account privileges;",
                          style: TextStyle(color: Colors.black, ))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0,),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "2. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Loss of special status;",
                          style: TextStyle(color: Colors.black, ))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 8),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "3. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black )),
                      TextSpan(text: "Account suspension",
                          style: TextStyle(color: Colors.black, ))
                    ]
                ),
              ),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("EMAIL ABUSE & THREAT POLICY",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
              child: Text("Private communication, including email correspondence, is not regulated"
                  " by Vroom. Vroom encourages its Users to be professional, courteous and respectful"
                  " when communicating by email."),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 6.0),
              child: Text("However, Vroom will investigate and can take action on certain types of "
                  "unwanted emails that violate Vroom policies"),
            ),


            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("SUCH INSTANCES INCLUDE:",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "1. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)),
                      TextSpan(text: "Threats of Bodily Harm - Vroom does not permit Users to "
                          "send explicit threats of bodily harm.",
                          style: TextStyle(color: Colors.black, ))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "2. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)),
                      TextSpan(text: "Misuse of Vroom System - Vroom allows Users to facilitate transactions "
                          "through the Vroom system but will investigate any misuse of this Service.",
                          style: TextStyle(color: Colors.black, ))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "3. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)),
                      TextSpan(text: "Spoof (Fake) email - Vroom will never ask you to provide "
                          "sensitive information through email. In case you receive any spoof "
                          "(fake) email, you are requested to report the same to Us"
                          " through the 'Contact Us' tab.",
                          style: TextStyle(color: Colors.black, ))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "4. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)),
                      TextSpan(text: "Spam (Unsolicited Commercial email) - Vroom's spam policy applies only"
                          " to unsolicited commercial messages sent by Vroom Users. "
                          "Vroom Users are not allowed to send spam messages to other Users.",
                          style: TextStyle(color: Colors.black, ))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "5. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)),
                      TextSpan(text: " Offers to Buy or Sell Outside of Vroom - Vroom prohibits email"
                          " offers to buy or sell listed products outside of the Vroom Website."
                          " Offers of this nature are a potential fraud risk for both Buyers and Sellers.",
                          style: TextStyle(color: Colors.black, ))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, top: 4.0),
              child: Text("Vroom policy prohibits user-to-user threats of physical harm via any "
                  "method including, phone, email and on Our public message boards."),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
              child: Text("Violations of this policy may result in a range of actions, including:"),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Limits on account privileges",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Account suspension",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: "Cancellation of listings",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),


            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "• ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17,)
                      ),
                      TextSpan(text: " Loss of special status",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("FOR SELLERS:",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "1. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Any dealer/ Seller found abusing/misusing the Vroom platform or "
                          "promotions will be suspended and may face legal action.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "2. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Any Seller caught violating Vroom policy(s) will be suspended for 3 "
                          "weeks and all their payments will be put on hold. If a Seller is suspended more"
                          " than 3 times they will permanently banned from selling at Vroom.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "3. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Any vehicle submitted for promotions must be held"
                          " for specified duration.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "4. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "In case of any policy violation, Vroom reserves the rights to"
                          " cancel any order while the Seller will still be liable to pay selling"
                          " service fees to Vroom.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "5. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "One Seller should have only one account with Vroom. Any seller"
                          " found with multiple accounts with Vroom will be liable to have their "
                          "account suspended and be liable to legal action.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "6. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Sellers need to submit a copy of a cancelled cheque to Vroom as "
                          "proof of their bank account at the time of on boarding.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "7. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Vroom shall deduct tax as per NBR guidelines.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("FURTHER",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "1. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Transactions, transaction price and all commercial terms such as"
                          " delivery, dispatch of products and/or services are as per principal to "
                          "principal bipartite contractual obligations between Buyer and Seller and"
                          " payment facility is merely used by the Buyer and Seller to facilitate the "
                          "completion of the transaction. Use of the payment facility shall not render"
                          " Vroom liable or responsible for the non-delivery, non-receipt, non-payment,"
                          " damage, breach of representations and warranties, non-provision of after "
                          "sales or warranty services or fraud as regards the products and /or services"
                          " listed on Vroom's Website.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "2. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "You have specifically authorized Vroom or its service providers to "
                          "collect, process, facilitate and remit payments and / or the transaction price"
                          " electronically or through Cash on Delivery to and from other Users in respect "
                          "of transactions through the payment facility. Your relationship with Vroom is "
                          "on a principal to principal basis and by accepting these Terms of Use you agree"
                          " that Vroom is an independent contractor for all purposes, and does not have "
                          "control of or liability for the products or services that are listed on Vroom's"
                          " Website that are paid for by using the payment facility. Vroom does not guarantee "
                          "the identity of any User nor does it ensure that a Buyer or a Seller will complete"
                          " a transaction.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "3. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "You understand, accept and agree that the payment facility provided"
                          " by Vroom is neither a banking nor financial service but is merely a facilitator"
                          " providing an electronic, automated online electronic payment, receiving payment"
                          " through Cash On Delivery, collection and remittance facility for the transactions"
                          " on the Vroom Website using the existing authorized banking infrastructure and "
                          "Credit Card payment gateway networks. Further, by providing payment facility,"
                          " Vroom is neither acting as trustees nor acting in a fiduciary capacity with "
                          "respect to the transaction or the transaction price.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 6.0),
                child: Text("PAYMENT FACILITY FOR BUYERS:",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "1. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "You, as a Buyer, understand that upon initiating a transaction You "
                          "are entering into a legally binding and enforceable contract with the Seller to"
                          " purchase the products and /or services from the Seller using the payment facility,"
                          " and You shall pay the transaction price through Your issuing bank/ instrument to"
                          " the Seller using the payment facility.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "2. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "You, as a Buyer, may agree with the Seller through electronic communication"
                          " and electronic records and using the automated features as may be provided by the "
                          "payment facility on any extension / increase in the dispatch and/or delivery time "
                          "and the transaction shall stand amended to such extent. Any such extension / increase"
                          " of dispatch / delivery time or subsequent novation / variation of the transaction "
                          "should be in compliance with these Terms of Use.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "3. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "You, as a Buyer, shall electronically notify the payment facility "
                          "using the appropriate Vroom Website features immediately upon delivery or non-delivery"
                          " within the time period as provided. Non-notification by You of delivery or"
                          " non-delivery within the time period specified shall be construed as a deemed"
                          " delivery in respect of that transaction. In case of Cash on Delivery transactions,"
                          " Buyer is not required to confirm the receipt of products or services.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "4. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "You, as a Buyer, shall be entitled to claim a refund of the transaction"
                          " price (as Your sole and exclusive remedy) in case You do not receive the delivery"
                          " within the time period agreed in the transaction or within the time period as "
                          "provided, whichever is earlier. In case you do not raise a refund claim using the"
                          " Website features within the stipulated time than this would make You ineligible"
                          " for a refund.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "5. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "You, as a Buyer, understand that the payment facility may not be"
                          " available in full or in part for certain category of products and/or Services "
                          "and/or transactions and hence You may not be entitled to a refund in respect"
                          " of the Transactions for those products and /or Services.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "6. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Except for Cash on Delivery transaction, refund, if any, shall be "
                          "made at the same issuing bank/ through the same instrument from where transaction "
                          "price was received.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "7. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Refund shall be made in Indian Rupees only and shall be equivalent "
                          "to the transaction price received in Indian Rupees.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "8. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "For electronics payments and Cash on Delivery transactions, refunds, "
                          "if any, shall be made through payment facility using NEFT / RTGS or any other "
                          "online banking / electronic funds transfer system approved by Reserve Bank India (RBI).",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "9. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Refund shall be conditional and shall be with recourse available to "
                          "Vroom in case of any misuse by Buyer.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "10. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Refund shall be subject to Buyer complying with these Terms of Use"
                          " and the Vroom policies.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "11. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Vroom reserves the right to impose limits on the number of transactions"
                          " or transaction Price which Vroom may receive from on an individual valid Credit/Debit/"
                          " Cash Card / valid bank account/ and such other infrastructure or any other financial"
                          " instrument directly or indirectly through payment aggregator or through any such "
                          "facility authorized by Reserve Bank of India to provide enabling support facility "
                          "for collection and remittance of payment or by an individual Buyer during any time"
                          " period, and reserves the right to refuse to process transactions exceeding such limit.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "12. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Vroom reserves the right to refuse to process transactions by Buyers "
                          "with a prior history of questionable charges including without limitation breach "
                          "of any agreements by Buyer with Vroom or breach/violation of any law or any charges"
                          " imposed by issuing bank or breach of any policy.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "13. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Vroom may do such checks as it deems fit before approving the receipt "
                          "of/Buyers commitment to pay (for Cash on Delivery transactions) transaction price "
                          "from the Buyer for security or other reasons at the discretion of Vroom. As a result "
                          "of such check if Vroom is not satisfied with the creditability of the Buyer or "
                          "genuineness of the transaction / transaction price, it will have the right to "
                          "reject the receipt of / Buyers commitment to pay transaction price.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "14. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Vroom may delay notifying the payment confirmation i.e. informing Seller"
                          " to dispatch, if Vroom deems suspicious or for Buyers conducting high transaction "
                          "volumes to ensure safety of the transaction and transaction price. In addition,"
                          " Vroom may hold transaction price and Vroom may not inform Seller to dispatch,"
                          " remit transaction price to law enforcement officials (instead of refunding "
                          "the same to Buyer) at the request of law enforcement officials or in the event "
                          "the Buyer is engaged in any form of illegal activity.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "15. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "The Buyer and Seller acknowledge that Vroom will not be liable for"
                          " any damages, interests or claims etc. resulting from not processing a "
                          "transaction/transaction price or any delay in processing a transaction/transaction"
                          " Price which is beyond control of Vroom.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, ),
                child: Text("DISPUTES VIA CHARGEBACK",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, bottom: 6.0),
                child: Text("Whenever a chargeback (CB) comes from a payment gateway/bank, "
                    "following situations may arise:",
                )
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "1. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Item not received CB - Buyer hasn't received the item."
                          " Refund will be created in accordance with the dispute policies.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "2. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Unauthorized CB - Buyer hasn't made this particular transaction."
                          " Refund will be created in accordance with the dispute policies. Seller "
                          "expressly agrees that issuing the correct and complete invoice is the "
                          "sole and primary responsibility of the Seller.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, ),
              child: RichText(
                text: TextSpan(
                    children: <TextSpan> [
                      TextSpan(text: "3. ", style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)
                      ),
                      TextSpan(text: "Item not as described - meaning item is not what Buyer expected."
                          " Dispute will be decided in accordance with the dispute policies.",
                          style: TextStyle(color: Colors.black))
                    ]
                ),
              ),
            ),


            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, ),
                child: Text("LIMITATION OF LIABILITY",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0 ),
                child: Text("IN NO EVENT SHALL VROOM BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT OR "
                    "CONSEQUENTIAL DAMAGES OF ANY KIND IN CONNECTION WITH THESE TERMS OF USE,"
                    "EVEN IF USER HAS BEEN INFORMED IN ADVANCE OF THE POSSIBILITY OF SUCH DAMAGES.",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, ),
                child: Text("CONTACT US",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, bottom: 6.0),
                child: Text("Please contact us for any questions or comments (including all inquiries "
                    "unrelated to trademark/ copyright infringement) regarding this Website by electronic"
                    " or written communication at the address provided in the Contact Us section of this"
                    " Website. If you have any concern about privacy or grievances with the Website or"
                    " any of its content or to report any abuse of laws or breach of the Privacy Policy "
                    "and Terms of Use, please contact us (info@vroom.com.bd) with a thorough description"
                    " of such concerns/ issues faced by you and we will try to resolve the issue for you.",
                )
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, ),
                child: Text("SEVERABILITY",
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
            ),

            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, bottom: 6.0),
                child: Text("Any provision of these Terms of Use that is found to be prohibited or"
                    " unenforceable shall be ineffective to the extent of such prohibition or "
                    "unenforceability, without invalidating the remaining portions hereof.",
                )
            ),
          ],
        ),
      ),
    );
  }
}