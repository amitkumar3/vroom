


import 'package:flutter/material.dart';

class PrivacyPolicy extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Privacy Policy'),
        elevation: 8,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0),
                  child: Text('Background', style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16.0),
                  ),
                ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text("The Vroom services Limited application"
                    " (hereinafter referred to as “the application”) is owned by us."
                    " The application collects and stores certain personal data/information "
                    "from its users/members. We understand that your privacy is important to "
                    "you and that you care about how it is used and shared online. "
                    "We value the privacy of the users and everyone who uses the "
                    "application and therefore subject to the limited exceptions described "
                    "hereinafter we do not collect information about you. "
                    "All information that our customers / users provide is held in the"
                    " utmost care and security and will never be shared with any third"
                    " party except as described herein. We will never sell any personal information "
                    "to any third party and any information we collect will only be used in ways"
                    " that are useful to you and in a manner consistent with your rights and our"
                    " obligations under the law. This policy describes what information we collect,"
                    " how it is used and shared and other terms and conditions in connection "
                    "to use of such information. Please read this Privacy Policy carefully and"
                    " ensure that you understand it. Your acceptance of Our Privacy Policy is"
                    " deemed to occur upon your first use of the application. If you do not "
                    "accept and agree with this Privacy Policy, you must stop using our"
                    " application immediately."),
              ),
            ),
 
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0),
                child: Text('Definitions And Interpretation',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text('In this Policy the following terms shall have the following meanings:'
                    ' “Application” means this application known as “Vroom Services Limited”'
                    ' that you are currently using. “Data” means collectively all information '
                    'that you submit to us via the application. “Data Controller” means a person '
                    'or persons recruited by us for the purpose of controlling and processing data'
                    ' collected from the users. “Personal data” means collectively all information '
                    'we collect from you, from which you may be personally identified. Typically, '
                    'this may include your name, address, telephone number(including mobile number), '
                    'private car details, credit card information, email address, age, sex and family '
                    'status, but may also include other information such as lifestyle, hobbies and interests. '
                    'Please be assured that we will not collect any personal data from your visits to our '
                    'site unless you provide this information voluntarily or obtained from the users of the'
                    ' application by the application itself or through other third party. “Users/customer”'
                    ' means any third party that accesses the application and is not employed by us and acting '
                    'in the course of their employment.'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
                child: Text('Services Used In Collecting Personal Data',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text('Apart from the application itself, '
                    'Personal Data is collected using the '
                    'following services:'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
                  child: Text('1. Registration and authentication',
                  style: TextStyle(fontWeight: FontWeight.w700),
                )
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 20.0, top: 4.0),
                child: Text('• Direct Registration'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 8, right: 8.0, top: 6.0),
                  child: Text('2. Location-based interactions',
                    style: TextStyle(fontWeight: FontWeight.w700),
                  )
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 20.0, top: 4.0),
                child: Text('• Geographical location'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                  child: Text('Personal Data: Geographic position',
                    style: TextStyle(fontWeight: FontWeight.normal),
                  )
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
                  child: Text('3. Analytics',
                    style: TextStyle(fontWeight: FontWeight.w700),
                  )
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 20.0, top: 4.0),
                child: Text('• Google Analytics'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                  child: Text('Personal Data: Cookie and Usage data',
                    style: TextStyle(fontWeight: FontWeight.normal),
                  )
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
                  child: Text('4. Access to third party service\'s accounts',
                    style: TextStyle(fontWeight: FontWeight.w700),
                  )
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 20.0, top: 4.0),
                child: Text('• Access to the Facebook account'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
                  child: Text('Permissions: About Me and Email',
                    style: TextStyle(fontWeight: FontWeight.normal),
                  )
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
                child: Text('Security',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, top: 4.0),
                child: Text("The security of your Personal Information is important to us, "
                    "but remember that no method of transmission over the Internet,"
                    " or method of electronic storage, is 100% secure. While we strive "
                    "to use commercially acceptable means to protect your Personal Information, "
                    "we cannot guarantee its absolute security. We take reasonable precautions "
                    "to keep your personal data secure, and require third party Data Processors"
                    " to do likewise. All personally identifiable information is subject to "
                    "restricted access to prevent modification, unauthorized access or misuse. "
                    "Please note, however, that we may release your personal data if required to"
                    " do so by law, or by search warrant, subpoena or court order."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
                child: Text('Communications',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
                child: Text("We may use your Personal Information to contact you with newsletters, "
                    "marketing or promotional materials and other information."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6, bottom: 4.0),
                child: Text('Scope - What Does This Policy Cover?',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0,),
                child: Text("This Privacy Policy applies only to your use of the application "
                    "and it does not extend to any websites/apps that are linked to from the "
                    "application (whether we provide those links or whether they are shared by "
                    "other users). We have no control over how your data is collected, stored or"
                    " used by other websites and we advise you to check the privacy policies of "
                    "any such websites before providing any data to them."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0),
                child: Text('Types Of Data/Information Collected',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text("In order to use the Application users are required to provide"
                    " pertinent contact information such as, their Name, Phone/Mobile Number,"
                    " Country, geographical location, Cookie, Usage data, gender and email address."
                    " The application collects GPS (Location Service API) details to display data most"
                    " relevant to your location. We use a third-party tracking service (Google Analytics)"
                    " that uses cookies (log files) to track non-personally identifiable information about "
                    "visitors to our application in the aggregate (usage, volume statistics and referrals) "
                    "to monitor the traffic flow and provide targeted service referrals. After subscribing,"
                    " users may also provide personally identifiable information for other purposes for the"
                    " sole purpose of the applications to function in tedium to what they are supposed. "
                    "Any transmission of these materials can only be achieved through the initiative of the user."
                    " The Personal Data may be freely provided by the User, or collected automatically when "
                    "using this Application. We do not collect personal information from the users unless they "
                    "voluntarily and knowingly provide it to us. Any use of Cookies - or of other tracking "
                    "tools - by this Application or by the owners of third party services used by this Application,"
                    " unless stated otherwise, serves to identify Users and remember their preferences, for the "
                    "sole purpose of providing the service required by the User. Failure to provide certain "
                    "Personal Data may make it impossible for this Application to provide its services. Users are "
                    "solely responsible for any Personal Data of third parties obtained, published or shared through"
                    " this Application and confirm that they have the third party's consent to provide the Data to us."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0),
                child: Text('Mode And Place Of Processing The Data',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, top: 6.0),
                child: Text('Methods Of Processing',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0
                ),
                child: Text("Data of users are processed in a proper manner and appropriate security "
                    "measures shall be taken to prevent unauthorized access, disclosure, modification,"
                    " or unauthorized destruction of the Data. The Data processing is carried out using"
                    " computers and/or IT enabled tools, following organizational procedures and modes"
                    " strictly related to the purposes indicated. In addition to the Data Controller,"
                    " in some cases, the Data may be accessible to certain types of persons in charge,"
                    " involved with the operation of the site (administration, sales, marketing, legal,"
                    " system administration) or external parties (such as third party technical service"
                    " providers, mail carriers, hosting providers, IT companies, communications agencies)"
                    " appointed, if necessary, as Data Processors by the Owner. The updated list of these"
                    " parties may be requested from the Data Controller at any time."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
                child: Text('Place Of Processing And Storing Data',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, top: 4.0),
                child: Text("The Data is processed at the Data Controller's operating offices"
                    " and in any other places where the parties involved with the processing "
                    "are located. For further information regarding location of storage of data,"
                    " you are advised to contact the Data Controller."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0),
                child: Text('Retention Time',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                child: Text('1. The Data is kept for the time necessary to provide the service requested '
                    'by the Users, or stated by the purposes outlined in this document, and the users'
                    ' can always request that the Data Controller suspend or remove the data.'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0,right:8.0, top: 8.0),
                child: Text('2. We only keep your data for as long as we need to in order to use'
                    ' it as described here in and/or for as long as we have your permission to keep it.'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right:8.0, top: 8.0),
                child: Text('3. Data security is of great importance to us, and to protect your data '
                    'we have taken suitable measures to safeguard and secure any data we hold about you.'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right:8.0, top: 8.0),
                child: Text('4. Steps we take to secure and protect your data include:'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 20.0, right:8.0, top: 4.0),
                child: Text('• Using our owned server to store the data'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 20.0, right:8.0, top: 4.0),
                child: Text('• Data is encrypted'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 20.0, right: 8.0, top: 4.0, bottom: 8.0),
                child: Text('• Only authorized person has access to use the data'),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0 ,bottom: 4.0),
                child: Text('The Use Of The Collected Data',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text("The Data concerning the Users is collected to allow us to provide our services,"
                    " as well as for the following purposes:"),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                child: Text("• Access to third party services' accounts, Analytics, "
                    "Location-based interactions and Registration and authentication."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• When you supply personal information about yourself for a "
                    "specific purpose, we use the information for only that purpose such as,"
                    " to provide the service or information you have requested."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• We use your information to send you marketing communications"
                    " and to communicate with you about our Services and let you know about"
                    " our policies and terms."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• We may use personal data you submit to us when ordering goods or"
                    " services from us for the purpose of fulfilling that order, and it may be"
                    " necessary for us to share such data with third parties such as the card "
                    "payment services provider or carrier of goods."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• Your contact information may also be used to contact you when "
                    "necessary in order to respond to your inquiries regarding the service."
                    " Every communication with us is held in strictest confidence and handled "
                    "with integrity. None of your personal information will be disclosed without"
                    " you taking affirmative action to disclose this information except to facilitate"
                    " the service as may be required"),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• Your personal information will not be disclosed to third parties"
                    " under any circumstances, except as disclosed in this privacy policy. "
                    "Any general demographic information that we receive will never be utilized"
                    " to identify you."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• We will never sell, share, rent, or trade any personal information"
                    " about any of our users to any third parties for their promotional or other"
                    " purposes. We will never sell, share, rent, or trade your email address or"
                    " any other contact information to any third parties for promotional purposes "
                    "or any other purposes."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• We will not share any information relating to your situation "
                    "with any third parties, except when required by law. Furthermore, "
                    "we will not share any information from our services with any third parties,"
                    " except when required by law. Any such information which you provide to us "
                    "or is transmitted over our servers will remain completely confidential."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• We use cookies to identify your browser as you visit pages on the site."
                    " The cookies are used to present dynamic content so that you are not presented "
                    "with identical content on every visit."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
                child: Text("• The User's Personal Data may be used for legal purposes by the Data "
                    "Controller, in Court or in the stages leading to possible legal action arising"
                    " from improper use of this Application or the related services."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0),
                child: Text('Security',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, ),
                child: Text('Legal Disclaimer',
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• We may disclose personal information when required by law or "
                    "in the good-faith belief that such action is necessary in order to conform "
                    "to the edicts of the law or comply with legal process served on us."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• Notwithstanding the security measures that we take, it is important to remember"
                    " that the transmission of data via the internet may not be completely secure and that you"
                    " are advised to take suitable precautions when transmitting to us data via the internet."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
                child: Text("• The User declares to be aware that the Data Controller may be required to "
                    "reveal personal data in such cases or upon request of public authorities."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, top: 6.0),
                child: Text("Additional Information About User's Personal Data",
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text("In addition to the information contained in this privacy policy,"
                    " this Application may provide the User with additional and contextual "
                    "information concerning particular services or the collection and processing "
                    "of Personal Data upon request."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6, bottom: 4),
                child: Text("System Logs And Maintenance",
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text("For operation and maintenance purposes, this Application and any "
                    "third party services may collect files that record interaction with this "
                    "Application (System Logs) or use for this purpose other Personal Data (such as IP Address)."),
              ),
            ),


            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6, bottom: 4.0),
                child: Text("Information Not Contained In This Policy",
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text("More details concerning the collection or processing of Personal Data may"
                    " be requested from the Data Controller at any time."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0),
                child: Text("The Rights Of Users",
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text("Users have the right, at any time, to know whether their Personal "
                    "Data has been stored and can consult the Data Controller to learn about"
                    " their contents and origin, to verify their accuracy or to ask for them"
                    " to be supplemented, canceled, updated or corrected, or for their transformation"
                    " into anonymous format or to block any data held in violation of the law, "
                    "as well as to oppose their treatment for any and all legitimate reasons."
                    " Besides, users have the right to withdraw his consent to the processing of his"
                    " personal data to which he has previously consented, to obtain a copy of the "
                    "personal data which we hold about users subject to any reasonable fee, to object"
                    " to the processing of your personal data for the purpose of direct marketing and "
                    "to have incorrect personal data we hold about users corrected. Requests should be"
                    " sent to the Data Controller at the contact information set out herein: @gmail.com "
                    "It is to be noted that this Application does not support “Do Not Track” requests."
                    " To determine whether any of the third party services it uses honor the “Do Not Track” "
                    "requests, please read their privacy policies."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6, bottom: 4.0),
                child: Text("Erasure Of Personal Data",
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0, ),
                child: Text("Users may request that we erase the personal data we hold about them "
                    "in the following circumstances:"),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right:8.0, ),
                child: Text("• It is no longer necessary for us to hold that personal data with "
                    "respect to the purpose for which it was originally collected or processed;"),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• The user wishes to withdraw his consent to holding and processing their personal data;"),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• The user objects to us holding and processing their personal data;"),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• The personal data has been processed unlawfully;"),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• The personal data needs to be erased in order for the Company"
                    " to comply with a particular legal obligation;"),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, top: 6.0),
                child: Text("Unless we have legitimate grounds to refuse to erase personal data, "
                    "all requests for erasure shall be complied with, and the users shall be "
                    "informed of the erasure, within one month of receipt of the data subject’s "
                    "request (this can be extended by up to two months in the case of complex "
                    "requests, and in such cases the users shall be informed of the need "
                    "for the extension)."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
                child: Text("Objections To Personal Data Processing",
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: Text("• Users have the right to object to us processing their personal data based"
                    " on legitimate interests (including profiling), direct marketing (including profiling),"
                    " [and processing for scientific and/or historical research and statistics purposes]."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 4.0),
                child: Text("• Where users object to us processing their personal data for the above mentioned"
                    " purposes, we shall cease such processing forthwith unless it can be demonstrated that our"
                    " legitimate grounds for such processing override the users’ interests."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0),
                child: Text("Data Breach Notification",
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text("All personal data breaches must be reported immediately to us along with"
                    " details of the measures taken, or proposed to be taken, by us to address the "
                    "breach including, where appropriate, measures to mitigate its possible adverse"
                    " effects so we can initiate necessary action for protection of such data."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0),
                child: Text("Information About This Privacy Policy",
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0, top: 4.0),
                child: Text("The Data Controller is responsible for implementing and ensuring this privacy policy."),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 4.0),
                child: Text("Changes To This Privacy Policy",
                  style: TextStyle(fontWeight: FontWeight.w700,
                      fontSize: 16.0),
                ),
              ),
            ),

            Center(
              child: Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
                child: Text("We may change this Privacy Policy as we may deem necessary from time"
                    " to time, or as may be required by law. Any changes will be immediately posted"
                    " on this page and you will be deemed to have accepted the terms of the Privacy "
                    "Policy on your first use of the application following the alterations. "
                    "It is strongly recommended to check this page often, referring to the date of the "
                    "last modification listed at the bottom. If a User objects to any of the changes "
                    "to the Policy, the User must cease using this Application and can request that "
                    "the Data Controller removes the Personal Data. Unless stated otherwise, the then "
                    "current privacy policy applies to all Personal Data the Data Controller has about Users."),
              ),
            ),
          ],
        ),
      ),
    );
  }
}