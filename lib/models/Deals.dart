class Deals {
  final int id;
  final String image;
  final String title;
  final String description;
  final int status;

  Deals._({this.id, this.image, this.title, this.description, this.status});

  factory Deals.fromJson(Map<String, dynamic> json) {
    return new Deals._(
        id: json['id'],
        image: json['image'],
        title: json['title'],
        description: json['description'],
        status: json['status']);
  }

  String toString() {
    return "${this.image}, ${this.title}, ${this.description}";
  }
}
