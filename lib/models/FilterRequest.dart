import 'dart:convert';

class FilterRequest {
  Map<String, dynamic> filters = {};
  Map<String, dynamic> filterNames = {};
  Map<String, List> selectedFilters = {};
  bool priceFilter = false;
  double minPrice, maxPrice, selectedMinPrice, selectedMaxPrice;
  void addFilter(String key, String name, Map filter) {
    filters[key] = filter;
    filterNames[key] = name;
  }

  dynamic getFilter(key) {
    return filters[key];
  }

  String getFilterName(key) {
    return filterNames[key];
  }

  Map getFilterNames() {
    return filterNames;
  }

  double getMinPrice() {
    return minPrice;
  }

  double getMaxPrice() {
    return maxPrice;
  }

  bool getPriceFilter() {
    return priceFilter;
  }

  void setPriceRange(double min, double max) {
    maxPrice = max;
    minPrice = min;
    if (selectedMinPrice == null) selectedMinPrice = min;
    if (selectedMaxPrice == null) selectedMaxPrice = max;
  }

  List getSelectedPriceRange() {
    return [selectedMinPrice, selectedMaxPrice];
  }

  void resetPriceFilter() {
    priceFilter = false;
    selectedMinPrice = minPrice;
    selectedMaxPrice = maxPrice;
  }

  void setSelectedPriceRange(List priceRange) {
    priceFilter = true;
    selectedMinPrice = priceRange[0];
    selectedMaxPrice = priceRange[1];
  }

  void removeFilterItem(String filterKey, var itemKey) {
    selectedFilters[filterKey].remove(itemKey);
  }

  String getFilterItem(var filterKey, var itemKey) {
    return filterNames[filterKey] + ": " + filters[filterKey][itemKey].name;
  }

  void setSelected(String key, List value) {
    selectedFilters[key] = value;
  }

  List<int> getSelected(String key) {
    if (selectedFilters.keys.contains(key))
      return selectedFilters[key];
    else
      return [];
  }

  void reset() {
    selectedFilters = {};
    resetPriceFilter();
  }

  String getFilterParamsURL() {
    Map url = {};

    for (var i in selectedFilters.keys) {
      if (selectedFilters[i].length != 0) {
        url[i] = selectedFilters[i];
      }
    }

    if (url.keys.length == 0 &&
        selectedMinPrice == minPrice &&
        selectedMaxPrice == maxPrice) return "";
    return "catfilterval=" +
        json.encode(url) +
        "&minp=" +
        selectedMinPrice.toInt().toString() +
        "&maxp=" +
        selectedMaxPrice.toInt().toString();
  }

  Map getSelectedFilters() {
    return selectedFilters;
  }
}
