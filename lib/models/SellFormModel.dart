import 'dart:core';
import 'dart:io';
import 'package:async/async.dart';
import 'package:sample_flutter/constants/EndPoints.dart';
import 'package:sample_flutter/models/ProductDetailModel.dart';
import 'package:sample_flutter/tools/app_tools.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:path/path.dart';

class SellFormModel {
  String token;
  bool authenticated = true;
  Map data = {};
  Map vehicleConditions = {};
  Map vehicleTypes = {};
  Map brands = {};
  Map models = {};
  Map fuelTypes = {};
  Map variants = {};
  Map locations = {};
  Map colors = {};
  Map bodyTypes = {};
  Map transmissionTypes = {};
  Map registrationYears = {};
  Map owners = {};

  List<File> images;
  Future setup() async {
    token = await getDataLocally(key: 'token');
    print(token);
    var resp = await http
        .get(EndPoints.preSubmitListing, headers: {"token": token});
    var parsedResponse = json.decode(resp.body);
    if (parsedResponse['error'] == 'Token is Expired' ||
        parsedResponse['error'] == 'Token is Invalid') {
      authenticated = false;
      return;
    }

    for (var i in parsedResponse['parentcategories']) {
      vehicleConditions[i['id']] = VehicleCondition.fromJson(i);
    }

    for (var i in parsedResponse['exteriorColor']) {
      colors[i['id']] = ExteriorColor.fromJson(i);
    }
    for (var i in parsedResponse['yearofregister']) {
      registrationYears[i['id']] = RegistrationYear.fromJson(i);
    }
    for (var i in parsedResponse['locations']) {
      locations[i['id']] = Cities.fromJson(i);
    }

    for (var i in parsedResponse['bodytypes']) {
      bodyTypes[i['id']] = BodyTypeSellForm.fromJson(i);
    }
//    owners = getStaticOwners();
//    transmissionTypes = getStaticTransmissionTypes();
  }

  get vehicleCondition => data['mcategory_name'];
  Future<void> setVehicleCondition(value) async {
    data['mcategory_name'] = value;
    await setVehicleTypes();
  }

  get vehicleType => data['category_name'];
  Future<void> setVehicleType(value) async {
    data['category_name'] = value;
    await setBrands();
  }

  get brandName => data['brand_name'];
  Future<void> setBrandName(value) async {
    data['brand_name'] = value;
    await setModels();
  }

  get modelName => data['model_name'];
  Future<void> setModelName(value) async {
    data['model_name'] = value;
    await setFuelTypes();
  }

  get fuelType => data['fuel_type'];
  Future<void> setFuelType(value) async {
    data['fuel_type'] = value;
    await setFuelTypeVariants();
  }

  get variant => data['varient_id'];
  set variant(value) => data['varient_id'] = value;

  Future<void> setVehicleTypes() async {
    var url =
        "pcatId=" + data['mcategory_name'].toString() + "&type=vehicletype";
    print(url);
    var parsedResponse = await authenticatedPostRequest(url);

    // print(parsedResponse['msg']['categories']);
    vehicleTypes = {};
    for (var i in parsedResponse['msg']['categories'])
      vehicleTypes[i['id']] = Categories.fromJson(i);
  }

  Future<void> setBrands() async {
    var url = "catId=" + data['category_name'].toString() + "&type=brand";
    print(url);
    var parsedResponse = await authenticatedPostRequest(url);
    brands = {};
    for (var i in parsedResponse['msg']['brands'])
      brands[i['id']] = SellFormBrands.fromJson(i);
  }

  Future<void> setModels() async {
    var url = "brandId=" + data['brand_name'].toString() + "&type=model";
    print(url);
    var parsedResponse = await authenticatedPostRequest(url);
    models = {};
    for (var i in parsedResponse['msg']['models'])
      models[i['id']] = SellFormBrand.fromJson(i);
  }

  Future<void> setFuelTypes() async {
    var url = "brandId=" + data['model_name'].toString() + "&type=fuelType";
    print(url);
    variants = {};

    var parsedResponse = await authenticatedPostRequest(url);
    // print("fuelType response");
    // print(parsedResponse);
    // fuelTypes["1"] = FuelType.fromJson({"id": 1, "name": "Petrol"});
    // fuelTypes["2"] = FuelType.fromJson({"id": 2, "name": "Diesel"});
    for (var i in parsedResponse['msg']['models'])
      fuelTypes[i['id']] = FuelType.fromJson(i);
  }

  Future<void> setFuelTypeVariants() async {
    var url = "brandId=" + data['model_name'].toString() + "&type=varients";
    print(url);
    var parsedResponse = await authenticatedPostRequest(url);
    print(parsedResponse['msg']);
    variants = {};
    for (var i in parsedResponse['msg']['fuleTypeVarients'])
      variants[i['id']] = VariantSellForm.fromJson(i);
  }

  String validateSelect(v) {
    if (v == "") return "This Field is Required";
    return null;
  }

  String validatePrice(String val) {
    if (val == "") return "This field is required";
    try {
      double.tryParse(val);
      return null;
    } catch (e) {
      return "Invalid Price";
    }
  }

  String validateKmDriven(String val) {
    if (val == "") return "This field is required";
    try {
      int.tryParse(val);
      return null;
    } catch (e) {
      return "Invalid Price";
    }
  }

  String validateModelNo(String val) {
    if (val == "") return "This field is required";

    return null;
  }

  get price => data['price'];
  void savePrice(String val) {
    data['price'] = val;
  }

  get kmDriven => data['kilometer_driven'];
  void saveKmDriven(String val) {
    data['kilometer_driven'] = val;
  }

  get model => data['model_no'];
  void saveModelNo(String val) {
    data['model_no'] = val;
  }

  get registrationYear => data['year_of_registers'];
  void saveRegistrationYear(val) {
    data['year_of_registers'] = val;
  }

  get location => data['location_id'];
  void saveLocation(v) {
    data['location_id'] = v;
  }

  get owner => data['number_of_owners'];
  void saveOwner(v) {
    data['number_of_owners'] = v;
  }

  get color => data['color_id'];
  void saveColor(v) {
    data['color_id'] = v;
  }

  get bodyType => data['body_type'];
  void saveBodyType(v) {
    data['body_type'] = v;
  }

  get transmissionType => data['transmission_type'];
  void saveTransmissionType(v) {
    data['transmission_type'] = v;
  }

  get imageList => images;
  void saveImages(List<File> imageList) {
    images = imageList;
  }

  dynamic authenticatedPostRequest(String url) async {
    var resp = await http
        .post(EndPoints.modelDetails + url, headers: {"token": token});
    var parsedResponse = json.decode(resp.body);
    return parsedResponse;
  }

  Future<String> submitListing() async {
    print("submit lisitng called");
    var uri = Uri.parse(
        "http://vroom.infini.work/api/save-product?responseType=json");
    var request = http.MultipartRequest("POST", uri);
    request.headers['token'] = token;
    // print(data);
    // return;
    for (var i in data.keys) request.fields[i] = data[i].toString();
    for (File file in images)
      request.files.add(new http.MultipartFile(
          "images[]",
          new http.ByteStream(DelegatingStream.typed(file.openRead())),
          await file.length(),
          filename: basename(file.path))
      );
    
    print(request.files);
    // request.
    http.StreamedResponse sres = await request.send();
    final res = await http.Response.fromStream(sres);
    final parsedResponse = json.decode(res.body);
    print(parsedResponse);
    if (parsedResponse['status'] == 1) {
      return "Application submitted successfully";
    }
    return "Error while processing request please try again later";
  }
}
