
import 'package:sample_flutter/models/ProductDetailModel.dart';
import 'package:sample_flutter/tools/app_tools.dart';

class Vehicle {
  final int id;
  final int brandId;
  final int modelId;
  final int token_discount;
  final String product_name;
  final String product_price;
  final String product_variant;
  final String product_location;
  final int product_kilometers_driven;
  final int product_year;
  final String product_no_of_owners;
  final String product_model_no;
  final String product_model_name;
  final String product_brand_name;
  final List<VehicleImage> images;
  final ExteriorColor exteriorColor;
  final Brands brands;
  //final Categories categories;
  final BrandModels brandModels;
  final FuelType fuelType;
  final Cities cities;
  final BodyType bodyType;
  final TransmissionData transmissionData;
  final VehicleCondition vehicleCondition;

  //final String product_image;

  Vehicle._(
      {this.id,
        this.brandId,
        this.modelId,
        this.token_discount,
        this.product_name,
        this.product_price,
        this.product_variant,
        this.product_location,
        this.product_kilometers_driven,
        this.product_year,
        this.product_no_of_owners,
        this.product_model_no,
        this.product_model_name,
        this.product_brand_name,
        this.images,
        this.exteriorColor,
        this.brands,
        //this.categories,
        this.brandModels,
        this.fuelType,
        this.cities,
        this.bodyType,
        this.transmissionData,
        this.vehicleCondition});

  //this.product_image

  factory Vehicle.fromJson(
      Map<String, dynamic> json,
      Map exteriorColor,
      Map brands,
      //Map categories,
      Map brandModels,
      Map fuelType,
      Map cities,
      Map bodyType,
      Map transmissionData,
      Map vehicleCondition) {
    var abc = (json['catalogimgs'] as List).map((data) {
      return VehicleImage.fromJson(data);
    }).toList();
    return new Vehicle._(
      id: json['id'],
      brandId: json['brand_id'] == null ? "NA" : json['brand_id'],
      modelId: json['model_id'] == null ? "NA" : json['model_id'] ,
      token_discount: json['token'] == null ? "NA" : json['token'],
      product_name: vehicleDetailValue(json['product_name']),
      product_price: json['price'] == null ? 0 : json['price'],
      product_variant: json['varient_name'] == null ? "NA" : json['varient_name'],
   //   product_variant: json['name'],
      product_location: json['location_name'] == null ? "NA" : json['location_name'],
      product_kilometers_driven: json['kilometer_driven'] == null ? 0 : json['kilometer_driven'],
      product_year: json['year_of_register_id'] == null ? 0 : json['year_of_register_id'],
      product_no_of_owners: json['number_of_owners'] == null ? "NA" : json['number_of_owners'],
      product_model_no: json['model_no'] == null ? "NA" : json['model_no'],
      product_model_name: json['model_name'] == null ? "NA": json['model_name'],
      product_brand_name: json['brand_name'] == null ? "NA" : json['brand_name'],
      images: abc,
      exteriorColor: exteriorColor[json['color_id']],
      brands: brands[json['brand_id'] == null ? 0 : json['brand_id']],
      //categories: categories[json['color_id']],
      brandModels: brandModels[json['model_id'] == null ? 0 : json['model_id']],
      fuelType: fuelType[json['fuel_type'] == null ? "NA" : json['fuel_type']],
      cities: cities[json['location_id'] == null ? 0 : json['location_id']],
      bodyType: bodyType[json['body_type'] == null ? "NA" : json['body_type']],
      transmissionData: transmissionData[json['transmission_type']],
      vehicleCondition: vehicleCondition[json['mcat_id']],
      //images: abc as List<VehicleImage>,
    );
  }

  String toString() {
    return "${this.product_name}, ${this.product_price}, ${this.product_variant}, "
        "${this.product_location}, ${this.product_no_of_owners}, ${this.product_model_no}, "
        "${this.product_model_name}, ${this.product_brand_name}";
  }
}

class VehicleImage {
  final int imageId;
  final String imageName;

  VehicleImage({this.imageId, this.imageName});

  factory VehicleImage.fromJson(Map<String, dynamic> parsedJson) {
    return VehicleImage(
        imageId: parsedJson['id'], imageName: parsedJson['filename']);
  }
}
