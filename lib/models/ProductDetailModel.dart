
class ExteriorColor {
  final int id;
  final String name;

  ExteriorColor._({
    this.id,
    this.name,
  });

  //this.product_image

  factory ExteriorColor.fromJson(Map<String, dynamic> json) {
    return new ExteriorColor._(id: json['id'], name: json['color_name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class Categories {
  final int id;
  final String name;

  Categories._({
    this.id,
    this.name,
  });

  //this.product_image

  factory Categories.fromJson(Map<String, dynamic> json) {
    return new Categories._(id: json['id'], name: json['category']);
  }

  String toString() {
    return "${this.name}";
  }
}

class BrandModels {
  final int id;
  final String name;

  BrandModels._({
    this.id,
    this.name,
  });

  //this.product_image

  factory BrandModels.fromJson(Map<String, dynamic> json) {
    return new BrandModels._(id: json['id'], name: json['model_name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class FuelType {
  final int id;
  final String name;

  FuelType._({
    this.id,
    this.name,
  });

  //this.product_image

  factory FuelType.fromJson(Map<String, dynamic> json) {
    return new FuelType._(id: json['id'], name: json['name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class Cities {
  final int id;
  final String name;

  Cities._({
    this.id,
    this.name,
  });

  //this.product_image

  factory Cities.fromJson(Map<String, dynamic> json) {
    return new Cities._(id: json['id'], name: json['location_name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class BodyType {
  final int id;
  final String name;

  BodyType._({
    this.id,
    this.name,
  });

  //this.product_image

  factory BodyType.fromJson(Map<String, dynamic> json) {
    return new BodyType._(id: json['id'], name: json['body_type_name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class BodyTypeSellForm {
  final int id;
  final String name;

  BodyTypeSellForm._({
    this.id,
    this.name,
  });

  //this.product_image

  factory BodyTypeSellForm.fromJson(Map<String, dynamic> json) {
    return new BodyTypeSellForm._(id: json['id'], name: json['name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class TransmissionData {
  final int id;
  final String name;

  TransmissionData._({
    this.id,
    this.name,
  });

  //this.product_image

  factory TransmissionData.fromJson(Map<String, dynamic> json) {
    return new TransmissionData._(id: json['id'], name: json['name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class Category {
  final int id;
  final String name;

  Category._({
    this.id,
    this.name,
  });

  //this.product_image

  factory Category.fromJson(Map<String, dynamic> json) {
    return new Category._(id: json['id'], name: json['category']);
  }

  String toString() {
    return "${this.name}";
  }
}

class VehicleCondition {
  final int id;
  final String name;

  VehicleCondition._({this.id, this.name});

  //this.product_image

  factory VehicleCondition.fromJson(Map<String, dynamic> json) {
    return new VehicleCondition._(id: json['id'], name: json['category']);
  }

  String toString() {
    return "${this.name}";
  }
}

class VehicleType {
  final int id;
  final String name;

  VehicleType._({this.id, this.name});

  //this.product_image

  factory VehicleType.fromJson(Map<String, dynamic> json) {
    return new VehicleType._(id: json['id'], name: json['category']);
  }

  String toString() {
    return "${this.name}";
  }
}

class Variant {
  final int id;
  final String name;

  Variant._({this.id, this.name});

  //this.product_image

  factory Variant.fromJson(Map<String, dynamic> json) {
    return new Variant._(id: json['id'], name: json['category']);
  }

  String toString() {
    return "${this.name}";
  }
}

class RegistrationYear {
  final int id;
  final String name;

  RegistrationYear._({this.id, this.name});

  //this.product_image

  factory RegistrationYear.fromJson(Map<String, dynamic> json) {
    return new RegistrationYear._(id: json['id'], name: json['name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class SellFormBrands {
  final int id;
  final String name;
  final String image;
  SellFormBrands._({this.id, this.name, this.image});

  //this.product_image

  factory SellFormBrands.fromJson(Map<String, dynamic> json) {
    return new SellFormBrands._(
        id: json['id'], name: json['name'], image: json['image']);
  }

/*String toString() {
    return "${this.brand_name}";
  }*/
}

class Brands {
  final int id;
  final String name;
  final String image;
  Brands._({this.id, this.name, this.image});

  //this.product_image

  factory Brands.fromJson(Map<String, dynamic> json) {
    return new Brands._(
        id: json['id'], name: json['brand_name'], image: json['image']);
  }

/*String toString() {
    return "${this.brand_name}";
  }*/
}

class SellFormVariant {
  final int id;
  final String name;

  SellFormVariant._({this.id, this.name});

  //this.product_image

  factory SellFormVariant.fromJson(Map<String, dynamic> json) {
    return new SellFormVariant._(id: json['id'], name: json['name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class SellFormBrand {
  final int id;
  final String name;

  SellFormBrand._({this.id, this.name});

  //this.product_image

  factory SellFormBrand.fromJson(Map<String, dynamic> json) {
    return new SellFormBrand._(id: json['id'], name: json['name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class VariantSellForm {
  final int id;
  final String name;

  VariantSellForm._({this.id, this.name});

  //this.product_image

  factory VariantSellForm.fromJson(Map<String, dynamic> json) {
    return new VariantSellForm._(id: json['id'], name: json['name']);
  }

  String toString() {
    return "${this.name}";
  }
}

class Owner {
  final String id;
  final String name;

  Owner._({this.id, this.name});

  //this.product_image

  factory Owner.fromJson(Map<String, dynamic> json) {
    return new Owner._(id: json['id'], name: json['name']);
  }

  String toString() {
    return "${this.name}";
  }
}
