
import 'dart:core';

class BrowseVehicleModels
{
  List sliders = [];
  List<List<CategoryModel>> sliderItems = [];

  BrowseVehicleModels.fromJson(data) {
    //print(data);
    for( var slider in data){
      sliders.add(slider['category']);
      List<CategoryModel> items = [];
      for(var child in slider['categories']){
        items.add(CategoryModel.fromJson(child));
      }
      sliderItems.add(items);
    }
  }

  List getSliders() {
    return sliders;
  }

  List<CategoryModel> getSliderItems(int index) {
    print(sliders[index]);
    return sliderItems[index];
  }

}

class CategoryModel
{
  int id;
  String name;
  String fontAwesomeClass;

  CategoryModel.fromJson(data)
  {
    this.id = data['id'];
    this.name = data['category'];
    this.fontAwesomeClass = data['icon'].replaceAll("fas ", "");
  }

}