class AppStrings {
  static final String TEXT_REVIEW_HINT = "Write your review here...";

  static final String TEXT_UPDATE_PROFILE = "Update Profile";

  //NO RESULTS & SUGGESTIONS TEXT
  static final String TEXT_NO_RESULTS_FOUND = "No Results Found.";
  static final String TEXT_SUGGESTIONS = "Suggestions:";
  static final String TEXT_SUGGESTION_POINT_1 = "• Make Sure that all words are spelled correctly.";
  static final String TEXT_SUGGESTION_POINT_2 = "• Try different keywords.";
  static final String TEXT_SUGGESTION_POINT_3 = "• Try more general keywords.";
  static final String TEXT_SUGGESTION_POINT_4 = "• Try fewer keywords.";

  //FILTER TITLE TEXT
  static final String TEXT_FILTER_TITLE = "Refine your Search";

  //GENERAL TEXTS
  static final String TEXT_ERROR = "Error";
  static final String TEXT_SUCCESS = "Success";
  static final String TEXT_OKAY = "Okay";
  static final String TEXT_YES = "Yes";
  static final String TEXT_NO = "No";
  static final String TEXT_WENT_WRONG = "Something went Wrong!";
}
