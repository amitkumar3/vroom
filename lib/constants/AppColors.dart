class AppColors {
  static final String COLOR_TRANSPARENT = "#00000000";
  static final String COLOR_TEXT_BLACK = "#000000";
  static final String COLOR_TEXT_WHITE = "#ffffff";
  static final String COLOR_APP_GREY = "#7C7C7C";
  static final String COLOR_LIGHT_GREY = "#d6d6d6";
  static final String COLOR_GREY_BACKGROUND = "#EFEFEF";
  static final String COLOR_ICONS = "#7C7C7C";
  static final String COLOR_ICONS_TEXT = "#7C7C7C";

//static final String COLOR_VROOM_THEME = "#e92918"; //THEME RED
  static final String COLOR_VROOM_THEME = "#FCAF17"; //THEME YELLOW
  static final String COLOR_DARK_BLACK = "#000000";
}
