import 'package:http/http.dart';

enum Environment { development, uat, production }

class EndPoints {
  static final Environment environment = Environment.development;

  static final String developmentDomain = "https://vroom.infini.work";
  static final String uatDomain = "https://vroom.infini.work";
  static final String productionDomain = "https://vroom.infini.work";

  static final String suffix = "/api/";

  static String getDomain() {
    switch (environment) {
      case Environment.development:
        return developmentDomain;

      case Environment.uat:
        return uatDomain;

      case Environment.production:
        return productionDomain;
    }
    return developmentDomain;
  }

  static String getAPIPrefix() {
    return getDomain() + suffix;
  }

  static String getPrefix() {
    return getDomain();
  }

  static final String modelDetails =
      getAPIPrefix() + "model-details?responseType=json&";

  static final String userLogin =
      getAPIPrefix() + "check-user?responseType=json";

  static final String preSubmitListing =
      getAPIPrefix() + "add-post?responseType=json";

  static final String checkExistingUser =
      getAPIPrefix() + "check-existing-user?";

  static final String userForgetPassword =
      getAPIPrefix() + "forget-password?username=";

  static final String imageEndpoint =
      getDomain() + "public/Admin/uploads/catalog/products/";

  static final String getCarListing = getAPIPrefix() + "get-car-listing?";

  static final String getSearchCarListing =
      getAPIPrefix() + "get-search-results?autosearch=0&q=";

}
