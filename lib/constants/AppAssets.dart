class AppAssets {
  //Logo
  static final ASSET_APP_LOGO = "assets/vroom_logo_yellow.png";
  static final BANNER_1 = "assets/Banner1.jpg";
  static final BANNER_2 = "assets/Banner2.jpg";
  static final SELL_ICON = "assets/sell_icon.png";
  static final SELL_BTN = "assets/sell_btn.png";
  static final CHANGE_PWD_LOGO = "assets/password_lock.svg";
  static final EDIT_PROFILE_LOGO = "assets/portfolio.svg";
}
