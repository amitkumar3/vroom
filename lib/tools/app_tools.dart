import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:sample_flutter/constants/AppColors.dart';
import 'package:sample_flutter/constants/UIAssets.dart';
import 'package:sample_flutter/screens/Login.dart';
import 'package:sample_flutter/screens/VehicleListing.dart';
import 'package:sample_flutter/tools/ProgressDialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

//import 'package:vroom_ap/model/ProductDetailModel.dart';
//import 'package:vroom_ap/screens/Login.dart';
//import 'package:vroom_ap/screens/VehicleListing.dart';
//import 'package:vroom_ap/tools/progressdialog.dart';

//Map getStaticOwners() {
//  var owners = {};
//  owners["1st"] = Owner.fromJson({"id": "1st", "name": "First Owner"});
//  owners["2nd"] = Owner.fromJson({"id": "2nd", "name": "Second Owner"});
//  owners["3rd"] = Owner.fromJson({"id": "3rd", "name": "Thrid Owner"});
//  owners["4th"] = Owner.fromJson({"id": "4th", "name": "Fourth Owner"});
//  owners["5th"] = Owner.fromJson({"id": "5th", "name": "Fifth Owner"});
//  owners["6th"] = Owner.fromJson({"id": "6th", "name": "Sixth Owner"});
//  owners["7th"] = Owner.fromJson({"id": "7th", "name": "Seventh Owner"});
//  owners["8th"] = Owner.fromJson({"id": "8th", "name": "Eight Owner"});
//  owners["9th"] = Owner.fromJson({"id": "9th", "name": "Ninth Owner"});
//  owners["10th"] = Owner.fromJson({"id": "10th", "name": "Tenth Owner"});
//  return owners;
//}

//Map getStaticTransmissionTypes() {
//  var transmissionTypes = {};
//  transmissionTypes["1"] = Owner.fromJson({"id": "1", "name": "Automatic"});
//  transmissionTypes["2"] =
//      Owner.fromJson({"id": "2", "name": "Semi Automatic"});
//  transmissionTypes["3"] = Owner.fromJson({"id": "3", "name": "Manual"});
//  transmissionTypes["4"] =
//      Owner.fromJson({"id": "4", "name": "Automatic Manual Transmission"});
//
//  return transmissionTypes;
//}

Widget appTextField(
    {IconData textIcon,
    String textHint,
    bool isPassword,
    double sidePadding,
    TextInputType textType,
    TextEditingController controller}) {
  sidePadding == null ? sidePadding = 0.0 : sidePadding;
  textHint == null ? textHint = "" : textHint;
  //textType == null ? textType == TextInputType.text : textType;

  return Padding(
    padding: new EdgeInsets.only(left: sidePadding, right: sidePadding),
    child: new Container(
      decoration: new BoxDecoration(
        color: Colors.teal,
        borderRadius: new BorderRadius.all(Radius.circular(5.0)),
      ),
      child: TextField(
        controller: controller,
        obscureText: isPassword == null ? false : isPassword,
        keyboardType: textType == null ? TextInputType.text : textType,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: textHint,
          prefixIcon: textIcon == null ? Container() : Icon(textIcon),
        ),
      ),
    ),
  );
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

_showAlertDialog(context, String title, String description) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return alert dialog object
      return AlertDialog(
        title: Text(title),
        content: Text(description),
        actions: <Widget>[
          FlatButton(
            child: Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Widget appButton(
    {String btnTxt,
    double btnPadding,
    Color btnColor,
    VoidCallback onBtnclicked}) {
  btnTxt == null ? btnTxt == "App Button" : btnTxt;
  btnPadding == null ? btnPadding = 0.0 : btnPadding;
  btnColor == null ? btnColor = Colors.black : btnColor;

  return Padding(
    padding: EdgeInsets.all(btnPadding),
    child: RaisedButton(
      color: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      onPressed: onBtnclicked,
      child: Container(
        height: 50.0,
        child: Center(
          child: Text(
            btnTxt,
            style: TextStyle(color: btnColor, fontSize: 18.0),
          ),
        ),
      ),
    ),
  );
}

Color hexToColor(String code) {
  return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
}

String getTokenAmount(String amount, int token) {
  double reciprocal(double d) => 1 / d;
  var actualAmount = double.parse(amount) * reciprocal(token.toDouble()) / 100;
  if (token == 0) {
    //return "Pay (" + "৳ " + amount + ") Token Amount Now";
    return "Buy Now at " + "৳ " + amountFormatted(amount);
  } else {
    return "Pay (" +
        "৳ " +
        amountFormatted(actualAmount.toString()) +
        ") Token Amount Now";
  }
}

String vehicleDetailValue(value) {
  if (!(value is String)) {
    value = value == null ? null : value.name;
  }
  if (value == null) {
    return ("N/A");
  } else
    return value;
}

String amountFormatted(String amount) {
  FlutterMoneyFormatter fmf =
      FlutterMoneyFormatter(amount: double.parse(amount), );
  return fmf.output.nonSymbol.toString();
}

String validateName(String value) {
  String pattern = r'(^[a-zA-Z ]*$)';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) {
    return "Name is Required";
  } else if (!regExp.hasMatch(value)) {
    return "Name must be a-z and A-Z";
  }
  return null;
}

String validateAddress(String value) {
  String pattern = r'(^([0-9a-zA-Z]+)(,\s*[0-9a-zA-Z]+)*$)';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) {
    return "Address is Required";
  } else if (!regExp.hasMatch(value)) {
    return "Address must be a-z and A-Z";
  }
  return null;
}

String validateRegNumber(String value) {
  String pattern = r'(^[0-9]*$)';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) {
    return "Reg. Number is Required";
  } else if (!regExp.hasMatch(value)) {
    return "Reg. Number must be digits";
  }
  return null;
}

String validateKmDriven(String value) {
  String pattern = r'(^[0-9]*$)';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) {
    return "KMS is Required";
  } else if (!regExp.hasMatch(value)) {
    return "KMS must be digits";
  }
  return null;
}


String validateMobile(String value) {
  String pattern = r'(^[0-9]*$)';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) {
    return "Mobile is Required";
  } else if (value.length != 10) {
    return "Mobile number must 10 digits";
  } else if (!regExp.hasMatch(value)) {
    return "Mobile Number must be digits";
  }
  return null;
}

String validateOtp(String value) {
  if (value.isEmpty) {
    return "Please enter OTP";
  } else if (value.length != 4) {
    return "Please enter 4 digt OTP";
  }
  return null;
}

String validateEmail(String value) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) {
    return "Email is Required";
  } else if (!regExp.hasMatch(value)) {
    return "Invalid Email";
  } else {
    return null;
  }
}

String validatePassword(String value) {
  if (value.length == 0) {
    return "Password is Required";
  } else if (value.length < 6) {
    return "Please enter password more than 6 characters";
  }
}

// search validation
String validateSearch(String value) {
  if(value.isEmpty)
    return 'Please Enter some text';
}

String validateEmailAndMobile(String value) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  String pattern1 = r'(^[0-9]*$)';
  RegExp regExp = new RegExp(pattern);
  RegExp regExp1 = new RegExp(pattern1);
  if (value.length == 0) {
    return "Email is Required";
  } else if (!regExp.hasMatch(value)) {
    return "Invalid Email";
  } else if (value.length == 0) {
    return "Mobile is Required";
  } else if (value.length != 10) {
    return "Mobile number must 10 digits";
  } else if (!regExp1.hasMatch(value)) {
    return "Mobile Number must be digits";
  } else {
    return null;
  }
}

String newUsedVehicleValue(String value) {
  if (value == "Used Vehicles") {
    return "Used";
  } else if (value == "New Vehicles") {
    return "New";
  } else if (value == "Reconditioned Vehicles") {
    return "Reconditioned";
  } else if (value == null) {
    return "";
  } else {
    return "";
  }
}

Widget newUsedContainer(String text) {
  return newUsedVehicleValue(text) != ""
      ? Container(
          color: text == "NA" ? Colors.white : Colors.green,
          padding: EdgeInsets.all(8.0),
          child: Text(
            newUsedVehicleValue(text),
            style: TextStyle(
              fontSize: 15,
              color: hexToColor(AppColors.COLOR_TEXT_WHITE),
            ),
          ))
      : Container();
}

Widget productTextField(
    {String textTitle,
    String textHint,
    double height,
    TextEditingController controller,
    TextInputType textType}) {
  textTitle == null ? textTitle = "Enter Title" : textTitle;
  textHint == null ? textHint = "Enter Hint" : textHint;
  height == null ? height = 50.0 : height;
  //height !=null

  return Column(
    //mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          textTitle,
          style: TextStyle(fontWeight: FontWeight.w700, color: Colors.white),
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: Container(
          height: height,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.white),
              borderRadius: BorderRadius.all(Radius.circular(4.0))),
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: TextField(
              controller: controller,
              keyboardType: textType == null ? TextInputType.text : textType,
              decoration:
                  InputDecoration(border: InputBorder.none, hintText: textHint),
            ),
          ),
        ),
      ),
    ],
  );
}

/*Widget buildListImage(String image, String text) {
  return Expanded(
    child: Container(
      child: Column(
        children: <Widget>[
          Image(
            image: AssetImage(image),
            width: 32,
            height: 32,
          ),
          Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 10, color: Colors.grey[600]),
          ),
        ],
      ),
    ),
  );
}*/

//Widget buildDashListImage(
//    BuildContext context, int image, String text, String endPoint) {
//  return Expanded(
//      child: InkWell(
//        onTap: () {
//          Navigator.push(
//              context,
//              MaterialPageRoute(
//                builder: (BuildContext context) =>
//                    VehicleListing(text, endPoint, ""),
//              ));
//        },
//        child: Container(
//          child: Column(
//            children: <Widget>[
//              IconTheme(
//                data: IconThemeData(color: hexToColor("#7c7c7c")),
//                child: Icon(
//                  IconData(image, fontFamily: "Dash", matchTextDirection: true),
//                  size: 32.0,
//                ),
//              ),
//              Text(
//                text,
//                textAlign: TextAlign.center,
//                style: TextStyle(fontSize: 10, color: Colors.grey[600]),
//              ),
//            ],
//          ),
//        ),
//      ));
//}

Widget buildDrawerIcon(int image) {
  return IconTheme(
    data: IconThemeData(color: hexToColor(AppColors.COLOR_ICONS)),
    child: Icon(
      IconData(image, fontFamily: "Dash", matchTextDirection: true),
      size: 32.0,
    ),
  );
}

Widget buildListImage(int image, String text) {
  return Expanded(
    child: Container(
      child: Column(
        children: <Widget>[
          IconTheme(
            data: IconThemeData(color: hexToColor(AppColors.COLOR_ICONS)),
            child: Icon(
              IconData(image, fontFamily: "Dash", matchTextDirection: true),
              size: 32.0,
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 10, color: hexToColor(AppColors.COLOR_ICONS_TEXT)),
          ),
        ],
      ),
    ),
  );
}

Widget buildDealsPromotionTabs(String image, String text, String bottomText,
    String endPoint, BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
        //left: 10
        left: UIAssets.innerSpace),
    child: Container(
      width: 140.0,
      child: Card(
        // clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      VehicleListing(image, text, endPoint),
                ));
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.network(
                image,
                height: 75,
                width: 140,
                fit: BoxFit.cover,
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      text,
                      style: TextStyle(
                        fontSize: 8.5,
                        color: Colors.grey[500],
                      ),
                    ),
                    Container(
                      child: Text(
                        bottomText,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey[800],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

void showCupertinoAlert(String title, String contentText, String buttonText,
    String pageRoute, BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      child: CupertinoAlertDialog(
        content: Container(
          child: Text(
            title,
            // style: TextStyle(fontSize: 20.0),
          ),
        ),
        actions: <Widget>[
          Text(
            contentText,
            textAlign: TextAlign.center,
            style: TextStyle(
                backgroundColor: Colors.transparent,
                fontSize: 12.0,
                color: hexToColor(AppColors.COLOR_TEXT_BLACK)),
          ),
          SizedBox(
            height: 10.0,
          ),
          new Container(
            child: FlatButton(
                onPressed: () {
                  switch (pageRoute) {
                    case "forgotPassword":
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => Login(),
                          ));
                      break;
                    case "":
                      Navigator.pop(context);
                  }
                },
                child: Text(buttonText)),
          )
        ],
      ));
}

Widget buildSuggestionsText(String text, double size, String color) {
  return Padding(
    padding: EdgeInsets.all(8.0),
    child: Text(
      text,
      style: TextStyle(fontSize: size, color: hexToColor(color)),
    ),
  );
}

Widget buildSuggestionsSubText(String text, double size, String color) {
  return Text(
    text,
    style: TextStyle(fontSize: size, color: hexToColor(color)),
  );
}

//Widget buildVehicleTabs(
//    String image, String text, String endPoint, BuildContext context) {
//  return Card(
//    clipBehavior: Clip.antiAlias,
//    child: InkWell(
//      onTap: () {
//        Navigator.push(
//            context,
//            MaterialPageRoute(
//              builder: (BuildContext context) =>
//                  VehicleListing(text, endPoint, ""),
//            ));
//      },
//      child: Column(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          Image(
//            height: 75,
//            width: 120,
//            fit: BoxFit.cover,
//            image: AssetImage(image),
//          ),
//          Padding(
//            padding: EdgeInsets.all(8.0),
//            child: Text(
//              text,
//              style: TextStyle(
//                fontSize: 11,
//                color: Colors.grey[800],
//              ),
//            ),
//          ),
//        ],
//      ),
//    ),
//  );
//}

Widget buildVehicleDetailTabs(String type, String detail) {
  return Padding(
    padding: EdgeInsets.all(12.0),
    child: Row(
      children: <Widget>[
        Expanded(
          child: Text(
            type,
            style: TextStyle(
              fontSize: 16,
              color: Colors.grey[800],
            ),
          ),
        ),
        Expanded(
          child: Text(
            detail,
            style: TextStyle(
              fontSize: 16,
              color: Colors.grey[800],
            ),
          ),
        ),
      ],
    ),
  );
}

List<DropdownMenuItem<String>> buildAndGetDropDownItems(List size) {
  List<DropdownMenuItem<String>> items = new List();
  for (String size in size) {
    items.add(new DropdownMenuItem(value: size, child: new Text(size)));
  }
  return items;
}

showSnackBar(String message, final scaffoldKey) {
  scaffoldKey.currentState.showSnackBar(new SnackBar(
    backgroundColor: Colors.deepOrange[600],
    content: Text(
      message,
      style: TextStyle(color: Colors.white),
    ),
  ));
}

displayProgressDialog(BuildContext context) {
  Navigator.of(context).push(new PageRouteBuilder(
      opaque: false,
      pageBuilder: (BuildContext context, _, __) {
        return new ProgressDialog();
      }));
}

closeProgressDialog(BuildContext context) {
  Navigator.of(context).pop();
}

writeDataLocally({String key, String value}) async {
  Future<SharedPreferences> saveLocal = SharedPreferences.getInstance();
  final SharedPreferences localData = await saveLocal;
  localData.setString(key, value);
}

writeBoolDataLocally({String key, bool value}) async {
  Future<SharedPreferences> saveLocal = SharedPreferences.getInstance();
  final SharedPreferences localData = await saveLocal;
  localData.setBool(key, value);
}

writeBooleanDataLocally(String key, bool value) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  sharedPreferences.setBool(key, value);
  sharedPreferences.commit();
}

getBooleanDataLocally(String key) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  return sharedPreferences.getBool(key);
}

getDataLocally({String key}) async {
  Future<SharedPreferences> saveLocal = SharedPreferences.getInstance();
  final SharedPreferences localData = await saveLocal;
  return localData.get(key);
}

getStringDataLocally({String key}) async {
  Future<SharedPreferences> saveLocal = SharedPreferences.getInstance();
  final SharedPreferences localData = await saveLocal;
  return localData.getString(key);
}

getBoolDataLocally({String key}) async {
  Future<SharedPreferences> saveLocal = SharedPreferences.getInstance();
  final SharedPreferences localData = await saveLocal;
  return localData.getBool(key) == null ? false : localData.getBool(key);
}

clearDataLocally() async {
  Future<SharedPreferences> saveLocal = SharedPreferences.getInstance();
  final SharedPreferences localData = await saveLocal;
  localData.clear();
}

String formatAmount(double value) {
  double inThousands = value / 1000;
  if (value > 100000) {
    return (value / 100000).toStringAsFixed(2) + "L";
  }
  return (value / 1000).toStringAsFixed(2) + "k";
}

void redirectToLogin(BuildContext context) {
  Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) {
    return Login();
  }));
}
