
//enum Environment {
//  development,
//  uat,
//  production
//}

//class Constants {

//  static final Environment environment    = Environment.development;
//
//  static final String developmentDomain   = "https://vroom.infini.work/";
//  static final String uatDomain           = "https://vroom.infini.work/";
//  static final String productionDomain    = "https://vroom.infini.work/";
//
//  static final String suffix = "/api/";
//
//  static String getDomain() {
//    switch (environment) {
//      case Environment.development:
//        return developmentDomain;
//
//      case Environment.uat:
//        return uatDomain;
//
//      case Environment.production:
//        return productionDomain;
//    }
//    return developmentDomain;
//  }
//
//  static String getAPIPrefix() {
//    return getDomain() + suffix;
//  }

//  static final String modelDetails =
//      getAPIPrefix() + "model-details?responseType=json&";
//
//  static final String userLogin =
//      getAPIPrefix() + "check-user?responseType=json";
//
//  static final String preSubmitListing =
//      getAPIPrefix() + "add-post?responseType=json";
//
//  static final String checkExistingUser =
//      getAPIPrefix() + "check-existing-user?";
//
//  static final String userForgetPassword =
//      getAPIPrefix() + "forget-password?username=";
//
//  static final String imageEndpoint =
//      getDomain() + "public/Admin/uploads/catalog/products/";
//
//  static final String getCarListing =
//      getAPIPrefix() + "get-car-listing?";
//
//  static final String getSearchCarListing =
//      getAPIPrefix() + "get-search-results?autosearch=0&q=";

//  static final String COLOR_TRANSPARENT = "#00000000";
//  static final String COLOR_DARK_BLACK = "#000000";
//  static final String COLOR_TEXT_BLACK = "#000000";
//  static final String COLOR_TEXT_WHITE = "#ffffff";
//  static final String COLOR_APP_GREY = "#7C7C7C";
//  static final String COLOR_LIGHT_GREY = "#d6d6d6";
//  static final String COLOR_GREY_BACKGROUND = "#EFEFEF";
//  static final String COLOR_ICONS = "#7C7C7C";
//  static final String COLOR_ICONS_TEXT = "#7C7C7C";
//
//  //static final String COLOR_VROOM_THEME = "#e92918"; //THEME RED
//  static final String COLOR_VROOM_THEME = "#FCAF17"; //THEME YELLOW

  //Logo
//  static final String ASSET_APP_LOGO = "assets/vroom_logo_yellow.png";

//  static final String TEXT_REVIEW_HINT = "Write your review here...";
//
//  static final String TEXT_UPDATE_PROFILE = "Update Profile";
//
//  //NO RESULTS & SUGGESTIONS TEXT
//  static final String TEXT_NO_RESULTS_FOUND = "No Results Found.";
//  static final String TEXT_SUGGESTIONS = "Suggestions:";
//  static final String TEXT_SUGGESTION_POINT_1 =
//      "• Make Sure that all words are spelled correctly.";
//  static final String TEXT_SUGGESTION_POINT_2 = "• Try different keywords.";
//  static final String TEXT_SUGGESTION_POINT_3 = "• Try more general keywords.";
//  static final String TEXT_SUGGESTION_POINT_4 = "• Try fewer keywords.";
//
//  //FILTER TITLE TEXT
//  static final String TEXT_FILTER_TITLE = "Refine your Search";
//
//  //GENERAL TEXTS
//  static final String TEXT_ERROR = "Error";
//  static final String TEXT_SUCCESS = "Success";
//  static final String TEXT_OKAY = "Okay";
//  static final String TEXT_YES = "Yes";
//  static final String TEXT_NO = "No";
//  static final String TEXT_WENT_WRONG = "Something went Wrong!";
//}


import 'package:shared_preferences/shared_preferences.dart';

//class LocalStorage {

//  static void setValues() {
//    print("Setting values");
//    LocalStorage.save<String>("string", "This is string");
//    LocalStorage.save<bool>("bool", true);
//    LocalStorage.save<int>("int", 123);
//    LocalStorage.save<double>("double", 3.14);
//    print("Values set");
//  }

//  static void getValues() {
//    print("Getting values");
//    print(LocalStorage.get("string"));
//    var value = LocalStorage.get("string");
//    print("Values is: $value");
//    /*print(LocalStorage.get("bool"));
//    print(LocalStorage.get("int"));
//    print(LocalStorage.get("double"));*/
//    print("Values fetched");
//  }

//  static Future<T> get<T>(String key) async {
//    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//    print("String value: ${sharedPreferences.getString("string")}");
//    print("Int value: ${sharedPreferences.getInt("int")}");
//    print("Double value: ${sharedPreferences.getDouble("double")}");
//    print("Bool value: ${sharedPreferences.getBool("bool")}");
//    return Future(sharedPreferences.get(key));
//  }

//  static void save<T>(String key, T value) async {
//    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//
//    switch (T) {
//      case double:
//        sharedPreferences.setDouble(key, T.runtimeType as double);
//        break;
//
//      case int:
//        sharedPreferences.setInt(key, T.runtimeType as int);
//        break;
//
//      case bool:
//        sharedPreferences.setBool(key, T.runtimeType as bool);
//        break;
//
//      case String:
//        sharedPreferences.setString(key, T.runtimeType as String);
//        break;
//
//      default:
//        print("Unknown data type");
//        break;
//    }
//
//  }


//}